-- phpMyAdmin SQL Dump
-- version 4.6.4
-- https://www.phpmyadmin.net/
--
-- Host: 10.91.191.5
-- Tempo de geração: 11/10/2016 às 17:00
-- Versão do servidor: 5.7.15-log
-- Versão do PHP: 5.6.24

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Banco de dados: `tennissquare_tobias_legal`
--

-- --------------------------------------------------------

--
-- Estrutura para tabela `m2p_backend_logs`
--

CREATE TABLE `m2p_backend_logs` (
  `id` int(10) UNSIGNED NOT NULL,
  `site_id` int(10) UNSIGNED NOT NULL,
  `category` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `level` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `message` text COLLATE utf8_unicode_ci NOT NULL,
  `details` mediumtext COLLATE utf8_unicode_ci,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura para tabela `m2p_contents`
--

CREATE TABLE `m2p_contents` (
  `id` int(10) UNSIGNED NOT NULL,
  `site_id` int(10) UNSIGNED NOT NULL,
  `author` int(10) UNSIGNED NOT NULL,
  `title` text COLLATE utf8_unicode_ci,
  `excerpt` text COLLATE utf8_unicode_ci,
  `content` text COLLATE utf8_unicode_ci,
  `filtered` text COLLATE utf8_unicode_ci,
  `main_image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `content_type_id` int(10) UNSIGNED NOT NULL,
  `status` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `parent` int(10) DEFAULT NULL,
  `comment_status` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `comment_count` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Fazendo dump de dados para tabela `m2p_contents`
--

INSERT INTO `m2p_contents` (`id`, `site_id`, `author`, `title`, `excerpt`, `content`, `filtered`, `main_image`, `content_type_id`, `status`, `slug`, `parent`, `comment_status`, `comment_count`, `created_at`, `updated_at`) VALUES
(2, 1, 1, 'Treinamento competitivo', 'Centro de Formação para tenistas que querem seguir na carreira profissional ou conseguir bolsas de estudos através do tênis nos EUA. Também é destinado para aqueles que buscam melhorar o nível de tênis para disputar torneios paulistas, nacionais e internacionais. O treinamento, que conta com uma equipe multidisciplinar (técnico, preparador físico, nutricionista e fisioterapeuta), inclui aperfeiçoamento técnico/tático, plano nutricional e desenvolvimento físico específico.', '<div>\r\n        <p>Somos um Centro de Formação\r\nde Tenistas, em que um jogador pode começar com 5 ou 6 anos de idade e ir\r\nevoluindo até o profissional ou até conseguir uma bolsa de estudos em uma universidade\r\nnos EUA, com toda a estrutura necessária. Nos últimos dois anos, já enviamos\r\nmais de 10 tenistas para o tênis universitário americano. O treinamento, que\r\nconta com equipe multidisciplinar (técnico, preparador físico, nutricionista e\r\nfisioterapeuta), inclui aperfeiçoamento técnico/tático, plano nutricional e\r\ndesenvolvimento físico específico. O treinamento também é destinado àqueles que\r\nbuscam elevar o nível de jogo para a disputa de torneios paulistas, nacionais e internacionais.</p>\r\n\r\n        <p>Dentro do treinamento, os tenistas podem contar com:</p>\r\n        <ul>\r\n            <li>Análise biomecânica</li>\r\n            <li>Avaliação, Prevenção e Tratamento de Fisioterapia</li>\r\n            <li>Centro de Recuperação (Max Recovery)</li>\r\n            <li>Preparação Física voltada ao tênis</li>\r\n            <li>Planejamento de calendário de torneios</li>\r\n            <li>Acompanhamento em torneios </li>\r\n        </ul>\r\n\r\n        <p> Carga horária: de segunda a sexta-feira, das 8h às 9h (preparo físico), das 9h às 12h (treino técnico). Ou, das 14h às 18h (duas horas de quadra e duas horas de preparo físico).</p>\r\n\r\n    </div>\r\n    <div>\r\n        <div>\r\n\r\n            <div>\r\n                <div>\r\n                    <a target="_blank" rel="nofollow">Coordernador do tênis:&nbsp;André Watanabe</a>\r\n                </div>\r\n                <div>Formado em Educação Física pela UNICID</div>\r\n                <div>Pós-graduado em Personal Trainer pela FMU</div>\r\n                <div>Possui cursos de capacitação da CBT de nível 1 a 4</div>\r\n                <div>Cursos Nível 1 e 2 da Academia Nick Bollettieri</div>\r\n                <div>Experiência de 16 anos como treinador de tenistas juvenis e profissionais</div>\r\n            </div>\r\n        </div>\r\n        <p>\r\n\r\n            No treinamento há divisão por nível de tênis:</p><ul>\r\n            <li>Intermediários</li>\r\n            <li>Treinamento Competitivo.</li>\r\n        </ul>\r\n        <p></p>\r\n        <p>\r\n            Professores: Edvaldo Nascimento Costa (Iniciantes/intermediários) e Fernando Lima (Competitivo)\r\n            Preparador Físico: Diego Ganan \r\n        </p>\r\n    </div>', NULL, NULL, 1, 'published', 'treinamento-competitivo', 0, 'closed', 0, '2016-09-06 00:09:56', '2016-09-30 18:29:05'),
(3, 1, 1, 'Escola de tênis para crianças', 'Emprega o método Play and Stay, usado para ensinar o tênis de forma fácil e divertida para iniciantes. Desenvolvido pela Federação Internacional de Tênis (ITF), com bolas vermelhas (com 25% da velocidade da bola normal), bolas laranjas (50% da velocidade) e verdes (com 75% da velocidade da bola amarela). É o primeiro estágio de nosso Centro de Formação de Tenistas.', '<p>Emprega o método Play and Stay, usado para ensinar o tênis de forma fácil e divertida para iniciantes, e desenvolvido pela Federação Internacional de Tênis (ITF), com bolas vermelhas (com 25% da velocidade da bola normal), bolas laranjas (50% da velocidade) e verdes (com 75% da velocidade da bola amarela). O espaço de quadra ocupado também vai aumentando de acordo com o nível e a bola do aluno, começando no minitênis.</p><p><br></p><p>Divisão de Grupos para Crianças:</p><ul><li>De 5 a 7 anos;</li><li>De 8 a 10 anos;</li><li>De 10 a 12 anos.<br></li></ul>', NULL, NULL, 1, 'published', 'escola-de-tenis-para-criancas', 0, 'closed', 0, '2016-09-06 00:15:23', '2016-09-28 20:20:57'),
(5, 1, 1, 'Aulas individuais e em grupo', 'Os alunos podem escolher se preferem fazer aulas individuais ou em grupo de até quatro pessoas. Nas aulas individuais, o professor tem mais liberdade para fazer correções específicas, mas as aulas em grupo podem ser mais dinâmicas e também ajudam na evolução dos jogadores.', '<p>Os alunos podem escolher se preferem fazer aulas individuais ou em grupo de até quatro pessoas. Nas aulas individuais, o professor tem mais liberdade para fazer correções específicas, mas as aulas em grupo podem ser mais dinâmicas e também ajudam na evolução dos jogadores. Os professores da TennisSquare estão capacitados para dar aulas para todos os níveis e idades. Marque uma aula experimental!</p>', NULL, NULL, 1, 'published', 'aulas-individuais-e-em-grupo', 0, 'closed', 0, '2016-09-06 00:18:58', '2016-09-21 12:00:06'),
(6, 1, 1, 'Locação de quadras', 'Temos à sua disposição quadras rápidas e de saibro. Agende o seu horário e venha jogar tênis.', '<p>Aluno, atleta ou amador com interesse em locar nossas quadras para a \r\nprática da sua modalidade de tênis preferida? São quadras rápidas e de \r\nsaibro. Basta agendar o seu horário.<br></p>', NULL, NULL, 1, 'published', 'locacao-de-quadras', 0, 'closed', 0, '2016-09-06 00:20:46', '2016-09-21 12:01:31'),
(7, 1, 1, 'Atividades para condicionamento físico de tenistas', 'Entrar na quadra e começar logo a jogar. Esse é o desejo de quem quer começar no tênis. Porém, como em qualquer atividade física, é preciso preparar o corpo para o esforço. É preciso desenvolver e manter a forma física para praticar tênis, sem riscos de lesões, dores e exageros. E se você não joga tênis, mas quer manter a saúde em dia com um bom condicionamento físico, venha para a TennisSquare.', '<p>O tênis é um esporte que exige um preparo físico específico para os jogadores, que deve ter exercícios de fortalecimento muscular, agilidade, velocidade, alongamento, equilíbrio, coordenação motora e para melhora da capacidade cardiorrespiratória. Aqui na TennisSquare, temos uma estrutura completa para este tipo de atividade.<br></p>', NULL, NULL, 1, 'published', 'atividades-para-condicionamento-fisico-de-tenistas', 0, 'closed', 0, '2016-09-06 00:22:16', '2016-09-21 12:06:07'),
(8, 1, 1, 'Musculação', 'A TennisSquare possui uma sala totalmente equipada, com aparelhos de musculação modernos, para exercícios de fortalecimento e também aparelhos para exercícios aeróbios.', '<p>A TennisSquare possui uma sala totalmente equipada, com aparelhos de musculação modernos, para exercícios de fortalecimento e também aparelhos para exercícios aeróbios. Nossos instrutores irão montar treinos específicos, de acordo com as necessidades e objetivos de cada um, sendo um atleta ou alguém que acabou de começar a praticar atividade física.</p>', NULL, NULL, 1, 'published', 'musculacao', 0, 'closed', 0, '2016-09-06 01:15:47', '2016-10-05 11:44:16'),
(9, 1, 1, 'Spinning', 'A aula de spinning, que também pode ser chamada de bike indoor, queima gorduras, fortalece os músculos e aumenta a resistência cardiovascular e respiratória.', '<p>O treino de spinning é feito com uma bicicleta ergométrica, trabalha bastante os membros inferiores e melhora o sistema cardiorrespiratório. Durante as aulas de spinning é possível perder aproximadamente 400-600 calorias, variando de acordo com indivíduo, favorecendo a redução do percentual de gordura. Com músicas animadas, é uma maneira de entrar em forma e ainda emagrecer de uma forma divertida.</p>', NULL, NULL, 1, 'published', 'spinning', 0, 'closed', 0, '2016-09-06 01:17:09', '2016-09-28 20:28:57'),
(10, 1, 1, 'Jump + Step', 'Aulas de salto de fácil acesso para todas as pessoas, contando com músicas contagiantes e coreografias com diferentes graus de complexidade e intensidade. Panturrilhas, coxas e glúteos são trabalhados intensamente durante os movimentos realizados.', '<p>A prática do Jump e do Step trazem ao corpo inúmeros benefícios, fortalecem os músculos e trabalham na parte cardiovascular, muito importante para a saúde. As calorias gastas são relativas ao tempo, disposição e intensidade destes exercícios. Apesar de serem umas das aulas favoritas das mulheres, elas podem ser praticadas por qualquer pessoa.</p>', NULL, NULL, 1, 'published', 'jump-step', 0, 'closed', 0, '2016-09-06 01:18:24', '2016-09-21 12:22:49'),
(11, 1, 1, 'GAP', 'A sigla GAP significa glúteos, abdômen e pernas, então são aulas de exercícios localizados, que trabalham muito estas áreas, normalmente procuradas mais pelas mulheres, que querem trabalhar esses grupos musculares.', '<p>GAP são exercícios para glúteos, abdômen e pernas, explicando a razão da sigla. A aula é dividida em 3 partes:</p><ul><li>aquecimento para preparar o corpo;</li><li>exercícios que trabalham diversos músculos ao mesmo tempo;</li><li>alongamento e relaxamento.<br></li></ul>', NULL, NULL, 1, 'published', 'gap', 0, 'closed', 0, '2016-09-06 01:20:55', '2016-09-28 20:34:41'),
(12, 1, 1, 'Remo', 'As aulas de remo ajudam a tonificar os músculos, porque trabalham os membros superiores e inferiores ao mesmo tempo, e também melhoram o condicionamento e a resistência física. Varie sua prática de atividade física nas aulas de remo aqui na TennisSquare!', '<p>Os treinos com as máquinas de remo são muito intensos, movimentando várias partes do corpo, melhorando a circulação e também a respiração. Ajuda a tonificar os músculos e queimar calorias. Esta atividade trabalha as partes superiores e inferiores do corpo, fortalece os braços, e também trabalha a capacidade cardiovascular, pernas, glúteos, os músculos das costas e abdominais, deixando o corpo mais definido. Experimente!<br></p>', NULL, NULL, 1, 'published', 'remo', 0, 'closed', 0, '2016-09-06 01:22:39', '2016-09-28 20:31:22'),
(28, 1, 1, 'COMBO 1', NULL, '<h3><br>Escolinha de Tênis<br>e Musculação</h3><p>a partir de<br><br></p>', NULL, NULL, 5, 'published', 'combo-1', 0, 'closed', 0, '2016-09-15 00:06:12', '2016-09-30 19:19:33'),
(29, 1, 1, 'COMBO 2', NULL, '<h3>Escolinha de Tênis, Musculação,<br>Pilates ou Funcional<br></h3><p>a partir de<br><br></p>', NULL, NULL, 5, 'published', 'combo-2', 0, 'closed', 0, '2016-09-15 00:15:06', '2016-09-30 19:18:39'),
(30, 1, 1, 'COMBO 3', NULL, '<h3>Escolinha de Tênis, Musculação,<br>Pilates ou Funcional,<br>Locação e Yoga <br></h3><p>a partir de<br></p>', NULL, NULL, 5, 'published', 'combo-3', 0, 'closed', 0, '2016-09-15 00:15:30', '2016-09-30 19:17:33'),
(36, 1, 1, 'COMBO 4', NULL, '<h3>Aula de Tênis <br>e Musculação</h3><p>a partir de<br></p>', NULL, NULL, 5, 'published', 'combo-4', 0, 'closed', 0, '2016-09-21 20:51:55', '2016-09-28 19:59:10'),
(37, 1, 1, 'COMBO 5', NULL, '<h3>Locação de Quadras,<br>Pilates ou Funcional<br></h3><p><small>a partir de</small></p>', NULL, NULL, 5, 'published', 'combo-5', 0, 'closed', 0, '2016-09-21 20:52:25', '2016-09-28 19:58:04'),
(38, 1, 1, 'COMBO 6', NULL, '<h3>Escolinha de Tênis,<br>Pilates ou Funcional</h3><p>a partir de</p><i></i>', NULL, NULL, 5, 'published', 'combo-6', 0, 'closed', 0, '2016-09-21 20:53:06', '2016-09-30 19:17:45'),
(43, 1, 1, 'Desmotec', NULL, NULL, NULL, NULL, 6, 'published', 'desmotec', 0, 'closed', 0, '2016-09-28 19:57:44', '2016-09-28 19:57:44'),
(44, 1, 1, 'Musculação', NULL, NULL, NULL, NULL, 6, 'published', 'musculacaolK3lwPRG5P', 0, 'closed', 0, '2016-09-28 19:58:07', '2016-10-11 12:14:12'),
(46, 1, 1, 'Fitness Center', NULL, NULL, NULL, NULL, 6, 'published', 'fitness-center', 0, 'closed', 0, '2016-09-28 19:58:39', '2016-09-28 19:58:39'),
(47, 1, 1, 'Pilates', NULL, NULL, NULL, NULL, 6, 'published', 'pilates', 0, 'closed', 0, '2016-09-28 19:58:56', '2016-10-11 12:14:00'),
(48, 1, 1, 'Workshop de Treinamento Competitivo', 'Na última sexta-feira, 23/09, aconteceu na TennisSquare o Workshop de Treinamento Competitivo.', '<p>\r\n</p><p> Na última sexta-feira, 23/09, aconteceu na TennisSquare o Workshop \r\nde Treinamento Competitivo, ministrado pelo coordenador do tênis, <a target="_blank" rel="nofollow" href="https://www.facebook.com/andre.hinowatanabe">Andre Hino Watanabe</a>, o coordenador técnico, <a target="_blank" rel="nofollow" href="https://www.facebook.com/profile.php?id=100005109313472">Marcelo Zangrande Munhoz</a>, a nutricionista, Camila Homsi, o preparador físico, <a target="_blank" rel="nofollow" href="https://www.facebook.com/diego.souzaganan">Diego Souza Ganan</a> e pelo gerente da academia, Leandro Siqueira Castanho.  Também participaram os professores <a target="_blank" rel="nofollow" href="https://www.facebook.com/rodrigomgtonini">Rodrigo Muccillo G. Tonini</a> e <a target="_blank" rel="nofollow" href="https://www.facebook.com/edvaldonascimento.nascimento.963">Edvaldo Nascimento Nascimento</a>.</p><p> Foi apresentado o\r\n conceito do treinamento competitivo, que é destinado aos tenistas que \r\nquerem aprimorar o jogo com o mesmo tipo de treino dos atletas \r\nprofissionais, principalmente para as pessoas que precisam treinar em \r\nhorários flexíveis, com 1h de quadra e 30 minutos de exercícios \r\npreventivos. Também será possível complementar os treinos com atividades\r\n extras de nutrição, fisioterapia e preparação física específica.</p><div><p> Para saber mais, ligue para 2215-2521. Turmas iniciam em outubro.</p></div>\r\n\r\n<br><p></p>', NULL, NULL, 2, 'published', 'workshop-de-treinamento-competitivo', 0, 'opened', 0, '2016-09-28 20:02:29', '2016-09-28 20:02:37'),
(50, 1, 1, NULL, NULL, NULL, NULL, NULL, 3, 'published', 'juliano-bosco', 0, 'closed', 0, '2016-09-28 20:10:08', '2016-09-28 20:10:08'),
(51, 1, 1, NULL, NULL, NULL, NULL, NULL, 3, 'published', 'tiago-souza', 0, 'closed', 0, '2016-09-28 20:10:25', '2016-09-28 20:10:25'),
(52, 1, 1, NULL, NULL, NULL, NULL, NULL, 3, 'published', 'carlos-korniusza', 0, 'closed', 0, '2016-09-28 20:10:34', '2016-09-28 20:10:34'),
(53, 1, 1, NULL, NULL, NULL, NULL, NULL, 3, 'published', 'pedro-bosco', 0, 'closed', 0, '2016-09-28 20:10:46', '2016-09-28 20:10:46'),
(54, 1, 1, NULL, NULL, NULL, NULL, NULL, 3, 'published', 'eduardo-kawakami', 0, 'closed', 0, '2016-09-28 20:10:56', '2016-09-28 20:10:56'),
(55, 1, 1, NULL, NULL, NULL, NULL, NULL, 3, 'published', 'joao-matsumoto', 0, 'closed', 0, '2016-09-28 20:11:13', '2016-09-28 20:11:13'),
(56, 1, 1, NULL, NULL, NULL, NULL, NULL, 3, 'published', 'edu-corsi', 0, 'closed', 0, '2016-09-28 20:11:40', '2016-09-28 20:11:40'),
(57, 1, 1, NULL, NULL, NULL, NULL, NULL, 3, 'published', 'erik-ichikawa', 0, 'closed', 0, '2016-09-28 20:11:51', '2016-09-28 20:11:51'),
(58, 1, 1, NULL, NULL, NULL, NULL, NULL, 3, 'published', 'nilson-maeda', 0, 'closed', 0, '2016-09-28 20:12:03', '2016-09-28 20:12:03'),
(59, 1, 1, NULL, NULL, NULL, NULL, NULL, 3, 'published', 'alexandre-correa', 0, 'closed', 0, '2016-09-28 20:12:20', '2016-09-28 20:12:20'),
(60, 1, 1, NULL, NULL, NULL, NULL, NULL, 3, 'published', 'luciano-oliveira', 0, 'closed', 0, '2016-09-28 20:12:43', '2016-09-28 20:12:43'),
(61, 1, 1, NULL, NULL, NULL, NULL, NULL, 3, 'published', 'eduardo-toledo', 0, 'closed', 0, '2016-09-28 20:12:53', '2016-09-28 20:12:53'),
(62, 1, 1, NULL, NULL, NULL, NULL, NULL, 3, 'published', 'vitor-kitahara', 0, 'closed', 0, '2016-09-28 20:13:04', '2016-09-28 20:13:04'),
(63, 1, 1, NULL, NULL, NULL, NULL, NULL, 3, 'published', 'benedito-mario', 0, 'closed', 0, '2016-09-28 20:13:20', '2016-09-28 20:13:20'),
(64, 1, 1, NULL, NULL, NULL, NULL, NULL, 3, 'published', 'mario-kroller', 0, 'closed', 0, '2016-09-28 20:13:31', '2016-09-28 20:13:31'),
(65, 1, 1, 'Yoga', 'Yoga é uma técnica que une exercícios e meditação, trabalhando tanto a parte física quanto a mente. Ao mesmo tempo que a pessoa está melhorando a postura, a flexibilidade e o equilíbrio, o exercício também trabalha as emoções, pois ele promove um profundo relaxamento e concentração mental.', '<p>A prática de yoga alia benefícios físicos e qualidade de vida por meio de exercícios antiestresse, que refletem o bom funcionamento de todo o organismo. Promete ainda diminuir os níveis de colesterol, melhorar problemas respiratórios e controlar  a pressão arterial. Fisicamente, o yoga pode fortalecer os músculos e torná-los ainda mais flexíveis. Para quem sofre com problemas de ansiedade, insônia e depressão, também pode servir como calmante.<br></p>', NULL, NULL, 1, 'published', 'yoga', 0, 'closed', 0, '2016-09-30 20:11:09', '2016-10-04 14:47:36'),
(66, 1, 1, 'Treinamento Funcional', 'Nosso espaço possui equipamentos de última geração, como o Desmotec, utilizado para treinamento isoinercial, que trabalha força e potência de uma forma mais eficiente. Além do First Beat, que controla a frequência cardíaca. Estes aparelhos proporcionam um trabalho diferenciado para fortalecimento, alongamento e condicionamento físico geral.', '<p>É um método de trabalho caracterizado por mesclar diferentes capacidades físicas em um único exercício. O treinamento funcional é muito eficaz para o fortalecimento e para condicionamento físico geral. Em alguns exercícios, a carga de trabalho é exercida pelo próprio peso do corpo somado ao equilíbrio.<br></p>', NULL, NULL, 1, 'published', 'treinamento-funcionalWTf7czMBGh', 0, 'closed', 0, '2016-09-30 20:13:45', '2016-09-30 20:13:45'),
(67, 1, 1, 'Pilates', 'Estúdio completo, com aparelhos que trabalham simultaneamente o fortalecimento/alongamento dos músculos. O método criado por Joseph Pilates envolve o corpo e a mente, promovendo a reeducação de movimentos, pois eles são realizados sem pressa e com bastante controle.', '<p>Pilates é a técnica que trabalha a mente e o corpo em conjunto e possui como princípios básicos a concentração, o controle, a precisão e a respiração. Força, tonificação e alongamento são trabalhados na musculatura mais profunda, tornando o corpo forte, elegante e saudável. O grande repertório de exercícios com poucas repetições e baixo impacto proporcionam menos desgaste das articulações e dos músculos. Por isso, ele é indicado tanto para atletas profissionais, quanto para pessoas sedentárias e para pessoas em reabilitação.<br></p>', NULL, NULL, 1, 'published', 'pilatesApTPoCywTS', 0, 'closed', 0, '2016-09-30 20:15:29', '2016-09-30 20:15:29'),
(68, 1, 1, 'Nutrição', 'A nutrição adequada é muito importante para a saúde, para o rendimento esportivo e influencia diretamente nos resultados desejados. Com ajuda de uma equipe interdisciplinar, a nutrição apropriada promove bem-estar físico e mental, ajuda a melhorar o rendimento no esporte, facilita a recuperação, previne lesões, melhora os índices fisiológicos e ajuda a manter a composição corporal em níveis adequados.', '<p></p><p></p><p>OBJETIVOS</p>\r\n<p><b>Promoção de saúde</b></p>\r\n<ul>\r\n<li>Avaliação de composição corporal e estado nutricional; </li>\r\n<li>Avaliação de intolerâncias, sensibilidades e alergias alimentares; </li>\r\n<li>Adequação da oferta de macronutrientes (proteínas, lipídeos e carboidratos) e micronutrientes (vitaminas e minerais), conforme as necessidades específicas e que ajude no bom funcionamento orgânico.</li>\r\n</ul>\r\n<p><b>Rendimento Esportivo</b></p>\r\n<ul>\r\n<li>Ter uma alimentação conforme a demanda da rotina de treinos (intensidade, duração e frequência); </li>\r\n<li>Controle das refeições antes, durante e após o exercício;</li>\r\n<li>Adequar a composição corporal (ganho de massa muscular, definição, redução de % de gordura) Estratégias adequadas de hidratação; </li>\r\n<li>Utilizar de forma adequada os suplementos esportivos; </li>\r\n<li>Ter uma alimentação buscando melhor fornecimento de energia; </li>\r\n<li>Recuperação integral entre sessões de treino / evento esportivo (estoques energéticos e reparação tecidual); </li>\r\n<li>Buscar na alimentação a redução de danos inflamatórios e de estresse oxidativo do esporte.</li>\r\n</ul>\r\n<p>PROCEDIMENTOS REALIZADOS</p>\r\n<p><b>Avaliação de hábitos alimentares</b></p>\r\n<p>Coleta de dados detalhados sobre o padrão alimentar. A nutricionista coleta informações quantitativas e qualitativas. São identificadas deficiências/excessos de cada nutriente, distribuição calórica ao longo do dia, consumo calórico diário, predisposição para agravos à saúde ou desequilíbrio orgânico, combinado com horários das refeições e padrões de sono.</p>\r\n<p><b>Avaliação Antropométrica</b></p>\r\n<p>Avaliação da composição corporal realizada a partir da aferição de peso, altura, circunferências corporais e dobras cutâneas medidas por meio do adipômetro em diferentes áreas do corpo. Verifica-se, dessa forma, a adequação do estado nutricional, porcentagem de gordura e massa magra.</p>\r\n<p><b>Teste de Bioimpedância (Inbody)</b></p>\r\n<p>Equipamento de análise da composição corporal por meio da tecnologia de bioimpedância que oferece um relatório de percentual e distribuição de gordura corporal, massa magra, gordura visceral, densidade óssea, taxa metabólica, idade metabólica, taxa de hidratação e sugestão de valor energético diário.</p>\r\n<p><b>Avaliação de gasto calórico diário</b></p>\r\n<p>Avaliação da rotina ocupacional e da rotina de treinos (modalidade, frequência, intensidade e duração); dessa forma é estimado o gasto calórico diário. Métodos mais complexos como o teste espirométrico, e a bioimpedância também podem ser utilizados para esta avaliação.</p>\r\n<p><b>Diagnóstico Nutricional</b></p>\r\n<p>Interpretação de todas as avaliações realizadas previamente. Informações gráficas e conceituais compõem um relatório que apresenta todo o cenário nutricional do indivíduo avaliado, bem como os pontos que devem ser considerados na elaboração do plano alimentar (cálculos, objetivos de composição corporal, funcionais, suplementação e práticas gerais).</p>\r\n<p><b>Plano Alimentar</b></p>\r\n<p>Apresentação em relatório dos alimentos e suplementos que deverão compor o plano alimentar em porções e horários pré-determinados, a partir dos aspectos identificados no diagnóstico nutricional. São inseridas as refeições pré e pós-treino, bem como as reposições energéticas adequadas para cada objetivo. O plano alimentar proposto é embasado em conceitos nutricionais atuais, buscando atender os objetivos do indivíduo.</p>\r\n<p><b>Teste de sensibilidade (intolerância) alimentar (Food Detective)</b></p>\r\n<p>A intolerância alimentar decorre de processos imunológicos que levam ao aparecimento de sintomas como: cansaço, desatenção, cólicas, enxaquecas, tonturas, náuseas, aftas, prisão de ventre, diarreias, inchaço pelo corpo, dores articulares, entre outros. O teste é específico para identificar 59 alimentos que podem levar à produção de anticorpos IGG que podem estar associados à intolerância alimentar. É feito a partir de rápida coleta de sangue (como no exame de glicose) e analisado pelo método ELISA.</p>\r\n<p><b>Avaliação da taxa de sudorese</b></p>\r\n<p>Protocolo nutricional para identificação do volume de suor produzido durante 1 hora de exercício. A partir dessa avaliação, a nutricionista estabelece uma estratégia de hidratação individual, que atenda as necessidades exigidas para a manutenção do estado ótimo de hidratação.</p>\r\n<p><b>Avaliação do estado de hidratação</b></p>\r\n<p>O estado de hidratação é avaliado a partir da combinação do controle de pesagens, amostra de urina e da bioimpedância elétrica, com o objetivo de adequar estratégias de hidratação para melhor funcionamento muscular, regulação da temperatura corporal e rendimento esportivo.</p>\r\n<p><b>Coleta de exames in loco para avaliação de fatores limitantes de rendimento esportivo</b></p>\r\n<p>Coleta de exames laboratoriais importantes para a intervenção nutricional em esportistas e atletas de alto rendimento. São avaliados antes e após o exercício: perda hídrica, glicemia, corpos cetônicos, cortisol e marcadores bioquímicos de danos musculares por desgaste físico.</p>\r\n\r\n\r\n<p></p>\r\n\r\n\r\n\r\n\r\n\r\n<p></p>', NULL, NULL, 1, 'published', 'nutricao', 0, 'closed', 0, '2016-09-30 20:19:09', '2016-10-03 12:22:50'),
(69, 1, 1, 'Fisioterapia Esportiva', 'A Fisioterapia Esportiva é uma área da fisioterapia que busca a prevenção e a reabilitação do atleta amador e profissional, ou de qualquer outra pessoa que pratique atividade física.', '<p>A Fisioterapia Esportiva é uma área da fisioterapia que busca a prevenção e a reabilitação do atleta amador e profissional, ou de qualquer outra pessoa que pratique atividade física.<br></p>\r\n<p>Nosso serviço conta com profissionais especializados e com ampla experiência de atuação na área esportiva. Todos os pacientes passam por uma avaliação detalhada para a elaboração de um tratamento específico para cada lesão.</p>\r\n<p>A Equipe de Fisioterapia também se preocupa com a integridade física do atleta, sendo assim, disponibiliza um programa exclusivo de Prevenção de Lesões para todos os tipos de esporte.</p>\r\n<p><b>ESTRUTURA</b></p>\r\n<ul>\r\n<li>Espaço específico para reabilitação e retorno funcional ao esporte.</li>\r\n<li>Equipamentos de última geração utilizados nos melhores centros esportivos (Desmotec, Compex, Laser, Ultra-Som e eletroterapia).</li>\r\n<li>Excelência em Terapia Manual. </li>\r\n</ul>\r\n<p>a) Rolfing</p>\r\n<p>É uma terapia de manipulação física que envolve mudanças significativas na forma do corpo, nos padrões de uso habitual, reforçando o equilíbrio e que libera a rigidez existente entre os segmentos corporais.</p>\r\n<p>Benefícios:</p>\r\n<p>- alinha, alonga e integra o corpo;</p>\r\n<p>- alívio de dores e tensões musculares;</p>\r\n<p>- melhora da circulação e respiração.</p>\r\n<p>b) Osteopatia</p>\r\n<p>É um método de Diagnóstico, Tratamento e Cura que se utiliza de recursos manuais para interferir na estrutura e função do organismo (articulações, músculos, ligamentos, fáscias, vísceras, tecidos nervoso, vascular e linfático).</p>\r\n<p>Benefícios:</p>\r\n<p>- melhora de dores agudas ou crônicas da coluna (hérnia de disco e ciáticas);</p>\r\n<p>- alívio de dores articulares;</p>\r\n<p>- melhora dos sistemas: digestivo, circulatório, respiratório, neural, linfático e imunológico.</p>\r\n<p>c) Liberação Miofascial – Miofibrólise Percutânea</p>\r\n<p>É um método de tratamento não invasivo (que não envolve incisões), que intervém sobre as fibroses e contraturas funcionais do tecido muscular com o objetivo de liberar o movimento e aliviar grande parte das dores sejam traumáticas ou inflamatórias.</p>\r\n<p>Benefícios:</p>\r\n<p>- aumento da mobilidade de diversos planos miofasciais;</p>\r\n<p>- Quebra de fibroses e aderências;</p>\r\n<p>- relaxamento muscular.</p>\r\n\r\n\r\n<br><p></p>', NULL, NULL, 1, 'published', 'fisioterapia-esportiva', 0, 'closed', 0, '2016-09-30 20:19:56', '2016-10-03 17:41:00'),
(70, 1, 1, 'Max Recovery', 'Diminuição significativa da dor e edema, resultando em uma menor necessidade de analgésicos e uma cicatrização mais rápida.', '<p></p><p><b>Crioimersão -</b> imersão do corpo ou segmento corporal na água com gelo.</p>\r\n<p><b>Equipamentos Game Ready -</b> novo padrão no tratamento de lesões, que faz crioterapia e compressão ativa, para uma recuperação mais completa e eficiente.</p>\r\n<p>Vantagens para o paciente:</p>\r\n<p>- Recuperação 20% mais rápida em comparação com outras modalidades de crioterapia.</p>\r\n<p>- Diminuição significativa da dor e edema, resultando em uma menor necessidade de analgésicos e uma cicatrização mais rápida.</p>\r\n\r\n\r\n<br><p></p>', NULL, NULL, 1, 'published', 'max-recovery', 0, 'closed', 0, '2016-09-30 20:20:27', '2016-10-05 13:37:30'),
(71, 1, 1, NULL, NULL, NULL, NULL, NULL, 3, 'published', 'paulo-kimura', 0, 'closed', 0, '2016-09-30 20:41:44', '2016-09-30 20:41:44'),
(72, 1, 1, NULL, NULL, NULL, NULL, NULL, 3, 'published', 'leandro-silva', 0, 'closed', 0, '2016-09-30 20:41:59', '2016-09-30 20:41:59'),
(73, 1, 1, NULL, NULL, NULL, NULL, NULL, 3, 'published', 'leandro-turbino', 0, 'closed', 0, '2016-09-30 20:42:15', '2016-09-30 20:42:15'),
(74, 1, 1, NULL, NULL, NULL, NULL, NULL, 3, 'published', 'otavio-g-fachin', 0, 'closed', 0, '2016-09-30 20:42:25', '2016-09-30 20:42:25'),
(75, 1, 1, NULL, NULL, NULL, NULL, NULL, 3, 'published', 'willian-sato', 0, 'closed', 0, '2016-09-30 20:42:36', '2016-09-30 20:42:36'),
(76, 1, 1, NULL, NULL, NULL, NULL, NULL, 3, 'published', 'marcio-paglia', 0, 'closed', 0, '2016-09-30 20:42:49', '2016-09-30 20:42:49'),
(77, 1, 1, NULL, NULL, NULL, NULL, NULL, 3, 'published', 'cecero', 0, 'closed', 0, '2016-09-30 20:43:03', '2016-09-30 20:43:03'),
(78, 1, 1, NULL, NULL, NULL, NULL, NULL, 3, 'published', 'luis-munhoz', 0, 'closed', 0, '2016-09-30 20:43:13', '2016-09-30 20:43:13'),
(79, 1, 1, NULL, NULL, NULL, NULL, NULL, 3, 'published', 'marcelo-urbano', 0, 'closed', 0, '2016-09-30 20:43:22', '2016-09-30 20:43:22'),
(80, 1, 1, NULL, NULL, NULL, NULL, NULL, 3, 'published', 'roberto-luis', 0, 'closed', 0, '2016-09-30 20:43:32', '2016-09-30 20:43:32'),
(81, 1, 1, NULL, NULL, NULL, NULL, NULL, 3, 'published', 'rogerio-both', 0, 'closed', 0, '2016-09-30 20:43:42', '2016-09-30 20:43:42'),
(82, 1, 1, NULL, NULL, NULL, NULL, NULL, 3, 'published', 'valter-trotta', 0, 'closed', 0, '2016-09-30 20:43:51', '2016-09-30 20:43:51'),
(83, 1, 1, NULL, NULL, NULL, NULL, NULL, 3, 'published', 'rodrigues-arjonas', 0, 'closed', 0, '2016-09-30 20:44:01', '2016-09-30 20:44:01'),
(84, 1, 1, NULL, NULL, NULL, NULL, NULL, 3, 'published', 'rogerio-onil', 0, 'closed', 0, '2016-09-30 20:44:13', '2016-09-30 20:44:13'),
(85, 1, 1, NULL, NULL, NULL, NULL, NULL, 3, 'published', 'igor', 0, 'closed', 0, '2016-09-30 20:44:23', '2016-09-30 20:44:23'),
(86, 1, 1, NULL, NULL, NULL, NULL, NULL, 3, 'published', 'daniel-abdul', 0, 'closed', 0, '2016-09-30 20:44:33', '2016-09-30 20:44:33'),
(87, 1, 1, NULL, NULL, NULL, NULL, NULL, 3, 'published', 'daniel-oliveira-aguiar', 0, 'closed', 0, '2016-09-30 20:44:43', '2016-09-30 20:44:43'),
(88, 1, 1, NULL, NULL, NULL, NULL, NULL, 3, 'published', 'denis-telles-gaia', 0, 'closed', 0, '2016-09-30 20:44:53', '2016-09-30 20:44:53'),
(89, 1, 1, NULL, NULL, NULL, NULL, NULL, 3, 'published', 'eduardo-katsuragawa', 0, 'closed', 0, '2016-09-30 20:45:11', '2016-09-30 20:45:11'),
(90, 1, 1, NULL, NULL, NULL, NULL, NULL, 3, 'published', 'fernando-beltrani', 0, 'closed', 0, '2016-09-30 20:45:22', '2016-09-30 20:45:22'),
(91, 1, 1, NULL, NULL, NULL, NULL, NULL, 3, 'published', 'fernando-losso', 0, 'closed', 0, '2016-09-30 20:45:32', '2016-09-30 20:45:32'),
(92, 1, 1, NULL, NULL, NULL, NULL, NULL, 3, 'published', 'guilherme-medeiros', 0, 'closed', 0, '2016-09-30 20:45:42', '2016-09-30 20:45:42'),
(93, 1, 1, NULL, NULL, NULL, NULL, NULL, 3, 'published', 'ivan-s-codo', 0, 'closed', 0, '2016-09-30 20:45:51', '2016-09-30 20:45:51'),
(94, 1, 1, NULL, NULL, NULL, NULL, NULL, 3, 'published', 'moacir-passo', 0, 'closed', 0, '2016-09-30 20:46:01', '2016-09-30 20:46:01'),
(95, 1, 1, NULL, NULL, NULL, NULL, NULL, 3, 'published', 'richard-ferraresi', 0, 'closed', 0, '2016-09-30 20:46:11', '2016-09-30 20:46:11'),
(96, 1, 1, NULL, NULL, NULL, NULL, NULL, 3, 'published', 'roberto-bicudo', 0, 'closed', 0, '2016-09-30 20:46:25', '2016-09-30 20:46:25'),
(97, 1, 1, NULL, NULL, NULL, NULL, NULL, 3, 'published', 'thales-randazzo', 0, 'closed', 0, '2016-09-30 20:46:46', '2016-09-30 20:46:46'),
(98, 1, 1, NULL, NULL, NULL, NULL, NULL, 3, 'published', 'wladimir-pereira', 0, 'closed', 0, '2016-09-30 20:46:55', '2016-09-30 20:46:55'),
(99, 1, 1, NULL, NULL, NULL, NULL, NULL, 3, 'published', 'denis-castanho', 0, 'closed', 0, '2016-09-30 20:50:14', '2016-09-30 20:50:14'),
(100, 1, 1, NULL, NULL, NULL, NULL, NULL, 3, 'published', 'gustavo-moreno', 0, 'closed', 0, '2016-09-30 20:50:26', '2016-09-30 20:50:26'),
(101, 1, 1, NULL, NULL, NULL, NULL, NULL, 3, 'published', 'jardel-correa', 0, 'closed', 0, '2016-09-30 20:50:38', '2016-09-30 20:50:38'),
(102, 1, 1, NULL, NULL, NULL, NULL, NULL, 3, 'published', 'jose-flavio', 0, 'closed', 0, '2016-09-30 20:50:47', '2016-09-30 20:50:47'),
(103, 1, 1, NULL, NULL, NULL, NULL, NULL, 3, 'published', 'ermeson-buassali', 0, 'closed', 0, '2016-09-30 20:50:57', '2016-09-30 20:50:57'),
(104, 1, 1, NULL, NULL, NULL, NULL, NULL, 3, 'published', 'fabian', 0, 'closed', 0, '2016-09-30 20:51:07', '2016-09-30 20:51:07'),
(105, 1, 1, NULL, NULL, NULL, NULL, NULL, 3, 'published', 'caio-cracco', 0, 'closed', 0, '2016-09-30 20:51:16', '2016-09-30 20:51:16'),
(106, 1, 1, NULL, NULL, NULL, NULL, NULL, 3, 'published', 'franscis-matsuda', 0, 'closed', 0, '2016-09-30 20:51:25', '2016-09-30 20:51:25'),
(107, 1, 1, NULL, NULL, NULL, NULL, NULL, 3, 'published', 'guilherme-cunha', 0, 'closed', 0, '2016-09-30 20:51:35', '2016-09-30 20:51:35'),
(108, 1, 1, NULL, NULL, NULL, NULL, NULL, 3, 'published', 'eduardo-germani', 0, 'closed', 0, '2016-09-30 20:51:45', '2016-09-30 20:51:45'),
(109, 1, 1, NULL, NULL, NULL, NULL, NULL, 3, 'published', 'rodrigo-bedin', 0, 'closed', 0, '2016-09-30 20:51:54', '2016-09-30 20:51:54'),
(110, 1, 1, NULL, NULL, NULL, NULL, NULL, 3, 'published', 'fabricio-carvalho', 0, 'closed', 0, '2016-09-30 20:52:03', '2016-09-30 20:52:03'),
(111, 1, 1, NULL, NULL, NULL, NULL, NULL, 3, 'published', 'julio-veloso', 0, 'closed', 0, '2016-09-30 20:52:16', '2016-09-30 20:52:16'),
(112, 1, 1, NULL, NULL, NULL, NULL, NULL, 3, 'published', 'alan-evangelista', 0, 'closed', 0, '2016-09-30 20:52:25', '2016-09-30 20:52:25'),
(113, 1, 1, NULL, NULL, NULL, NULL, NULL, 3, 'published', 'massami-kobo', 0, 'closed', 0, '2016-09-30 20:52:34', '2016-09-30 20:52:34'),
(114, 1, 1, NULL, NULL, NULL, NULL, NULL, 3, 'published', 'michel-furlan', 0, 'closed', 0, '2016-09-30 20:52:47', '2016-09-30 20:52:47'),
(115, 1, 1, NULL, NULL, NULL, NULL, NULL, 3, 'published', 'marcelo-massano', 0, 'closed', 0, '2016-09-30 20:52:59', '2016-09-30 20:52:59'),
(116, 1, 1, NULL, NULL, NULL, NULL, NULL, 3, 'published', 'dagmar-ribeiro', 0, 'closed', 0, '2016-09-30 20:53:08', '2016-09-30 20:53:08'),
(117, 1, 1, NULL, NULL, NULL, NULL, NULL, 3, 'published', 'diogo-conte', 0, 'closed', 0, '2016-09-30 20:53:17', '2016-09-30 20:53:17'),
(118, 1, 1, NULL, NULL, NULL, NULL, NULL, 3, 'published', 'marcelo-urbanoEjY46odv87', 0, 'closed', 0, '2016-09-30 20:53:26', '2016-09-30 20:53:26'),
(119, 1, 1, NULL, NULL, NULL, NULL, NULL, 3, 'published', 'adilson-arruda', 0, 'closed', 0, '2016-09-30 20:53:51', '2016-09-30 20:53:51'),
(120, 1, 1, NULL, NULL, NULL, NULL, NULL, 3, 'published', 'sergio-r-da-silva', 0, 'closed', 0, '2016-09-30 20:54:00', '2016-09-30 20:54:00'),
(121, 1, 1, NULL, NULL, NULL, NULL, NULL, 3, 'published', 'alessandro-fernandes', 0, 'closed', 0, '2016-09-30 20:54:09', '2016-09-30 20:54:09'),
(122, 1, 1, NULL, NULL, NULL, NULL, NULL, 3, 'published', 'dacio-ogata', 0, 'closed', 0, '2016-09-30 20:54:19', '2016-09-30 20:54:19'),
(123, 1, 1, NULL, NULL, NULL, NULL, NULL, 3, 'published', 'marlon-amorim', 0, 'closed', 0, '2016-09-30 20:54:29', '2016-09-30 20:54:29'),
(124, 1, 1, NULL, NULL, NULL, NULL, NULL, 3, 'published', 'pablo-suzuki', 0, 'closed', 0, '2016-09-30 20:54:38', '2016-09-30 20:54:38'),
(125, 1, 1, NULL, NULL, NULL, NULL, NULL, 3, 'published', 'alexandre-moraes', 0, 'closed', 0, '2016-09-30 20:54:47', '2016-09-30 20:54:47'),
(126, 1, 1, NULL, NULL, NULL, NULL, NULL, 3, 'published', 'alexandre-souza', 0, 'closed', 0, '2016-09-30 20:54:56', '2016-09-30 20:54:56'),
(127, 1, 1, NULL, NULL, NULL, NULL, NULL, 3, 'published', 'andre-pinto', 0, 'closed', 0, '2016-09-30 20:55:04', '2016-09-30 20:55:04'),
(128, 1, 1, NULL, NULL, NULL, NULL, NULL, 3, 'published', 'andrey-vicockas', 0, 'closed', 0, '2016-09-30 20:55:13', '2016-09-30 20:55:13'),
(129, 1, 1, NULL, NULL, NULL, NULL, NULL, 3, 'published', 'arnaldo-marinho', 0, 'closed', 0, '2016-09-30 20:55:22', '2016-09-30 20:55:22'),
(130, 1, 1, NULL, NULL, NULL, NULL, NULL, 3, 'published', 'bruno-boscolo', 0, 'closed', 0, '2016-09-30 20:55:32', '2016-09-30 20:55:32'),
(131, 1, 1, NULL, NULL, NULL, NULL, NULL, 3, 'published', 'eduardo-quintana', 0, 'closed', 0, '2016-09-30 20:55:40', '2016-09-30 20:55:40'),
(132, 1, 1, NULL, NULL, NULL, NULL, NULL, 3, 'published', 'eloy-nicotera', 0, 'closed', 0, '2016-09-30 20:55:53', '2016-09-30 20:55:53'),
(133, 1, 1, NULL, NULL, NULL, NULL, NULL, 3, 'published', 'danilo-saito', 0, 'closed', 0, '2016-10-03 13:41:41', '2016-10-03 13:41:41'),
(134, 1, 1, NULL, NULL, NULL, NULL, NULL, 3, 'published', 'celso-igarachi', 0, 'closed', 0, '2016-10-03 13:41:52', '2016-10-03 13:41:52'),
(135, 1, 1, NULL, NULL, NULL, NULL, NULL, 3, 'published', 'jose-manuel-parada', 0, 'closed', 0, '2016-10-03 13:42:07', '2016-10-03 13:42:07'),
(136, 1, 1, NULL, NULL, NULL, NULL, NULL, 3, 'published', 'fernando-cardoso', 0, 'closed', 0, '2016-10-03 13:42:16', '2016-10-03 13:42:16'),
(137, 1, 1, NULL, NULL, NULL, NULL, NULL, 3, 'published', 'danilo-salerno', 0, 'closed', 0, '2016-10-03 13:42:33', '2016-10-03 13:42:33'),
(138, 1, 1, NULL, NULL, NULL, NULL, NULL, 3, 'published', 'eduardo-neves', 0, 'closed', 0, '2016-10-03 13:42:43', '2016-10-03 13:42:43'),
(139, 1, 1, NULL, NULL, NULL, NULL, NULL, 3, 'published', 'matheus-almeida', 0, 'closed', 0, '2016-10-03 13:42:53', '2016-10-03 13:42:53'),
(140, 1, 1, NULL, NULL, NULL, NULL, NULL, 3, 'published', 'edney-cabral', 0, 'closed', 0, '2016-10-03 13:43:16', '2016-10-03 13:43:16'),
(141, 1, 1, NULL, NULL, NULL, NULL, NULL, 3, 'published', 'thibault', 0, 'closed', 0, '2016-10-03 13:43:27', '2016-10-03 13:43:27'),
(142, 1, 1, NULL, NULL, NULL, NULL, NULL, 3, 'published', 'andre-massis', 0, 'closed', 0, '2016-10-03 13:43:39', '2016-10-03 13:43:39'),
(143, 1, 1, NULL, NULL, NULL, NULL, NULL, 3, 'published', 'danilo-norcia', 0, 'closed', 0, '2016-10-03 13:43:48', '2016-10-03 13:43:48'),
(144, 1, 1, NULL, NULL, NULL, NULL, NULL, 3, 'published', 'guilherme-teles', 0, 'closed', 0, '2016-10-03 13:43:58', '2016-10-03 13:43:58'),
(145, 1, 1, NULL, NULL, NULL, NULL, NULL, 3, 'published', 'otoniel-barbado', 0, 'closed', 0, '2016-10-03 13:44:07', '2016-10-03 13:44:07'),
(146, 1, 1, NULL, NULL, NULL, NULL, NULL, 3, 'published', 'eduardo-silveira', 0, 'closed', 0, '2016-10-03 13:44:17', '2016-10-03 13:44:17'),
(147, 1, 1, NULL, NULL, NULL, NULL, NULL, 3, 'published', 'fabio-cardoso', 0, 'closed', 0, '2016-10-03 13:44:26', '2016-10-03 13:44:26'),
(148, 1, 1, NULL, NULL, NULL, NULL, NULL, 3, 'published', 'nairan-pedreira', 0, 'closed', 0, '2016-10-03 13:44:35', '2016-10-03 13:44:35'),
(149, 1, 1, NULL, NULL, NULL, NULL, NULL, 3, 'published', 'leandro-sanday', 0, 'closed', 0, '2016-10-03 13:44:47', '2016-10-03 13:44:47'),
(150, 1, 1, NULL, NULL, NULL, NULL, NULL, 3, 'published', 'julio-baiocato', 0, 'closed', 0, '2016-10-03 13:44:58', '2016-10-03 13:44:58'),
(151, 1, 1, NULL, NULL, NULL, NULL, NULL, 3, 'published', 'eder-miranda', 0, 'closed', 0, '2016-10-03 13:45:06', '2016-10-03 13:45:06'),
(152, 1, 1, NULL, NULL, NULL, NULL, NULL, 3, 'published', 'eduardo-yano', 0, 'closed', 0, '2016-10-03 13:45:15', '2016-10-03 13:45:15'),
(153, 1, 1, NULL, NULL, NULL, NULL, NULL, 3, 'published', 'flavio-shitara', 0, 'closed', 0, '2016-10-03 13:45:29', '2016-10-03 13:45:29'),
(154, 1, 1, NULL, NULL, NULL, NULL, NULL, 3, 'published', 'shozo-hayakawa', 0, 'closed', 0, '2016-10-03 13:45:40', '2016-10-03 13:45:40'),
(155, 1, 1, NULL, NULL, NULL, NULL, NULL, 3, 'published', 'yasunori-suzue', 0, 'closed', 0, '2016-10-03 13:45:56', '2016-10-03 13:45:56'),
(156, 1, 1, NULL, NULL, NULL, NULL, NULL, 3, 'published', 'wagner-rodrigues', 0, 'closed', 0, '2016-10-03 13:46:05', '2016-10-03 13:46:05'),
(157, 1, 1, NULL, NULL, NULL, NULL, NULL, 3, 'published', 'alberto-burani', 0, 'closed', 0, '2016-10-03 13:46:14', '2016-10-03 13:46:14'),
(158, 1, 1, NULL, NULL, NULL, NULL, NULL, 3, 'published', 'antonio-blanck', 0, 'closed', 0, '2016-10-03 13:46:23', '2016-10-03 13:46:23'),
(159, 1, 1, NULL, NULL, NULL, NULL, NULL, 3, 'published', 'fabricio-carvalhodDANVLMqZB', 0, 'closed', 0, '2016-10-03 13:46:33', '2016-10-03 13:46:33'),
(160, 1, 1, NULL, NULL, NULL, NULL, NULL, 3, 'published', 'gustavo-matsumoto', 0, 'closed', 0, '2016-10-03 13:46:46', '2016-10-03 13:46:46'),
(161, 1, 1, NULL, NULL, NULL, NULL, NULL, 3, 'published', 'helio-sato', 0, 'closed', 0, '2016-10-03 13:46:53', '2016-10-03 13:46:53'),
(162, 1, 1, NULL, NULL, NULL, NULL, NULL, 3, 'published', 'jun-fukushima', 0, 'closed', 0, '2016-10-03 13:47:02', '2016-10-03 13:47:02'),
(163, 1, 1, NULL, NULL, NULL, NULL, NULL, 3, 'published', 'leandro-dutra', 0, 'closed', 0, '2016-10-03 13:47:12', '2016-10-03 13:47:12'),
(164, 1, 1, NULL, NULL, NULL, NULL, NULL, 3, 'published', 'lucca-musolin', 0, 'closed', 0, '2016-10-03 13:47:22', '2016-10-03 13:47:22'),
(165, 1, 1, NULL, NULL, NULL, NULL, NULL, 3, 'published', 'marcel-godoy', 0, 'closed', 0, '2016-10-03 13:47:32', '2016-10-03 13:47:32'),
(166, 1, 1, NULL, NULL, NULL, NULL, NULL, 3, 'published', 'marcelo-franca', 0, 'closed', 0, '2016-10-03 13:50:41', '2016-10-03 13:50:41'),
(167, 1, 1, NULL, NULL, NULL, NULL, NULL, 3, 'published', 'nei-prado', 0, 'closed', 0, '2016-10-03 13:50:52', '2016-10-03 13:50:52'),
(168, 1, 1, NULL, NULL, NULL, NULL, NULL, 3, 'published', 'reinaldo-stein', 0, 'closed', 0, '2016-10-03 13:51:01', '2016-10-03 13:51:01'),
(169, 1, 1, NULL, NULL, NULL, NULL, NULL, 3, 'published', 'rodrigo-aguiliani', 0, 'closed', 0, '2016-10-03 13:51:11', '2016-10-03 13:51:11'),
(170, 1, 1, NULL, NULL, NULL, NULL, NULL, 3, 'published', 'rodrigo-teofilo', 0, 'closed', 0, '2016-10-03 13:51:19', '2016-10-03 13:51:19'),
(171, 1, 1, NULL, NULL, NULL, NULL, NULL, 3, 'published', 'sergio-rodrigues', 0, 'closed', 0, '2016-10-03 13:51:27', '2016-10-03 13:51:27'),
(172, 1, 1, NULL, NULL, NULL, NULL, NULL, 3, 'published', 'valter', 0, 'closed', 0, '2016-10-03 13:51:36', '2016-10-03 13:51:36'),
(173, 1, 1, 'Tênis', NULL, NULL, NULL, NULL, 6, 'published', 'tenis', 0, 'closed', 0, '2016-10-03 13:56:08', '2016-10-03 13:56:08'),
(175, 1, 1, 'Treinamento de Alto Rendimento', 'A TennisSquare oferece bolsa de treinamento para campeões.', '<p></p><p>A <strong>Academia TennisSquare</strong> está lançando um programa \r\nde Treinamento de Alto Rendimento com bolsas de estudo gratuitas e \r\ncusteio de viagens para disputa de torneios nacionais e internacionais. O\r\n programa que terá início em fevereiro, irá promover um torneio seletivo\r\n em janeiro com o objetivo de selecionar atletas que almejam o \r\nprofissionalismo ou ingresso em universidade americanas.</p>\r\n<p><strong><br>\r\nPremiação do torneio para os atletas em transição (categoria 17/18/19/20 anos completos)</strong></p><p></p><ul><li>Primeiro colocado no torneio seletivo: <br>R$ 1.800,00/Mês para despesas pessoais + programa de treinamento integral gratuito.</li></ul><ul><li>Segundo colocado no torneio seletivo: <br>R$ 1.200,00/Mês para despesas pessoais + programa de treinamento integral gratuito.</li></ul><ul><li>Terceiro e Quarto colocados no torneio seletivo: <br>R$ 500,00/Mês para \r\ndespesas pessoais + programa de treinamento integral gratuito.</li></ul><ul><li>Quadrifinalista:<br>desconto de 20% na mensalidade do programa de treinamento integral.<br></li></ul><p></p>\r\n\r\n\r\n\r\n<p><strong>Premiação do torneio para os atletas da categoria 12/14/16 anos</strong></p>\r\n<p></p><ul><li>Primeiro colocado em cada categoria: <br>programa de treinamento gratuito.</li></ul><ul><li>Segundo colocado em cada categoria: <br>desconto de 40% na mensalidade do programa de treinamento treinamento.</li></ul><ul><li>Oitavo finalista em cada categoria: <br>desconto de 20% na mensalidade do programa de treinamento.<br></li></ul><p></p>\r\n\r\n\r\n<p>Descontos especiais para atletas ranqueados na CBT/FPT.</p>\r\n\r\n<br><p></p>', NULL, NULL, 2, 'published', 'treinamento-de-alto-rendimento15GfGafoaK', 0, 'closed', 0, '2016-10-03 18:21:39', '2016-10-03 18:21:39'),
(176, 1, 1, 'A TennisSquare', 'A TennisSquare', '<p></p><h2>Tradicional academia de tênis no bairro do Ipiranga, em São Paulo</h2>\r\n                    <p>Fundada em 1980, passou recentemente por reformas para adaptar suas instalações à prática do tênis moderno. A Academia conta com seis quadras de tênis, sendo duas de saibro cobertas e quatro quadras de piso rápido descobertas.</p>\r\n                     <p></p><p><b></b>Temos<b></b> uma equipe\r\nde professores e instrutores com cursos de formação e especialização em\r\ndiversos níveis de tênis, com orientação para preparo físico e nutricional.\r\nAlém de jogar tênis, aqui você pode fazer musculação, treinamento funcional,\r\npilates, yoga, spinning e outras aulas de academia, tudo em um só lugar.</p>\r\n\r\n<p>Para a recuperação física e tratamento de lesões existentes\r\nou pré-existentes, a TennisSquare disponibiliza uma sala com equipamentos\r\nespecíficos de fisioterapia.</p>\r\n\r\n\r\n\r\n\r\n\r\n<p> </p><br><p></p>', NULL, NULL, 1, 'published', 'a-tennissquare', 0, 'closed', 0, '2016-10-04 18:40:49', '2016-10-04 18:40:49'),
(177, 1, 1, '', NULL, NULL, NULL, NULL, 8, 'published', 'lnrvjpyoss', 0, 'closed', 0, '2016-10-10 02:43:57', '2016-10-10 02:43:57'),
(178, 1, 1, '', NULL, NULL, NULL, NULL, 8, 'published', 'lc60mutxzs', 0, 'closed', 0, '2016-10-10 02:44:12', '2016-10-10 02:44:12'),
(179, 1, 1, '', NULL, NULL, NULL, NULL, 8, 'published', 'jjs3zcgcp2', 0, 'closed', 0, '2016-10-10 02:44:25', '2016-10-10 02:44:25'),
(180, 1, 1, '', NULL, NULL, NULL, NULL, 8, 'published', '451mw5gc5a', 0, 'closed', 0, '2016-10-10 02:44:34', '2016-10-10 02:44:34'),
(181, 1, 1, '', NULL, NULL, NULL, NULL, 8, 'published', 'hfii5hhprv', 0, 'closed', 0, '2016-10-10 02:44:43', '2016-10-10 02:44:43'),
(182, 1, 1, '', NULL, NULL, NULL, NULL, 8, 'published', '753sgfmxpd', 0, 'closed', 0, '2016-10-10 02:44:54', '2016-10-10 02:44:54'),
(184, 1, 1, '', NULL, NULL, NULL, NULL, 8, 'published', '7zciv1linu', 0, 'closed', 0, '2016-10-10 02:45:13', '2016-10-10 02:45:13'),
(185, 1, 1, '', NULL, NULL, NULL, NULL, 8, 'published', 'wio8dx0uoo', 0, 'closed', 0, '2016-10-10 02:47:44', '2016-10-10 02:47:44'),
(186, 1, 1, '', NULL, NULL, NULL, NULL, 8, 'published', 'batyzttpxv', 0, 'closed', 0, '2016-10-10 02:47:57', '2016-10-10 02:47:57'),
(187, 1, 1, '', NULL, NULL, NULL, NULL, 8, 'published', 'b3fwdacmpx', 0, 'closed', 0, '2016-10-10 02:48:07', '2016-10-10 02:48:07'),
(188, 1, 1, '', NULL, NULL, NULL, NULL, 8, 'published', 'vqnn4hsmms', 0, 'closed', 0, '2016-10-10 02:48:20', '2016-10-10 02:48:20'),
(189, 1, 1, '', NULL, NULL, NULL, NULL, 8, 'published', 'on7igard22', 0, 'closed', 0, '2016-10-10 02:48:32', '2016-10-10 02:48:32'),
(190, 1, 1, '', NULL, NULL, NULL, NULL, 8, 'published', '22fevnj3jg', 0, 'closed', 0, '2016-10-10 02:48:43', '2016-10-10 02:48:43'),
(191, 1, 1, '', NULL, NULL, NULL, NULL, 8, 'published', 'go5jwpgzxu', 0, 'closed', 0, '2016-10-10 02:48:51', '2016-10-10 02:48:51'),
(192, 1, 1, '', NULL, NULL, NULL, NULL, 8, 'published', 'zsnfnhkqsy', 0, 'closed', 0, '2016-10-10 02:48:59', '2016-10-10 02:48:59'),
(193, 1, 1, '', NULL, NULL, NULL, NULL, 8, 'published', 'uirpgdb86n', 0, 'closed', 0, '2016-10-10 02:49:07', '2016-10-10 02:49:07'),
(194, 1, 1, '', NULL, NULL, NULL, NULL, 8, 'published', 'dgwvyucrsb', 0, 'closed', 0, '2016-10-10 02:49:15', '2016-10-10 02:49:15'),
(195, 1, 1, '', NULL, NULL, NULL, NULL, 8, 'published', 'zqei77sine', 0, 'closed', 0, '2016-10-10 02:49:23', '2016-10-10 02:49:23'),
(196, 1, 1, '', NULL, NULL, NULL, NULL, 8, 'published', 'nah8ojw75x', 0, 'closed', 0, '2016-10-10 02:49:31', '2016-10-10 02:49:31'),
(197, 1, 1, '', NULL, NULL, NULL, NULL, 8, 'published', 'jtfulj8bk8', 0, 'closed', 0, '2016-10-10 02:49:38', '2016-10-10 02:49:38'),
(198, 1, 1, '', NULL, NULL, NULL, NULL, 8, 'published', 'fdtbhru0b4', 0, 'closed', 0, '2016-10-10 02:49:46', '2016-10-10 02:49:46'),
(199, 1, 1, '', NULL, NULL, NULL, NULL, 8, 'published', 'myvlxvoi6n', 0, 'closed', 0, '2016-10-10 02:49:53', '2016-10-10 02:49:53'),
(200, 1, 1, '', NULL, NULL, NULL, NULL, 8, 'published', '7mip6srxyf', 0, 'closed', 0, '2016-10-10 02:50:02', '2016-10-10 02:50:02'),
(201, 1, 1, '', NULL, NULL, NULL, NULL, 8, 'published', 'yub77zi0jh', 0, 'closed', 0, '2016-10-10 02:50:10', '2016-10-10 02:50:10'),
(202, 1, 1, '', NULL, NULL, NULL, NULL, 8, 'published', 'iy3ehyekyf', 0, 'closed', 0, '2016-10-10 02:50:21', '2016-10-10 02:50:21'),
(203, 1, 1, '', NULL, NULL, NULL, NULL, 8, 'published', 'eyhakgi7ns', 0, 'closed', 0, '2016-10-10 02:50:29', '2016-10-10 02:50:29');

-- --------------------------------------------------------

--
-- Estrutura para tabela `m2p_content_meta`
--

CREATE TABLE `m2p_content_meta` (
  `id` int(10) UNSIGNED NOT NULL,
  `content_id` int(10) UNSIGNED NOT NULL,
  `key` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `value` json DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Fazendo dump de dados para tabela `m2p_content_meta`
--

INSERT INTO `m2p_content_meta` (`id`, `content_id`, `key`, `value`, `created_at`, `updated_at`) VALUES
(2, 2, 'taxonomies', '{"category": "2"}', '2016-09-06 00:09:56', '2016-10-03 13:19:15'),
(3, 3, 'taxonomies', '{"category": "2"}', '2016-09-06 00:15:23', '2016-09-28 20:20:57'),
(5, 5, 'taxonomies', '{"category": "2"}', '2016-09-06 00:18:58', '2016-09-30 20:34:22'),
(6, 6, 'taxonomies', '{"category": "2"}', '2016-09-06 00:20:46', '2016-09-28 20:00:59'),
(7, 7, 'taxonomies', '{"category": "2"}', '2016-09-06 00:22:16', '2016-10-03 17:32:25'),
(8, 8, 'taxonomies', '{"category": "3"}', '2016-09-06 01:15:47', '2016-10-05 11:44:16'),
(9, 9, 'taxonomies', '{"category": "3"}', '2016-09-06 01:17:09', '2016-09-28 20:28:57'),
(10, 10, 'taxonomies', '{"category": "3"}', '2016-09-06 01:18:24', '2016-09-28 20:03:19'),
(11, 11, 'taxonomies', '{"category": "3"}', '2016-09-06 01:20:55', '2016-09-28 20:34:41'),
(12, 12, 'taxonomies', '{"category": "3"}', '2016-09-06 01:22:39', '2016-09-30 20:27:38'),
(28, 28, 'plan', '{"cost": "232.00"}', '2016-09-15 00:06:12', '2016-09-30 19:19:33'),
(29, 29, 'plan', '{"cost": "418.00"}', '2016-09-15 00:15:06', '2016-09-30 19:18:39'),
(30, 30, 'plan', '{"cost": "696.00"}', '2016-09-15 00:15:30', '2016-09-30 19:17:33'),
(36, 36, 'plan', '{"cost": "316.00"}', '2016-09-21 20:51:55', '2016-09-28 19:59:10'),
(37, 37, 'plan', '{"cost": "421.00"}', '2016-09-21 20:52:25', '2016-09-28 19:58:04'),
(38, 38, 'plan', '{"cost": "380.00"}', '2016-09-21 20:53:06', '2016-09-30 19:17:45'),
(41, 48, 'taxonomies', '{"category": ""}', '2016-09-28 20:02:29', '2016-09-28 20:02:37'),
(43, 50, 'player', '{"name": "JULIANO BOSCO", "rank": "10", "points": "85"}', '2016-09-28 20:10:08', '2016-10-03 22:22:05'),
(44, 51, 'player', '{"name": "TIAGO SOUZA", "rank": "10", "points": "76"}', '2016-09-28 20:10:25', '2016-09-28 20:10:25'),
(45, 52, 'player', '{"name": "CARLOS KORNIUSZA", "rank": "10", "points": "75"}', '2016-09-28 20:10:34', '2016-09-28 20:10:34'),
(46, 53, 'player', '{"name": "PEDRO BOSCO", "rank": "10", "points": "72"}', '2016-09-28 20:10:46', '2016-09-28 20:10:46'),
(47, 54, 'player', '{"name": "EDUARDO KAWAKAMI", "rank": "10", "points": "53"}', '2016-09-28 20:10:56', '2016-09-28 20:10:56'),
(48, 55, 'player', '{"name": "JOAO MATSUMOTO", "rank": "8", "points": "112"}', '2016-09-28 20:11:13', '2016-09-28 20:11:13'),
(49, 56, 'player', '{"name": "EDU CORSI", "rank": "8", "points": "102"}', '2016-09-28 20:11:40', '2016-09-28 20:11:40'),
(50, 57, 'player', '{"name": "ERIK ICHIKAWA", "rank": "8", "points": "100"}', '2016-09-28 20:11:51', '2016-09-28 20:11:51'),
(51, 58, 'player', '{"name": "NILSON MAEDA", "rank": "8", "points": "83"}', '2016-09-28 20:12:03', '2016-09-28 20:12:03'),
(52, 59, 'player', '{"name": "ALEXANDRE CORREA", "rank": "8", "points": "79"}', '2016-09-28 20:12:20', '2016-09-28 20:12:20'),
(53, 60, 'player', '{"name": "LUCIANO OLIVEIRA", "rank": "6", "points": "33"}', '2016-09-28 20:12:43', '2016-09-28 20:12:43'),
(54, 61, 'player', '{"name": "EDUARDO TOLEDO", "rank": "6", "points": "30"}', '2016-09-28 20:12:53', '2016-09-28 20:12:53'),
(55, 62, 'player', '{"name": "VITOR KITAHARA", "rank": "6", "points": "30"}', '2016-09-28 20:13:04', '2016-09-28 20:13:04'),
(56, 63, 'player', '{"name": "BENEDITO MARIO", "rank": "6", "points": "29"}', '2016-09-28 20:13:20', '2016-09-28 20:13:20'),
(57, 64, 'player', '{"name": "MARIO KROLLER", "rank": "6", "points": "28"}', '2016-09-28 20:13:31', '2016-09-28 20:13:31'),
(58, 65, 'taxonomies', '{"category": "3"}', '2016-09-30 20:11:09', '2016-10-04 14:47:36'),
(59, 66, 'taxonomies', '{"category": "3"}', '2016-09-30 20:13:45', '2016-09-30 20:13:45'),
(60, 67, 'taxonomies', '{"category": "3"}', '2016-09-30 20:15:29', '2016-09-30 20:15:29'),
(61, 68, 'taxonomies', '{"category": "3"}', '2016-09-30 20:19:09', '2016-10-03 17:37:01'),
(62, 69, 'taxonomies', '{"category": "3"}', '2016-09-30 20:19:56', '2016-10-03 17:41:00'),
(63, 70, 'taxonomies', '{"category": "3"}', '2016-09-30 20:20:27', '2016-10-05 13:37:31'),
(64, 71, 'player', '{"name": "PAULO KIMURA", "rank": "6", "points": "19"}', '2016-09-30 20:41:44', '2016-09-30 20:41:44'),
(65, 72, 'player', '{"name": "LEANDRO SILVA", "rank": "6", "points": "13"}', '2016-09-30 20:41:59', '2016-09-30 20:41:59'),
(66, 73, 'player', '{"name": "LEANDRO TURBINO", "rank": "6", "points": "13"}', '2016-09-30 20:42:15', '2016-09-30 20:42:15'),
(67, 74, 'player', '{"name": "OTAVIO G FACHIN", "rank": "6", "points": "13"}', '2016-09-30 20:42:25', '2016-09-30 20:42:26'),
(68, 75, 'player', '{"name": "WILLIAN SATO", "rank": "6", "points": "12"}', '2016-09-30 20:42:36', '2016-09-30 20:42:36'),
(69, 76, 'player', '{"name": "MARCIO PAGLIA", "rank": "6", "points": "10"}', '2016-09-30 20:42:49', '2016-09-30 20:42:49'),
(70, 77, 'player', '{"name": "CECERO", "rank": "6", "points": "10"}', '2016-09-30 20:43:03', '2016-09-30 20:43:03'),
(71, 78, 'player', '{"name": "LUIS MUNHOZ", "rank": "6", "points": "10"}', '2016-09-30 20:43:13', '2016-09-30 20:43:13'),
(72, 79, 'player', '{"name": "MARCELO URBANO", "rank": "6", "points": "10"}', '2016-09-30 20:43:22', '2016-09-30 20:43:22'),
(73, 80, 'player', '{"name": "ROBERTO LUIS", "rank": "6", "points": "10"}', '2016-09-30 20:43:32', '2016-09-30 20:43:33'),
(74, 81, 'player', '{"name": "ROGERIO BOTH", "rank": "6", "points": "10"}', '2016-09-30 20:43:42', '2016-09-30 20:43:42'),
(75, 82, 'player', '{"name": "VALTER TROTTA", "rank": "6", "points": "10"}', '2016-09-30 20:43:52', '2016-09-30 20:43:52'),
(76, 83, 'player', '{"name": "RODRIGUES ARJONAS", "rank": "6", "points": "9"}', '2016-09-30 20:44:01', '2016-09-30 20:44:01'),
(77, 84, 'player', '{"name": "ROGERIO ONIL", "rank": "6", "points": "9"}', '2016-09-30 20:44:13', '2016-09-30 20:44:13'),
(78, 85, 'player', '{"name": "IGOR", "rank": "6", "points": "3"}', '2016-09-30 20:44:23', '2016-09-30 20:44:23'),
(79, 86, 'player', '{"name": "DANIEL  ABDUL", "rank": "6", "points": "0"}', '2016-09-30 20:44:33', '2016-09-30 20:44:33'),
(80, 87, 'player', '{"name": "DANIEL OLIVEIRA AGUIAR", "rank": "6", "points": "0"}', '2016-09-30 20:44:43', '2016-09-30 20:44:43'),
(81, 88, 'player', '{"name": "DENIS TELLES GAIA", "rank": "6", "points": "0"}', '2016-09-30 20:44:53', '2016-09-30 20:44:53'),
(82, 89, 'player', '{"name": "EDUARDO KATSURAGAWA", "rank": "6", "points": "0"}', '2016-09-30 20:45:11', '2016-09-30 20:45:11'),
(83, 90, 'player', '{"name": "FERNANDO BELTRANI", "rank": "6", "points": "0"}', '2016-09-30 20:45:22', '2016-09-30 20:45:22'),
(84, 91, 'player', '{"name": "FERNANDO LOSSO", "rank": "6", "points": "0"}', '2016-09-30 20:45:32', '2016-09-30 20:45:32'),
(85, 92, 'player', '{"name": "GUILHERME MEDEIROS", "rank": "6", "points": "0"}', '2016-09-30 20:45:42', '2016-09-30 20:45:42'),
(86, 93, 'player', '{"name": "IVAN S CODO", "rank": "6", "points": "0"}', '2016-09-30 20:45:51', '2016-09-30 20:45:51'),
(87, 94, 'player', '{"name": "MOACIR PASSO", "rank": "6", "points": "0"}', '2016-09-30 20:46:01', '2016-09-30 20:46:01'),
(88, 95, 'player', '{"name": "RICHARD FERRARESI", "rank": "6", "points": "0"}', '2016-09-30 20:46:11', '2016-09-30 20:46:11'),
(89, 96, 'player', '{"name": "ROBERTO BICUDO", "rank": "6", "points": "0"}', '2016-09-30 20:46:25', '2016-09-30 20:46:25'),
(90, 97, 'player', '{"name": "THALES RANDAZZO", "rank": "6", "points": "0"}', '2016-09-30 20:46:46', '2016-09-30 20:46:46'),
(91, 98, 'player', '{"name": "WLADIMIR PEREIRA", "rank": "6", "points": "0"}', '2016-09-30 20:46:55', '2016-09-30 20:46:55'),
(92, 99, 'player', '{"name": "DENIS CASTANHO", "rank": "8", "points": "60"}', '2016-09-30 20:50:14', '2016-09-30 20:50:14'),
(93, 100, 'player', '{"name": "GUSTAVO MORENO", "rank": "8", "points": "52"}', '2016-09-30 20:50:26', '2016-09-30 20:50:26'),
(94, 101, 'player', '{"name": "JARDEL CORREA", "rank": "8", "points": "42"}', '2016-09-30 20:50:38', '2016-09-30 20:50:38'),
(95, 102, 'player', '{"name": "JOSE FLAVIO", "rank": "8", "points": "39"}', '2016-09-30 20:50:48', '2016-09-30 20:50:48'),
(96, 103, 'player', '{"name": "ERMESON BUASSALI", "rank": "8", "points": "33"}', '2016-09-30 20:50:57', '2016-09-30 20:50:57'),
(97, 104, 'player', '{"name": "FABIAN", "rank": "8", "points": "32"}', '2016-09-30 20:51:07', '2016-09-30 20:51:07'),
(98, 105, 'player', '{"name": "CAIO CRACCO", "rank": "8", "points": "26"}', '2016-09-30 20:51:16', '2016-09-30 20:51:16'),
(99, 106, 'player', '{"name": "FRANSCIS MATSUDA", "rank": "8", "points": "23"}', '2016-09-30 20:51:25', '2016-09-30 20:51:25'),
(100, 107, 'player', '{"name": "GUILHERME CUNHA", "rank": "8", "points": "23"}', '2016-09-30 20:51:35', '2016-09-30 20:51:35'),
(101, 108, 'player', '{"name": "EDUARDO GERMANI", "rank": "8", "points": "22"}', '2016-09-30 20:51:45', '2016-09-30 20:51:45'),
(102, 109, 'player', '{"name": "RODRIGO BEDIN", "rank": "8", "points": "22"}', '2016-09-30 20:51:54', '2016-09-30 20:51:54'),
(103, 110, 'player', '{"name": "FABRICIO CARVALHO", "rank": "8", "points": "20"}', '2016-09-30 20:52:03', '2016-09-30 20:52:03'),
(104, 111, 'player', '{"name": "JULIO VELOSO", "rank": "8", "points": "19"}', '2016-09-30 20:52:16', '2016-09-30 20:52:16'),
(105, 112, 'player', '{"name": "ALAN EVANGELISTA", "rank": "8", "points": "16"}', '2016-09-30 20:52:25', '2016-09-30 20:52:25'),
(106, 113, 'player', '{"name": "MASSAMI KOBO", "rank": "8", "points": "13"}', '2016-09-30 20:52:34', '2016-09-30 20:52:34'),
(107, 114, 'player', '{"name": "MICHEL FURLAN", "rank": "8", "points": "13"}', '2016-09-30 20:52:47', '2016-09-30 20:52:47'),
(108, 115, 'player', '{"name": "MARCELO MASSANO", "rank": "8", "points": "12"}', '2016-09-30 20:52:59', '2016-09-30 20:52:59'),
(109, 116, 'player', '{"name": "DAGMAR RIBEIRO", "rank": "8", "points": "10"}', '2016-09-30 20:53:08', '2016-09-30 20:53:08'),
(110, 117, 'player', '{"name": "DIOGO CONTE", "rank": "8", "points": "10"}', '2016-09-30 20:53:17', '2016-09-30 20:53:17'),
(111, 118, 'player', '{"name": "MARCELO URBANO", "rank": "8", "points": "9"}', '2016-09-30 20:53:26', '2016-09-30 20:53:26'),
(112, 119, 'player', '{"name": "ADILSON ARRUDA", "rank": "10", "points": "6"}', '2016-09-30 20:53:51', '2016-09-30 20:53:51'),
(113, 120, 'player', '{"name": "SERGIO R DA SILVA", "rank": "8", "points": "6"}', '2016-09-30 20:54:00', '2016-09-30 20:54:00'),
(114, 121, 'player', '{"name": "ALESSANDRO FERNANDES", "rank": "8", "points": "3"}', '2016-09-30 20:54:09', '2016-09-30 20:54:09'),
(115, 122, 'player', '{"name": "DACIO OGATA", "rank": "8", "points": "3"}', '2016-09-30 20:54:19', '2016-09-30 20:54:19'),
(116, 123, 'player', '{"name": "MARLON AMORIM", "rank": "8", "points": "3"}', '2016-09-30 20:54:29', '2016-09-30 20:54:29'),
(117, 124, 'player', '{"name": "PABLO SUZUKI", "rank": "8", "points": "3"}', '2016-09-30 20:54:38', '2016-09-30 20:54:38'),
(118, 125, 'player', '{"name": "ALEXANDRE MORAES", "rank": "8", "points": "0"}', '2016-09-30 20:54:47', '2016-09-30 20:54:47'),
(119, 126, 'player', '{"name": "ALEXANDRE SOUZA", "rank": "8", "points": "0"}', '2016-09-30 20:54:56', '2016-09-30 20:54:56'),
(120, 127, 'player', '{"name": "ANDRE PINTO", "rank": "8", "points": "0"}', '2016-09-30 20:55:04', '2016-09-30 20:55:04'),
(121, 128, 'player', '{"name": "ANDREY VICOCKAS", "rank": "8", "points": "0"}', '2016-09-30 20:55:13', '2016-09-30 20:55:13'),
(122, 129, 'player', '{"name": "ARNALDO MARINHO", "rank": "8", "points": "0"}', '2016-09-30 20:55:22', '2016-09-30 20:55:22'),
(123, 130, 'player', '{"name": "BRUNO BOSCOLO", "rank": "8", "points": "0"}', '2016-09-30 20:55:32', '2016-09-30 20:55:32'),
(124, 131, 'player', '{"name": "EDUARDO QUINTANA", "rank": "8", "points": "0"}', '2016-09-30 20:55:40', '2016-09-30 20:55:40'),
(125, 132, 'player', '{"name": "ELOY NICOTERA", "rank": "8", "points": "0"}', '2016-09-30 20:55:53', '2016-09-30 20:55:53'),
(126, 133, 'player', '{"name": "DANILO SAITO", "rank": "10", "points": "50"}', '2016-10-03 13:41:41', '2016-10-03 13:41:41'),
(127, 134, 'player', '{"name": "CELSO IGARACHI", "rank": "10", "points": "39"}', '2016-10-03 13:41:52', '2016-10-03 13:41:52'),
(128, 135, 'player', '{"name": "JOSE MANUEL PARADA", "rank": "10", "points": "39"}', '2016-10-03 13:42:07', '2016-10-03 13:42:07'),
(129, 136, 'player', '{"name": "FERNANDO CARDOSO", "rank": "10", "points": "36"}', '2016-10-03 13:42:16', '2016-10-03 13:42:16'),
(130, 137, 'player', '{"name": "DANILO SALERNO", "rank": "10", "points": "31"}', '2016-10-03 13:42:33', '2016-10-03 13:42:33'),
(131, 138, 'player', '{"name": "EDUARDO NEVES", "rank": "10", "points": "30"}', '2016-10-03 13:42:43', '2016-10-03 13:42:43'),
(132, 139, 'player', '{"name": "MATHEUS ALMEIDA", "rank": "10", "points": "30"}', '2016-10-03 13:42:53', '2016-10-03 13:42:53'),
(133, 140, 'player', '{"name": "EDNEY CABRAL", "rank": "10", "points": "29"}', '2016-10-03 13:43:16', '2016-10-03 13:43:16'),
(134, 141, 'player', '{"name": "THIBAULT", "rank": "10", "points": "23"}', '2016-10-03 13:43:27', '2016-10-03 13:43:27'),
(135, 142, 'player', '{"name": "ANDRE MASSIS", "rank": "10", "points": "13"}', '2016-10-03 13:43:39', '2016-10-03 13:43:39'),
(136, 143, 'player', '{"name": "DANILO NORCIA", "rank": "10", "points": "13"}', '2016-10-03 13:43:48', '2016-10-03 13:43:48'),
(137, 144, 'player', '{"name": "GUILHERME TELES", "rank": "10", "points": "13"}', '2016-10-03 13:43:58', '2016-10-03 13:43:58'),
(138, 145, 'player', '{"name": "OTONIEL BARBADO", "rank": "10", "points": "13"}', '2016-10-03 13:44:07', '2016-10-03 13:44:07'),
(139, 146, 'player', '{"name": "EDUARDO SILVEIRA", "rank": "10", "points": "10"}', '2016-10-03 13:44:17', '2016-10-03 13:44:17'),
(140, 147, 'player', '{"name": "FABIO CARDOSO", "rank": "10", "points": "10"}', '2016-10-03 13:44:26', '2016-10-03 13:44:26'),
(141, 148, 'player', '{"name": "NAIRAN PEDREIRA", "rank": "10", "points": "10"}', '2016-10-03 13:44:35', '2016-10-03 13:44:35'),
(142, 149, 'player', '{"name": "LEANDRO SANDAY", "rank": "10", "points": "9"}', '2016-10-03 13:44:47', '2016-10-03 13:44:47'),
(143, 150, 'player', '{"name": "JULIO BAIOCATO", "rank": "10", "points": "6"}', '2016-10-03 13:44:58', '2016-10-03 13:44:58'),
(144, 151, 'player', '{"name": "EDER MIRANDA", "rank": "10", "points": "3"}', '2016-10-03 13:45:06', '2016-10-03 13:45:06'),
(145, 152, 'player', '{"name": "EDUARDO YANO", "rank": "10", "points": "3"}', '2016-10-03 13:45:15', '2016-10-03 13:45:15'),
(146, 153, 'player', '{"name": "FLAVIO SHITARA", "rank": "10", "points": "3"}', '2016-10-03 13:45:29', '2016-10-03 13:45:29'),
(147, 154, 'player', '{"name": "SHOZO HAYAKAWA", "rank": "10", "points": "3"}', '2016-10-03 13:45:40', '2016-10-03 13:45:40'),
(148, 155, 'player', '{"name": "YASUNORI SUZUE", "rank": "10", "points": "3"}', '2016-10-03 13:45:56', '2016-10-03 13:45:56'),
(149, 156, 'player', '{"name": "WAGNER RODRIGUES", "rank": "10", "points": "3"}', '2016-10-03 13:46:05', '2016-10-03 13:46:05'),
(150, 157, 'player', '{"name": "ALBERTO BURANI", "rank": "10", "points": "0"}', '2016-10-03 13:46:14', '2016-10-03 13:46:14'),
(151, 158, 'player', '{"name": "ANTONIO BLANCK", "rank": "10", "points": "0"}', '2016-10-03 13:46:23', '2016-10-03 13:46:24'),
(152, 159, 'player', '{"name": "FABRICIO CARVALHO", "rank": "10", "points": "0"}', '2016-10-03 13:46:33', '2016-10-03 13:46:33'),
(153, 160, 'player', '{"name": "GUSTAVO MATSUMOTO", "rank": "10", "points": "0"}', '2016-10-03 13:46:46', '2016-10-03 13:46:46'),
(154, 161, 'player', '{"name": "HELIO SATO", "rank": "10", "points": "0"}', '2016-10-03 13:46:53', '2016-10-03 13:46:53'),
(155, 162, 'player', '{"name": "JUN FUKUSHIMA", "rank": "10", "points": "0"}', '2016-10-03 13:47:02', '2016-10-03 13:47:02'),
(156, 163, 'player', '{"name": "LEANDRO DUTRA", "rank": "10", "points": "0"}', '2016-10-03 13:47:12', '2016-10-03 13:47:12'),
(157, 164, 'player', '{"name": "LUCCA MUSOLIN", "rank": "10", "points": "0"}', '2016-10-03 13:47:22', '2016-10-03 13:47:22'),
(158, 165, 'player', '{"name": "MARCEL GODOY", "rank": "10", "points": "0"}', '2016-10-03 13:47:32', '2016-10-03 13:47:32'),
(159, 166, 'player', '{"name": "MARCELO FRANÇA", "rank": "10", "points": "0"}', '2016-10-03 13:50:41', '2016-10-03 13:50:41'),
(160, 167, 'player', '{"name": "NEI PRADO", "rank": "10", "points": "0"}', '2016-10-03 13:50:52', '2016-10-03 13:50:52'),
(161, 168, 'player', '{"name": "REINALDO STEIN", "rank": "10", "points": "0"}', '2016-10-03 13:51:01', '2016-10-03 13:51:01'),
(162, 169, 'player', '{"name": "RODRIGO A.GUILIANI", "rank": "10", "points": "0"}', '2016-10-03 13:51:11', '2016-10-03 13:51:11'),
(163, 170, 'player', '{"name": "RODRIGO TEOFILO", "rank": "10", "points": "0"}', '2016-10-03 13:51:19', '2016-10-03 13:51:19'),
(164, 171, 'player', '{"name": "SERGIO RODRIGUES", "rank": "10", "points": "0"}', '2016-10-03 13:51:27', '2016-10-03 13:51:27'),
(165, 172, 'player', '{"name": "VALTER", "rank": "10", "points": "0"}', '2016-10-03 13:51:36', '2016-10-03 13:51:36'),
(166, 173, 'banner', '{"text": "Tênis"}', '2016-10-03 13:56:08', '2016-10-11 12:13:54'),
(168, 175, 'taxonomies', '{"category": ""}', '2016-10-03 18:21:39', '2016-10-03 18:21:39'),
(169, 176, 'taxonomies', '{"category": "1"}', '2016-10-04 18:40:49', '2016-10-04 18:40:49'),
(170, 47, 'banner', '{"text": "Pilates"}', '2016-10-11 12:14:00', '2016-10-11 12:14:00'),
(171, 46, 'banner', '{"text": "Fitness Center"}', '2016-10-11 12:14:06', '2016-10-11 12:14:06'),
(172, 44, 'banner', '{"text": "Musculação"}', '2016-10-11 12:14:12', '2016-10-11 12:14:12'),
(173, 43, 'banner', '{"text": "Desmotec"}', '2016-10-11 12:14:16', '2016-10-11 12:20:27');

-- --------------------------------------------------------

--
-- Estrutura para tabela `m2p_content_taxonomies`
--

CREATE TABLE `m2p_content_taxonomies` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `content_type_id` int(10) UNSIGNED NOT NULL,
  `capabilities` json DEFAULT NULL,
  `site_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Fazendo dump de dados para tabela `m2p_content_taxonomies`
--

INSERT INTO `m2p_content_taxonomies` (`id`, `name`, `slug`, `description`, `content_type_id`, `capabilities`, `site_id`, `created_at`, `updated_at`) VALUES
(1, 'tag', 'tag', 'Tags', 1, NULL, 1, '2016-09-05 00:00:00', NULL),
(2, 'category', 'category', 'Descrições', 1, NULL, 1, '2016-09-05 00:00:00', NULL),
(3, 'Rank', 'rank', 'Ranking', 3, NULL, 1, '2016-09-06 00:00:00', NULL);

-- --------------------------------------------------------

--
-- Estrutura para tabela `m2p_content_taxonomy_terms`
--

CREATE TABLE `m2p_content_taxonomy_terms` (
  `id` int(10) UNSIGNED NOT NULL,
  `content_taxonomy_id` int(10) UNSIGNED NOT NULL,
  `term` text COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Fazendo dump de dados para tabela `m2p_content_taxonomy_terms`
--

INSERT INTO `m2p_content_taxonomy_terms` (`id`, `content_taxonomy_id`, `term`, `slug`, `description`, `created_at`, `updated_at`) VALUES
(1, 2, 'Institucional', 'institucional', NULL, '2016-09-05 23:56:23', '2016-09-11 22:56:23'),
(2, 2, 'Academia de Tênis', 'academia-de-tenis', 'A TennisSquare possui 2 quadras de saibro cobertas e 4 quadras de piso rápido descobertas, excelente iluminação com refletores de alta potência. Aqui você pode jogar tênis com uma estrutura adequada para a prática deste esporte.\r\n', '2016-09-05 23:57:16', '2016-09-30 18:25:04'),
(3, 2, 'Fitness Center', 'fitness-center', 'Espaço com equipamentos modernos, aberto a todas as pessoas interessadas em aprimorar o condicionamento físico, e também para os atletas que procuram o aperfeiçoamento na prática do tênis. Contamos com profissionais qualificados para elaborar treinos individualizados.', '2016-09-05 23:57:51', '2016-09-26 02:57:46'),
(6, 3, 'Principiante masculino', 'principiante-masculino', NULL, '2016-09-06 04:22:23', '2016-09-06 04:22:23'),
(7, 3, 'Principiante feminino', 'principiante-feminino', NULL, '2016-09-06 04:22:23', '2016-09-06 04:22:23'),
(8, 3, 'Intermediário masculino', 'intermediario-masculino', NULL, '2016-09-06 04:22:23', '2016-09-06 04:22:23'),
(9, 3, 'Intermediário feminino', 'intermediario-feminino', NULL, '2016-09-06 04:22:23', '2016-09-06 04:22:23'),
(10, 3, 'Avançado masculino', 'avancado-masculino', NULL, '2016-09-06 04:22:23', '2016-09-06 04:22:23'),
(11, 3, 'Avançado feminino', 'avancado-feminino', NULL, '2016-09-06 04:22:23', '2016-09-06 04:22:23');

-- --------------------------------------------------------

--
-- Estrutura para tabela `m2p_content_types`
--

CREATE TABLE `m2p_content_types` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `slug` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `site_id` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Fazendo dump de dados para tabela `m2p_content_types`
--

INSERT INTO `m2p_content_types` (`id`, `name`, `description`, `slug`, `site_id`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'page', 'Páginas', 'pagina', NULL, '2016-09-05 11:11:44', '2016-09-05 11:11:44', NULL),
(2, 'post', 'Square News', 'post', NULL, '2016-09-05 11:11:44', '2016-09-05 11:11:44', NULL),
(3, 'player', 'Jogadores', 'jogador', 1, '2016-09-06 00:00:00', NULL, NULL),
(4, 'galery', 'Galeria', 'galeria', 1, '2016-09-12 00:00:00', NULL, NULL),
(5, 'plan', 'Planos', 'plano', 1, '2016-09-14 00:00:00', NULL, NULL),
(6, 'banner', 'Banners', 'banner', NULL, '2016-09-23 00:00:00', '2016-09-23 00:00:00', NULL),
(7, 'partner', 'Parceiros', 'parceiro', NULL, '2016-10-04 03:00:00', NULL, NULL),
(8, 'infrastructure', 'Estrutura', 'estrutura', 1, '2016-10-01 00:00:00', '2016-10-01 00:00:00', NULL);

-- --------------------------------------------------------

--
-- Estrutura para tabela `m2p_options`
--

CREATE TABLE `m2p_options` (
  `id` int(10) UNSIGNED NOT NULL,
  `key` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `value` json NOT NULL,
  `is_public` tinyint(1) NOT NULL DEFAULT '0',
  `site_id` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Fazendo dump de dados para tabela `m2p_options`
--

INSERT INTO `m2p_options` (`id`, `key`, `value`, `is_public`, `site_id`, `created_at`, `updated_at`) VALUES
(1, 'm2p_public', '{"results_per_page": 10}', 1, NULL, '2016-09-05 11:11:44', '2016-09-05 11:11:44');

-- --------------------------------------------------------

--
-- Estrutura para tabela `m2p_permissions`
--

CREATE TABLE `m2p_permissions` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `display_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Fazendo dump de dados para tabela `m2p_permissions`
--

INSERT INTO `m2p_permissions` (`id`, `name`, `display_name`, `description`, `created_at`, `updated_at`) VALUES
(1, 'list_sites', NULL, NULL, '2016-09-05 11:11:44', '2016-09-05 11:11:44'),
(2, 'create_sites', NULL, NULL, '2016-09-05 11:11:44', '2016-09-05 11:11:44'),
(3, 'edit_sites', NULL, NULL, '2016-09-05 11:11:44', '2016-09-05 11:11:44'),
(4, 'delete_sites', NULL, NULL, '2016-09-05 11:11:44', '2016-09-05 11:11:44'),
(5, 'assing_sites', NULL, NULL, '2016-09-05 11:11:44', '2016-09-05 11:11:44'),
(6, 'list_users', NULL, NULL, '2016-09-05 11:11:44', '2016-09-05 11:11:44'),
(7, 'create_users', NULL, NULL, '2016-09-05 11:11:44', '2016-09-05 11:11:44'),
(8, 'edit_users', NULL, NULL, '2016-09-05 11:11:44', '2016-09-05 11:11:44'),
(9, 'delete_users', NULL, NULL, '2016-09-05 11:11:44', '2016-09-05 11:11:44'),
(10, 'manage_categories', NULL, NULL, '2016-09-05 11:11:44', '2016-09-05 11:11:44'),
(11, 'manage_options', NULL, NULL, '2016-09-05 11:11:44', '2016-09-05 11:11:44'),
(12, 'edit_contents', NULL, NULL, '2016-09-05 11:11:44', '2016-09-05 11:11:44'),
(13, 'delete_contents', NULL, NULL, '2016-09-05 11:11:44', '2016-09-05 11:11:44'),
(14, 'edit_pages', NULL, NULL, '2016-09-05 11:11:44', '2016-09-05 11:11:44'),
(15, 'delete_pages', NULL, NULL, '2016-09-05 11:11:44', '2016-09-05 11:11:44'),
(16, 'edit_posts', NULL, NULL, '2016-09-05 11:11:44', '2016-09-05 11:11:44'),
(17, 'delete_posts', NULL, NULL, '2016-09-05 11:11:44', '2016-09-05 11:11:44'),
(18, 'edit_others_contents', NULL, NULL, '2016-09-05 11:11:44', '2016-09-05 11:11:44'),
(19, 'edit_others_pages', NULL, NULL, '2016-09-05 11:11:44', '2016-09-05 11:11:44'),
(20, 'edit_others_posts', NULL, NULL, '2016-09-05 11:11:44', '2016-09-05 11:11:44'),
(21, 'edit_published_contents', NULL, NULL, '2016-09-05 11:11:44', '2016-09-05 11:11:44'),
(22, 'edit_published_pages', NULL, NULL, '2016-09-05 11:11:44', '2016-09-05 11:11:44'),
(23, 'edit_published_posts', NULL, NULL, '2016-09-05 11:11:44', '2016-09-05 11:11:44'),
(24, 'delete_others_contents', NULL, NULL, '2016-09-05 11:11:44', '2016-09-05 11:11:44'),
(25, 'delete_others_pages', NULL, NULL, '2016-09-05 11:11:44', '2016-09-05 11:11:44'),
(26, 'delete_others_posts', NULL, NULL, '2016-09-05 11:11:44', '2016-09-05 11:11:44'),
(27, 'public_contents', NULL, NULL, '2016-09-05 11:11:44', '2016-09-05 11:11:44'),
(28, 'publish_pages', NULL, NULL, '2016-09-05 11:11:44', '2016-09-05 11:11:44'),
(29, 'publish_posts', NULL, NULL, '2016-09-05 11:11:44', '2016-09-05 11:11:44'),
(30, 'moderate_comments', NULL, NULL, '2016-09-05 11:11:44', '2016-09-05 11:11:44'),
(31, 'upload_files', NULL, NULL, '2016-09-05 11:11:44', '2016-09-05 11:11:44'),
(32, 'edit_files', NULL, NULL, '2016-09-05 11:11:44', '2016-09-05 11:11:44'),
(33, 'read', NULL, NULL, '2016-09-05 11:11:44', '2016-09-05 11:11:44');

-- --------------------------------------------------------

--
-- Estrutura para tabela `m2p_permission_role`
--

CREATE TABLE `m2p_permission_role` (
  `permission_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Fazendo dump de dados para tabela `m2p_permission_role`
--

INSERT INTO `m2p_permission_role` (`permission_id`, `role_id`) VALUES
(1, 1),
(2, 1),
(3, 1),
(4, 1),
(5, 1),
(6, 1),
(7, 1),
(8, 1),
(9, 1),
(10, 1),
(11, 1),
(12, 1),
(13, 1),
(14, 1),
(15, 1),
(16, 1),
(17, 1),
(18, 1),
(19, 1),
(20, 1),
(21, 1),
(22, 1),
(23, 1),
(24, 1),
(25, 1),
(26, 1),
(27, 1),
(28, 1),
(29, 1),
(30, 1),
(31, 1),
(32, 1),
(33, 1);

-- --------------------------------------------------------

--
-- Estrutura para tabela `m2p_roles`
--

CREATE TABLE `m2p_roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `display_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Fazendo dump de dados para tabela `m2p_roles`
--

INSERT INTO `m2p_roles` (`id`, `name`, `display_name`, `description`, `created_at`, `updated_at`) VALUES
(1, 'super-admin', 'Master', 'Alguém com acesso à administração de todos os sites e todos os recursos', '2016-09-05 11:11:44', '2016-09-05 11:11:44'),
(2, 'admin', 'Administrador', 'Alguém com acesso a todos os recursos de administração de um único site.', '2016-09-05 11:11:44', '2016-09-05 11:11:44'),
(3, 'editor', 'Editor', 'Alǵuem que pode publicar e gerir conteúdos de sua autoria ou de outros.', '2016-09-05 11:11:44', '2016-09-05 11:11:44'),
(4, 'author', 'Autor', 'Alguém que pode publicar e gerir seus próprios conteúdos.', '2016-09-05 11:11:44', '2016-09-05 11:11:44'),
(5, 'contributor', 'Contribuidor', 'Alguém que pode escrever e gerir seus próprios conteúdos mas não pode publicá-los.', '2016-09-05 11:11:44', '2016-09-05 11:11:44');

-- --------------------------------------------------------

--
-- Estrutura para tabela `m2p_role_user`
--

CREATE TABLE `m2p_role_user` (
  `user_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Fazendo dump de dados para tabela `m2p_role_user`
--

INSERT INTO `m2p_role_user` (`user_id`, `role_id`) VALUES
(1, 1);

-- --------------------------------------------------------

--
-- Estrutura para tabela `m2p_sites`
--

CREATE TABLE `m2p_sites` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `title` text COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` char(20) COLLATE utf8_unicode_ci NOT NULL,
  `is_default` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Fazendo dump de dados para tabela `m2p_sites`
--

INSERT INTO `m2p_sites` (`id`, `name`, `title`, `description`, `url`, `slug`, `is_default`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'TennisSquare', 'Tennis Square', 'A default website', 'www.tennissquare.com.br', 'default', 1, '2016-09-05 11:11:44', '2016-09-05 11:11:44', NULL);

-- --------------------------------------------------------

--
-- Estrutura para tabela `m2p_users`
--

CREATE TABLE `m2p_users` (
  `id` int(10) UNSIGNED NOT NULL,
  `site_id` int(10) UNSIGNED NOT NULL,
  `first_name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `last_name` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `login` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `activation_code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `remember_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `reset_password_code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `activated_at` datetime DEFAULT NULL,
  `last_login_at` datetime DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Fazendo dump de dados para tabela `m2p_users`
--

INSERT INTO `m2p_users` (`id`, `site_id`, `first_name`, `last_name`, `login`, `email`, `password`, `activation_code`, `remember_token`, `reset_password_code`, `created_at`, `updated_at`, `activated_at`, `last_login_at`, `deleted_at`) VALUES
(1, 1, 'TennisSquare', '', 'admin', 'web@ph2.com.br', '$2y$10$kZCj.T7e73u6CHmbhyBdNeAmsdC.SkBvEeMmS2uc7yTDrQ53iccx6', NULL, 'Joigs4NTkW51xZquf0YFzMXQC7o59LzYmWTrZuFClKQqADQhMqvqdVMSldYA', NULL, '2016-09-05 11:11:44', '2016-09-28 20:22:47', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Estrutura para tabela `m2p_user_password_resets`
--

CREATE TABLE `m2p_user_password_resets` (
  `site_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura para tabela `media`
--

CREATE TABLE `media` (
  `id` int(10) UNSIGNED NOT NULL,
  `model_id` int(10) UNSIGNED NOT NULL,
  `model_type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `collection_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `file_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `disk` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `size` int(10) UNSIGNED NOT NULL,
  `manipulations` text COLLATE utf8_unicode_ci NOT NULL,
  `custom_properties` text COLLATE utf8_unicode_ci NOT NULL,
  `order_column` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Fazendo dump de dados para tabela `media`
--

INSERT INTO `media` (`id`, `model_id`, `model_type`, `collection_name`, `name`, `file_name`, `disk`, `size`, `manipulations`, `custom_properties`, `order_column`, `created_at`, `updated_at`) VALUES
(54, 2, 'Mind2Press\\Modules\\Content\\Models\\Content', 'chamada', 'tre-avan1', 'tre-avan1.jpg', 'media', 493434, '[]', '[]', 50, '2016-09-22 19:56:41', '2016-09-22 19:56:41'),
(56, 3, 'Mind2Press\\Modules\\Content\\Models\\Content', 'chamada', 'esc-tenis-kids', 'esc-tenis-kids.jpg', 'media', 557437, '[]', '[]', 51, '2016-09-22 20:00:08', '2016-09-22 20:00:08'),
(58, 5, 'Mind2Press\\Modules\\Content\\Models\\Content', 'chamada', 'ind-group', 'ind-group.jpg', 'media', 552031, '[]', '[]', 53, '2016-09-22 20:04:56', '2016-09-22 20:04:56'),
(59, 6, 'Mind2Press\\Modules\\Content\\Models\\Content', 'chamada', 'loc-quadras', 'loc-quadras.jpg', 'media', 608754, '[]', '[]', 54, '2016-09-22 20:09:19', '2016-09-22 20:09:19'),
(67, 8, 'Mind2Press\\Modules\\Content\\Models\\Content', 'chamada', 'musc', 'musc.jpg', 'media', 500115, '[]', '[]', 56, '2016-09-22 20:19:08', '2016-09-22 20:19:08'),
(68, 9, 'Mind2Press\\Modules\\Content\\Models\\Content', 'chamada', 'spining', 'spining.jpg', 'media', 469583, '[]', '[]', 57, '2016-09-22 20:21:07', '2016-09-22 20:21:07'),
(69, 10, 'Mind2Press\\Modules\\Content\\Models\\Content', 'chamada', 'jump-step', 'jump-step.jpg', 'media', 688400, '[]', '[]', 58, '2016-09-22 20:22:22', '2016-09-22 20:22:22'),
(71, 11, 'Mind2Press\\Modules\\Content\\Models\\Content', 'chamada', 'gap1', 'gap1.jpg', 'media', 495759, '[]', '[]', 59, '2016-09-22 20:24:13', '2016-09-22 20:24:13'),
(72, 12, 'Mind2Press\\Modules\\Content\\Models\\Content', 'chamada', 'remo', 'remo.jpg', 'media', 567081, '[]', '[]', 60, '2016-09-22 20:25:21', '2016-09-22 20:25:21'),
(75, 39, 'Mind2Press\\Modules\\Content\\Models\\Content', 'banner', 'principal', 'principal.png', 'media', 496438, '[]', '[]', 63, '2016-09-23 05:33:54', '2016-09-23 05:33:54'),
(81, 43, 'Mind2Press\\Modules\\Content\\Models\\Content', 'banner', 'Banner-Home_1', 'Banner-Home_1.jpg', 'media', 154676, '[]', '[]', 64, '2016-09-28 19:57:44', '2016-09-28 19:57:44'),
(82, 44, 'Mind2Press\\Modules\\Content\\Models\\Content', 'banner', 'Banner-Home_2', 'Banner-Home_2.jpg', 'media', 319340, '[]', '[]', 65, '2016-09-28 19:58:07', '2016-09-28 19:58:07'),
(84, 46, 'Mind2Press\\Modules\\Content\\Models\\Content', 'banner', 'Banner-Home_4', 'Banner-Home_4.jpg', 'media', 527380, '[]', '[]', 67, '2016-09-28 19:58:40', '2016-09-28 19:58:40'),
(85, 47, 'Mind2Press\\Modules\\Content\\Models\\Content', 'banner', 'Banner-Home_5', 'Banner-Home_5.jpg', 'media', 252292, '[]', '[]', 68, '2016-09-28 19:58:56', '2016-09-28 19:58:56'),
(88, 6, 'Mind2Press\\Modules\\Content\\Models\\Content', 'main', 'loc-quadras', 'loc-quadras.png', 'media', 1590537, '[]', '[]', 71, '2016-09-28 20:00:59', '2016-09-28 20:00:59'),
(92, 3, 'Mind2Press\\Modules\\Content\\Models\\Content', 'main', 'tenis-kids', 'tenis-kids.png', 'media', 1219177, '[]', '[]', 75, '2016-09-28 20:02:27', '2016-09-28 20:02:27'),
(93, 48, 'Mind2Press\\Modules\\Content\\Models\\Content', 'chamada', '14433085_1229913843695907_7387874690622987098_n', '14433085_1229913843695907_7387874690622987098_n.jpg', 'media', 29460, '[]', '[]', 76, '2016-09-28 20:02:29', '2016-09-28 20:02:29'),
(94, 48, 'Mind2Press\\Modules\\Content\\Models\\Content', 'main', '14449013_1229908097029815_7875718827706461600_n', '14449013_1229908097029815_7875718827706461600_n.jpg', 'media', 146157, '[]', '[]', 77, '2016-09-28 20:02:30', '2016-09-28 20:02:30'),
(97, 11, 'Mind2Press\\Modules\\Content\\Models\\Content', 'main', 'gap', 'gap.png', 'media', 1254149, '[]', '[]', 80, '2016-09-28 20:03:07', '2016-09-28 20:03:07'),
(98, 10, 'Mind2Press\\Modules\\Content\\Models\\Content', 'main', 'jump-setp', 'jump-setp.png', 'media', 1662518, '[]', '[]', 81, '2016-09-28 20:03:19', '2016-09-28 20:03:19'),
(99, 9, 'Mind2Press\\Modules\\Content\\Models\\Content', 'main', 'spinning', 'spinning.png', 'media', 1179647, '[]', '[]', 82, '2016-09-28 20:03:28', '2016-09-28 20:03:28'),
(100, 8, 'Mind2Press\\Modules\\Content\\Models\\Content', 'main', 'musculacaO', 'musculacaO.png', 'media', 1815964, '[]', '[]', 83, '2016-09-28 20:03:38', '2016-09-28 20:03:38'),
(110, 65, 'Mind2Press\\Modules\\Content\\Models\\Content', 'main', 'yoga', 'yoga.png', 'media', 1138110, '[]', '[]', 93, '2016-09-30 20:11:10', '2016-09-30 20:11:10'),
(111, 66, 'Mind2Press\\Modules\\Content\\Models\\Content', 'chamada', 'trei-func', 'trei-func.jpg', 'media', 316713, '[]', '[]', 94, '2016-09-30 20:13:45', '2016-09-30 20:13:45'),
(112, 66, 'Mind2Press\\Modules\\Content\\Models\\Content', 'main', 'trei-func', 'trei-func.png', 'media', 1014846, '[]', '[]', 95, '2016-09-30 20:13:45', '2016-09-30 20:13:45'),
(113, 67, 'Mind2Press\\Modules\\Content\\Models\\Content', 'chamada', 'pilates', 'pilates.jpg', 'media', 504989, '[]', '[]', 96, '2016-09-30 20:15:29', '2016-09-30 20:15:29'),
(114, 67, 'Mind2Press\\Modules\\Content\\Models\\Content', 'main', 'pillates', 'pillates.png', 'media', 1815680, '[]', '[]', 97, '2016-09-30 20:15:30', '2016-09-30 20:15:30'),
(121, 12, 'Mind2Press\\Modules\\Content\\Models\\Content', 'main', 'remo', 'remo.png', 'media', 1951710, '[]', '[]', 104, '2016-09-30 20:27:38', '2016-09-30 20:27:38'),
(122, 5, 'Mind2Press\\Modules\\Content\\Models\\Content', 'main', 'ind-gru', 'ind-gru.png', 'media', 1239546, '[]', '[]', 105, '2016-09-30 20:34:22', '2016-09-30 20:34:22'),
(125, 2, 'Mind2Press\\Modules\\Content\\Models\\Content', 'main', 'trei-comp', 'trei-comp.png', 'media', 1188397, '[]', '[]', 108, '2016-10-03 13:19:15', '2016-10-03 13:19:15'),
(126, 7, 'Mind2Press\\Modules\\Content\\Models\\Content', 'chamada', 'icon-tenis', 'icon-tenis.png', 'media', 93045, '[]', '[]', 109, '2016-10-03 13:24:23', '2016-10-03 13:24:23'),
(128, 70, 'Mind2Press\\Modules\\Content\\Models\\Content', 'chamada', 'espaco-max', 'espaco-max.jpg', 'media', 582897, '[]', '[]', 111, '2016-10-03 14:07:03', '2016-10-03 14:07:03'),
(130, 7, 'Mind2Press\\Modules\\Content\\Models\\Content', 'main', 'cond-fis', 'cond-fis.png', 'media', 1727968, '[]', '[]', 113, '2016-10-03 17:32:25', '2016-10-03 17:32:25'),
(131, 68, 'Mind2Press\\Modules\\Content\\Models\\Content', 'chamada', 'nutri-ch', 'nutri-ch.jpg', 'media', 362948, '[]', '[]', 114, '2016-10-03 17:37:01', '2016-10-03 17:37:01'),
(132, 68, 'Mind2Press\\Modules\\Content\\Models\\Content', 'main', 'nutricao', 'nutricao.png', 'media', 1471505, '[]', '[]', 115, '2016-10-03 17:37:02', '2016-10-03 17:37:02'),
(133, 69, 'Mind2Press\\Modules\\Content\\Models\\Content', 'chamada', 'fisio-cha', 'fisio-cha.jpg', 'media', 541971, '[]', '[]', 116, '2016-10-03 17:40:41', '2016-10-03 17:40:41'),
(134, 69, 'Mind2Press\\Modules\\Content\\Models\\Content', 'main', 'fisioterapia', 'fisioterapia.png', 'media', 1759051, '[]', '[]', 117, '2016-10-03 17:40:42', '2016-10-03 17:40:42'),
(136, 173, 'Mind2Press\\Modules\\Content\\Models\\Content', 'banner', 'Banner-Home_6', 'Banner-Home_6.jpg', 'media', 396474, '[]', '[]', 119, '2016-10-03 18:07:26', '2016-10-03 18:07:26'),
(139, 175, 'Mind2Press\\Modules\\Content\\Models\\Content', 'chamada', 'quadra1', 'quadra1.jpg', 'media', 1033197, '[]', '[]', 120, '2016-10-03 18:21:39', '2016-10-03 18:21:39'),
(140, 175, 'Mind2Press\\Modules\\Content\\Models\\Content', 'main', 'quadra-t', 'quadra-t.png', 'media', 174183, '[]', '[]', 121, '2016-10-03 18:21:40', '2016-10-03 18:21:40'),
(142, 65, 'Mind2Press\\Modules\\Content\\Models\\Content', 'chamada', 'yoga-cha', 'yoga-cha.jpg', 'media', 219353, '[]', '[]', 123, '2016-10-04 14:47:36', '2016-10-04 14:47:36'),
(143, 70, 'Mind2Press\\Modules\\Content\\Models\\Content', 'main', 'nax-recovery', 'nax-recovery.png', 'media', 1613145, '[]', '[]', 124, '2016-10-04 14:47:53', '2016-10-04 14:47:53'),
(144, 177, 'Mind2Press\\Modules\\Content\\Models\\Content', 'infrastructure', 'DSC01447', 'DSC01447.jpg', 'media', 257234, '[]', '[]', 125, '2016-10-10 02:43:57', '2016-10-10 02:43:57'),
(145, 178, 'Mind2Press\\Modules\\Content\\Models\\Content', 'infrastructure', 'DSC01473', 'DSC01473.jpg', 'media', 425132, '[]', '[]', 126, '2016-10-10 02:44:12', '2016-10-10 02:44:12'),
(146, 179, 'Mind2Press\\Modules\\Content\\Models\\Content', 'infrastructure', 'DSC01496', 'DSC01496.jpg', 'media', 394628, '[]', '[]', 127, '2016-10-10 02:44:25', '2016-10-10 02:44:25'),
(147, 180, 'Mind2Press\\Modules\\Content\\Models\\Content', 'infrastructure', 'DSC01517', 'DSC01517.jpg', 'media', 273317, '[]', '[]', 128, '2016-10-10 02:44:35', '2016-10-10 02:44:35'),
(148, 181, 'Mind2Press\\Modules\\Content\\Models\\Content', 'infrastructure', 'DSC01521', 'DSC01521.jpg', 'media', 254717, '[]', '[]', 129, '2016-10-10 02:44:43', '2016-10-10 02:44:43'),
(149, 182, 'Mind2Press\\Modules\\Content\\Models\\Content', 'infrastructure', 'DSC01526', 'DSC01526.jpg', 'media', 269144, '[]', '[]', 130, '2016-10-10 02:44:55', '2016-10-10 02:44:55'),
(151, 184, 'Mind2Press\\Modules\\Content\\Models\\Content', 'infrastructure', 'DSC01541_thumb', 'DSC01541_thumb.jpg', 'media', 691216, '[]', '[]', 132, '2016-10-10 02:45:13', '2016-10-10 02:45:13'),
(152, 185, 'Mind2Press\\Modules\\Content\\Models\\Content', 'infrastructure', 'DSC01551', 'DSC01551.jpg', 'media', 299631, '[]', '[]', 133, '2016-10-10 02:47:44', '2016-10-10 02:47:44'),
(153, 186, 'Mind2Press\\Modules\\Content\\Models\\Content', 'infrastructure', 'DSC01555', 'DSC01555.jpg', 'media', 270939, '[]', '[]', 134, '2016-10-10 02:47:57', '2016-10-10 02:47:57'),
(154, 187, 'Mind2Press\\Modules\\Content\\Models\\Content', 'infrastructure', 'DSC01562', 'DSC01562.jpg', 'media', 359795, '[]', '[]', 135, '2016-10-10 02:48:07', '2016-10-10 02:48:07'),
(155, 188, 'Mind2Press\\Modules\\Content\\Models\\Content', 'infrastructure', 'DSC01589', 'DSC01589.jpg', 'media', 301786, '[]', '[]', 136, '2016-10-10 02:48:20', '2016-10-10 02:48:20'),
(156, 189, 'Mind2Press\\Modules\\Content\\Models\\Content', 'infrastructure', 'DSC01595', 'DSC01595.jpg', 'media', 325155, '[]', '[]', 137, '2016-10-10 02:48:32', '2016-10-10 02:48:32'),
(157, 190, 'Mind2Press\\Modules\\Content\\Models\\Content', 'infrastructure', 'DSC01598', 'DSC01598.jpg', 'media', 359329, '[]', '[]', 138, '2016-10-10 02:48:43', '2016-10-10 02:48:43'),
(158, 191, 'Mind2Press\\Modules\\Content\\Models\\Content', 'infrastructure', 'DSC01601', 'DSC01601.jpg', 'media', 348719, '[]', '[]', 139, '2016-10-10 02:48:51', '2016-10-10 02:48:51'),
(159, 192, 'Mind2Press\\Modules\\Content\\Models\\Content', 'infrastructure', 'DSC01603', 'DSC01603.jpg', 'media', 336316, '[]', '[]', 140, '2016-10-10 02:48:59', '2016-10-10 02:48:59'),
(160, 193, 'Mind2Press\\Modules\\Content\\Models\\Content', 'infrastructure', 'DSC01606', 'DSC01606.jpg', 'media', 329656, '[]', '[]', 141, '2016-10-10 02:49:07', '2016-10-10 02:49:07'),
(161, 194, 'Mind2Press\\Modules\\Content\\Models\\Content', 'infrastructure', 'DSC01608', 'DSC01608.jpg', 'media', 306008, '[]', '[]', 142, '2016-10-10 02:49:16', '2016-10-10 02:49:16'),
(162, 195, 'Mind2Press\\Modules\\Content\\Models\\Content', 'infrastructure', 'DSC01657', 'DSC01657.jpg', 'media', 340823, '[]', '[]', 143, '2016-10-10 02:49:23', '2016-10-10 02:49:23'),
(163, 196, 'Mind2Press\\Modules\\Content\\Models\\Content', 'infrastructure', 'DSC01666', 'DSC01666.jpg', 'media', 338796, '[]', '[]', 144, '2016-10-10 02:49:31', '2016-10-10 02:49:31'),
(164, 197, 'Mind2Press\\Modules\\Content\\Models\\Content', 'infrastructure', 'DSC01690', 'DSC01690.jpg', 'media', 241235, '[]', '[]', 145, '2016-10-10 02:49:38', '2016-10-10 02:49:38'),
(165, 198, 'Mind2Press\\Modules\\Content\\Models\\Content', 'infrastructure', 'DSC01694', 'DSC01694.jpg', 'media', 297568, '[]', '[]', 146, '2016-10-10 02:49:46', '2016-10-10 02:49:46'),
(166, 199, 'Mind2Press\\Modules\\Content\\Models\\Content', 'infrastructure', 'DSC01709', 'DSC01709.jpg', 'media', 279666, '[]', '[]', 147, '2016-10-10 02:49:53', '2016-10-10 02:49:53'),
(167, 200, 'Mind2Press\\Modules\\Content\\Models\\Content', 'infrastructure', 'DSC01712', 'DSC01712.jpg', 'media', 364388, '[]', '[]', 148, '2016-10-10 02:50:02', '2016-10-10 02:50:02'),
(168, 201, 'Mind2Press\\Modules\\Content\\Models\\Content', 'infrastructure', 'DSC01721', 'DSC01721.jpg', 'media', 330841, '[]', '[]', 149, '2016-10-10 02:50:10', '2016-10-10 02:50:10'),
(169, 202, 'Mind2Press\\Modules\\Content\\Models\\Content', 'infrastructure', 'DSC01729', 'DSC01729.jpg', 'media', 292198, '[]', '[]', 150, '2016-10-10 02:50:21', '2016-10-10 02:50:21'),
(170, 203, 'Mind2Press\\Modules\\Content\\Models\\Content', 'infrastructure', 'DSC01736', 'DSC01736.jpg', 'media', 324103, '[]', '[]', 151, '2016-10-10 02:50:29', '2016-10-10 02:50:29');

-- --------------------------------------------------------

--
-- Estrutura para tabela `migrations`
--

CREATE TABLE `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Fazendo dump de dados para tabela `migrations`
--

INSERT INTO `migrations` (`migration`, `batch`) VALUES
('2016_08_01_01001_create_m2p_sites_table', 1),
('2016_08_01_01011_create_m2p_options_table', 1),
('2016_08_01_01012_create_m2p_backend_logs_table', 1),
('2016_08_01_01013_create_m2p_backend_request_logs_table', 1),
('2016_08_01_01014_create_m2p_user_groups_table', 1),
('2016_08_01_01015_create_m2p_users_table', 1),
('2016_08_01_01017_create_m2p_user_password_resets_table', 1),
('2016_08_01_01030_create_failed_jobs_table', 1),
('2016_08_01_01031_create_jobs_table', 1),
('2016_08_01_01032_create_sessions_table', 1),
('2016_08_01_02001_create_m2p_content_types_table', 1),
('2016_08_01_02002_create_m2p_contents_table', 1),
('2016_08_01_02003_create_m2p_content_meta_table', 1),
('2016_08_01_02004_create_m2p_content_taxonomies_table', 1),
('2016_08_01_02005_create_m2p_content_taxonomy_terms_table', 1),
('2016_08_28_180500_entrust_setup_tables', 1),
('2016_09_12_041956_create_media_table', 2);

--
-- Índices de tabelas apagadas
--

--
-- Índices de tabela `m2p_backend_logs`
--
ALTER TABLE `m2p_backend_logs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `m2p_backend_logs_site_id_foreign` (`site_id`),
  ADD KEY `m2p_backend_logs_level_index` (`level`);

--
-- Índices de tabela `m2p_contents`
--
ALTER TABLE `m2p_contents`
  ADD PRIMARY KEY (`id`),
  ADD KEY `m2p_contents_site_id_foreign` (`site_id`),
  ADD KEY `m2p_contents_author_foreign` (`author`),
  ADD KEY `m2p_contents_content_type_id_foreign` (`content_type_id`),
  ADD KEY `m2p_contents_status_index` (`status`),
  ADD KEY `m2p_contents_slug_index` (`slug`);
ALTER TABLE `m2p_contents` ADD FULLTEXT KEY `index_name` (`title`,`content`);

--
-- Índices de tabela `m2p_content_meta`
--
ALTER TABLE `m2p_content_meta`
  ADD PRIMARY KEY (`id`),
  ADD KEY `m2p_content_meta_content_id_foreign` (`content_id`);

--
-- Índices de tabela `m2p_content_taxonomies`
--
ALTER TABLE `m2p_content_taxonomies`
  ADD PRIMARY KEY (`id`),
  ADD KEY `m2p_content_taxonomies_content_type_id_foreign` (`content_type_id`),
  ADD KEY `m2p_content_taxonomies_site_id_foreign` (`site_id`),
  ADD KEY `m2p_content_taxonomies_slug_index` (`slug`);

--
-- Índices de tabela `m2p_content_taxonomy_terms`
--
ALTER TABLE `m2p_content_taxonomy_terms`
  ADD PRIMARY KEY (`id`),
  ADD KEY `m2p_content_taxonomy_terms_content_taxonomy_id_foreign` (`content_taxonomy_id`);

--
-- Índices de tabela `m2p_content_types`
--
ALTER TABLE `m2p_content_types`
  ADD PRIMARY KEY (`id`),
  ADD KEY `m2p_content_types_site_id_foreign` (`site_id`),
  ADD KEY `m2p_content_types_name_index` (`name`),
  ADD KEY `m2p_content_types_slug_index` (`slug`);

--
-- Índices de tabela `m2p_options`
--
ALTER TABLE `m2p_options`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `m2p_options_key_unique` (`key`),
  ADD KEY `m2p_options_site_id_foreign` (`site_id`);

--
-- Índices de tabela `m2p_permissions`
--
ALTER TABLE `m2p_permissions`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `m2p_permissions_name_unique` (`name`);

--
-- Índices de tabela `m2p_permission_role`
--
ALTER TABLE `m2p_permission_role`
  ADD PRIMARY KEY (`permission_id`,`role_id`),
  ADD KEY `m2p_permission_role_role_id_foreign` (`role_id`);

--
-- Índices de tabela `m2p_roles`
--
ALTER TABLE `m2p_roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `m2p_roles_name_unique` (`name`);

--
-- Índices de tabela `m2p_role_user`
--
ALTER TABLE `m2p_role_user`
  ADD PRIMARY KEY (`user_id`,`role_id`),
  ADD KEY `m2p_role_user_role_id_foreign` (`role_id`);

--
-- Índices de tabela `m2p_sites`
--
ALTER TABLE `m2p_sites`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `m2p_sites_slug_unique` (`slug`);

--
-- Índices de tabela `m2p_users`
--
ALTER TABLE `m2p_users`
  ADD PRIMARY KEY (`id`),
  ADD KEY `m2p_users_site_id_foreign` (`site_id`),
  ADD KEY `m2p_users_login_index` (`login`),
  ADD KEY `m2p_users_email_index` (`email`);

--
-- Índices de tabela `m2p_user_password_resets`
--
ALTER TABLE `m2p_user_password_resets`
  ADD KEY `m2p_user_password_resets_site_id_foreign` (`site_id`),
  ADD KEY `m2p_user_password_resets_user_id_foreign` (`user_id`),
  ADD KEY `m2p_user_password_resets_email_index` (`email`),
  ADD KEY `m2p_user_password_resets_token_index` (`token`);

--
-- Índices de tabela `media`
--
ALTER TABLE `media`
  ADD PRIMARY KEY (`id`),
  ADD KEY `media_model_id_model_type_index` (`model_id`,`model_type`);

--
-- AUTO_INCREMENT de tabelas apagadas
--

--
-- AUTO_INCREMENT de tabela `m2p_backend_logs`
--
ALTER TABLE `m2p_backend_logs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de tabela `m2p_contents`
--
ALTER TABLE `m2p_contents`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=204;
--
-- AUTO_INCREMENT de tabela `m2p_content_meta`
--
ALTER TABLE `m2p_content_meta`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=174;
--
-- AUTO_INCREMENT de tabela `m2p_content_taxonomies`
--
ALTER TABLE `m2p_content_taxonomies`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT de tabela `m2p_content_taxonomy_terms`
--
ALTER TABLE `m2p_content_taxonomy_terms`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT de tabela `m2p_content_types`
--
ALTER TABLE `m2p_content_types`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT de tabela `m2p_options`
--
ALTER TABLE `m2p_options`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de tabela `m2p_permissions`
--
ALTER TABLE `m2p_permissions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;
--
-- AUTO_INCREMENT de tabela `m2p_roles`
--
ALTER TABLE `m2p_roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT de tabela `m2p_sites`
--
ALTER TABLE `m2p_sites`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de tabela `m2p_users`
--
ALTER TABLE `m2p_users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de tabela `media`
--
ALTER TABLE `media`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=171;
--
-- Restrições para dumps de tabelas
--

--
-- Restrições para tabelas `m2p_backend_logs`
--
ALTER TABLE `m2p_backend_logs`
  ADD CONSTRAINT `m2p_backend_logs_site_id_foreign` FOREIGN KEY (`site_id`) REFERENCES `m2p_sites` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Restrições para tabelas `m2p_contents`
--
ALTER TABLE `m2p_contents`
  ADD CONSTRAINT `m2p_contents_author_foreign` FOREIGN KEY (`author`) REFERENCES `m2p_users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `m2p_contents_content_type_id_foreign` FOREIGN KEY (`content_type_id`) REFERENCES `m2p_content_types` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `m2p_contents_site_id_foreign` FOREIGN KEY (`site_id`) REFERENCES `m2p_sites` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Restrições para tabelas `m2p_content_meta`
--
ALTER TABLE `m2p_content_meta`
  ADD CONSTRAINT `m2p_content_meta_content_id_foreign` FOREIGN KEY (`content_id`) REFERENCES `m2p_contents` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Restrições para tabelas `m2p_content_taxonomies`
--
ALTER TABLE `m2p_content_taxonomies`
  ADD CONSTRAINT `m2p_content_taxonomies_content_type_id_foreign` FOREIGN KEY (`content_type_id`) REFERENCES `m2p_content_types` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `m2p_content_taxonomies_site_id_foreign` FOREIGN KEY (`site_id`) REFERENCES `m2p_sites` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Restrições para tabelas `m2p_content_taxonomy_terms`
--
ALTER TABLE `m2p_content_taxonomy_terms`
  ADD CONSTRAINT `m2p_content_taxonomy_terms_content_taxonomy_id_foreign` FOREIGN KEY (`content_taxonomy_id`) REFERENCES `m2p_content_taxonomies` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Restrições para tabelas `m2p_content_types`
--
ALTER TABLE `m2p_content_types`
  ADD CONSTRAINT `m2p_content_types_site_id_foreign` FOREIGN KEY (`site_id`) REFERENCES `m2p_sites` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Restrições para tabelas `m2p_options`
--
ALTER TABLE `m2p_options`
  ADD CONSTRAINT `m2p_options_site_id_foreign` FOREIGN KEY (`site_id`) REFERENCES `m2p_sites` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Restrições para tabelas `m2p_permission_role`
--
ALTER TABLE `m2p_permission_role`
  ADD CONSTRAINT `m2p_permission_role_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `m2p_permissions` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `m2p_permission_role_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `m2p_roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Restrições para tabelas `m2p_role_user`
--
ALTER TABLE `m2p_role_user`
  ADD CONSTRAINT `m2p_role_user_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `m2p_roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `m2p_role_user_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `m2p_users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Restrições para tabelas `m2p_users`
--
ALTER TABLE `m2p_users`
  ADD CONSTRAINT `m2p_users_site_id_foreign` FOREIGN KEY (`site_id`) REFERENCES `m2p_sites` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Restrições para tabelas `m2p_user_password_resets`
--
ALTER TABLE `m2p_user_password_resets`
  ADD CONSTRAINT `m2p_user_password_resets_site_id_foreign` FOREIGN KEY (`site_id`) REFERENCES `m2p_sites` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `m2p_user_password_resets_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `m2p_users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
