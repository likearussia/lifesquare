<?php

namespace Mind2Press\Http\Controllers\Auth;

use Illuminate\Http\Request;
use Mind2Press\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    protected $redirectTo = '/mind2press';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest', ['except' => 'logout']);
        
        $this->redirectTo = route('m2p::home');
    }
    
    /**
     * Get the login username to be used by the controller.
     *
     * @return string
     */
    public function username()
    {
        return 'login';
    }
    
    /**
     * O usuário foi autenticado.
     * 
     * @param Request $request
     * @param type $user
     */
    public function authenticated(Request $request, $user) {
        session(['site' => $user->site->toArray()]);
    }
}
