<?php

namespace Mind2Press\Http\Controllers;

use Illuminate\Http\Request;
use Mind2Press\Http\Requests;
use Mind2Press\Modules\Content\Models\Content;
use Mind2Press\Modules\Content\Models\ContentType;
use Mind2Press\Modules\Content\Models\ContentTaxonomy;
use Mind2Press\Modules\Content\Models\ContentTaxonomyTerm;

class BlogController extends Controller
{
    /**
     *
     * @var Content
     */
    protected $content;
    
    /**
     *
     * @var ContentType
     */
    protected $contentType;
    
    /**
     *
     * @var ContentTaxonomy
     */
    protected $contentTaxonomy;
    
    /**
     *
     * @var ContentTaxonomyTerm
     */
    protected $contentTaxonomyTerm;
    
    public function __construct(Content $content,
                                ContentType $contentType,
                                ContentTaxonomy $contentTaxonomy,
                                ContentTaxonomyTerm $contentTaxonomyTerm)
    {
        $this->content         = $content;
        $this->contentType     = $contentType;
        $this->contentTaxonomy = $contentTaxonomy;
        $this->contentTaxonomyTerm = $contentTaxonomyTerm;
    }
    
    /**
     * 
     */
    public function index(Request $request)
    {
        $data = [];
        
        $data['contentType'] = $this->contentType->where('name', 'post')->firstOrFail();
        
        $searchContent = $this->content->where('content_type_id', $data['contentType']->id)
                                       ->orderBy('created_at', 'DESC');
        if ($request->has('q')) {
            $searchContent->whereRaw("MATCH(content, title) AGAINST(? IN BOOLEAN MODE)", ['*' . $request->q . '*']);
        }
        $data['content'] = $searchContent->paginate(10);
                
        $data['more_content']  = $this->content->where('content_type_id', $data['contentType']->id)
                                               ->whereNotIn('id', $data['content']->pluck('id')->toArray())
                                               ->limit(10)->get();
        
        return view('blog.index', $data);
    }
    
    public function show($content)
    {
        $data = [];
        
        $data['contentType']   = $this->contentType->where('name', 'post')->firstOrFail();
        $data['content']       = $this->content->ofSlug($content)->with('user')->firstOrFail();
        $data['more_content']  = $this->content->where('content_type_id', $data['contentType']->id)
                                               ->whereNotIn('id', [$data['content']->id])
                                               ->limit(10)->get();
        
        return view('blog.content', $data);
    }
}
