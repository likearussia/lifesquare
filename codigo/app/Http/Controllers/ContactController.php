<?php

namespace Mind2Press\Http\Controllers;

use Illuminate\Http\Request;
use Mind2Press\Mail\Contact;
use Illuminate\Support\Facades\Mail;

class ContactController extends Controller
{
    /**
     * Shows the page.
     * 
     * @return void
     */
    public function show()
    {
        $data = [
            'subjects' => $this->getSubjects(),
        ];
        
        return view('site.pages.contato', $data);
    }
    
    /**
     * Send the contact message.
     * 
     * @param Request $request
     */
    public function send(Request $request)
    {
        $this->validateContact($request);
        
        $send = Mail::to(config('mail.contact.address'))->send(new Contact($request->all()));
        
        if ($send === null) {
            flash()->success('E-mail enviado com sucesso!');
        } else {
            flash()->error('Ocorreu um erro ao enviar a mensagem.');
        }
        
        return redirect()->back();
    }
    
    /**
     * Validate the contact message.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return void
     */
    protected function validateContact(Request $request)
    {
        $this->validate($request, [
            'nome'      => 'required|min:3|max:100',
            'email'     => 'required|email',
            'interesse' => 'required',
            'mensagem'  => 'required|min:3|max:2000'
        ]);
    }
    
    protected function getSubjects() {
        return [
            'Academia de Tênis' => 'Academia de Tênis',
            'Fitness Center' => 'Fitness Center',
            'Locação de Quadras' => 'Locação de Quadras',
            'Outros' => 'Outros',
        ];
    }
}
