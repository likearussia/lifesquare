<?php

namespace Mind2Press\Http\Controllers;

use Mind2Press\Modules\Content\Models\Content;

class HomeController extends Controller
{
    /**
     *
     * @var Content
     */
    protected $content;
    
    public function __construct(Content $content) {
        $this->content = $content;
    }
    
    public function index()
    {
        $data = [];
        
        $data['banners'] = $this->content
                                ->with('meta')
                                ->ofContentType('banner')
                                ->published()
                                ->reorder()
                                ->orderBy('id', 'DESC')
                                ->get();
        
        return view('site.index', $data);
    }
}
