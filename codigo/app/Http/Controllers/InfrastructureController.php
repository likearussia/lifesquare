<?php

namespace Mind2Press\Http\Controllers;

use Mind2Press\Modules\Content\Models\Content;

class InfrastructureController extends Controller
{
    /**
     *
     * @var Content
     */
    protected $content;
    
    public function __construct(Content $content)
    {
        $this->content = $content;
    }
    
    public function index()
    {
        $data = [];

        $data['content'] = $this->content->ofContentType('infrastructure')
                                         ->published()
                                         ->paginate(16);
        
        return view('site.pages.infrastructure', $data);
    }
}
