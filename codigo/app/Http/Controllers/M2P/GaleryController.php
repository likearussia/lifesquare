<?php

namespace Mind2Press\Http\Controllers\M2P;

use Illuminate\Http\Request;
use Spatie\MediaLibrary\Media;
use Illuminate\Support\Facades\Storage;
use Mind2Press\Http\Controllers\Controller;
use Mind2Press\Modules\Content\Models\Content;
use Mind2Press\Modules\Core\Traits\UploadTrait;

class GaleryController extends Controller
{
    use UploadTrait;
    
    /**
     *
     * @var Content
     */
    protected $content;
    
    public function __construct(Content $content) {
        $this->content = $content;
    }

    public function show(Request $request)
    {
        $content = $this->content->find($request->contentId);
        
        $data = [
            'images' => $content->getMedia('galery'),
            'maxFilesize' => $this->getMaxUploadSize('M'),
        ];
        
        return view('m2p.galery.galery', $data);
    }
    
    public function upload(Request $request)
    {
        $content = $this->content->find($request->contentId);
        
        try {
            $content->addMedia($request->file('file'))->toCollection('galery');
        } catch (\Exception $e) {
            return response()->json([
                'success' => false,
                'message' => $e->getMessage(),
            ], 500);
        }
        
        return response()->json([
            'success' => true,
        ]);
    }
    
    /**
     * 
     * @param string $filename
     * 
     * @return none
     * 
     * @fixme o delete do arquivo não funciona
     */
    public function delete($filename = null)
    {
        if ($filename === null) {
            return;
        }
        
        Media::where('file_name', $filename)->delete();
        
        Storage::disk('media')->delete($filename);
    }
}
