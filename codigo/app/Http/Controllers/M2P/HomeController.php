<?php

namespace Mind2Press\Http\Controllers\M2P;

use Analytics;
use Spatie\Analytics\Period;
use Mind2Press\Http\Controllers\Controller;

class HomeController extends Controller
{
    public function index()
    {
        $data = [];
        
        $data['visitorsAndPageViews'] = $this->getVisitorsAndPageViewsAsFormattedJson();
        $data['mostVisitedPages']     = $this->getMostVisitedPages();
        
        return view('m2p.home', $data);
    }
    
    protected function getVisitorsAndPageViewsAsFormattedJson()
    {
        $data = $this->getVisitorsAndPageViews();
        
        $newData = [];
        
        foreach ($data as $date => $values) {
            $newData[] = [
                'y' => $date,
                'visitors' => $values['visitors'],
                'pageViews' => $values['pageViews'],
            ];
        }
        
        return json_encode($newData, JSON_PRETTY_PRINT);
    }
    
    /**
     * Retorna o número de visitantes e visualizações de página nos últimos $days dias.
     * 
     * Exemplo de retorno:
     * [
     *  "2016-10-21" => [
     *      "visitors" => 3
     *      "pageViews" => 5
     *  ]
     *  ...
     * ]
     * 
     * @param int $days número de dias
     * 
     * @return array
     */
    protected function getVisitorsAndPageViews($days = 7)
    {
        $data = Analytics::fetchVisitorsAndPageViews(Period::days($days))
                         ->groupBy(function($item, $key) {
                             // Agrupa por ano-mes-dia
                             return $item['date']->format('Y-m-d');
                         })->toArray();
        
        // Cria um novo array contendo apenas os totais
        $newData = [];
        foreach ($data as $date => $values) {
            $newData[$date] = [
                'visitors' => 0,
                'pageViews' => 0,
            ];
            
            foreach ($values as $item => $key) {
                $newData[$date]['visitors'] += $key['visitors'];
                $newData[$date]['pageViews'] += $key['pageViews'];
            }
        }
                                      
        return $newData;
    }
    
    /**
     * 
     * @param int $days
     * @param int $maxResults número máximo de resultados a serem retornados
     * 
     * @return array
     */
    protected function getMostVisitedPages($days = 7, $maxResults = 7)
    {
        $data = Analytics::fetchMostVisitedPages(Period::days($days), $maxResults);
        
        return $data->toArray();
    }
}
