<?php

namespace Mind2Press\Http\Controllers\M2P;

use Illuminate\Http\Request;
use Mind2Press\Http\Controllers\Controller;

class OptionController extends Controller
{
    public function index()
    {
        return view('m2p.option.layout');
    }
}
