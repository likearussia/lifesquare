<?php

namespace Mind2Press\Http\Controllers\M2P;

use Auth;
use Validator;
use Illuminate\Http\Request;
use Mind2Press\Http\Controllers\Controller;
use Mind2Press\Modules\Content\Models\Content;
use Mind2Press\Modules\Content\Models\ContentType;
use Mind2Press\Modules\Content\Models\ContentMeta;
use Mind2Press\Modules\Content\Models\ContentTaxonomy;
use Mind2Press\Modules\Content\Models\ContentTaxonomyTerm;
use Mind2Press\Providers\Table\ContentTableSortTrait;
use Mind2Press\Modules\Content\Traits\SearchContentTrait;

use Mind2Press\Modules\Core\Traits\AuthorizesSitesTrait;
use Mind2Press\Modules\Core\Exceptions\ParameterNotFoundException;
use Mind2Press\Modules\Content\Http\Controllers\Utils\SlugController;


class PlanController extends Controller
{
    use AuthorizesSitesTrait {
        getPreparedData as getPreparedDataTrait;
    }
    
    use ContentTableSortTrait, SearchContentTrait;
    
    public $statuses = [
      'published' => 'Ativo',
      'draft' => 'Inativo',
    ];
    
    public $commentStatuses = [
        'opened' => 'Aberto',
        'closed' => 'Fechado',
    ];
    
    /**
     *
     * @var Content
     */
    protected $content;
    
    /**
     *
     * @var ContentType
     */
    protected $contentType;
    
    /**
     *
     * @var ContentMeta
     */
    protected $contentMeta;
    
    /**
     *
     * @var ContentTaxonomy
     */
    protected $contentTaxonomy;
    
    /**
     *
     * @var ContentTaxonomyTerm
     */
    protected $contentTaxonomyTerm;
    
    public function __construct(Content $content,
                                ContentType $contentType,
                                ContentMeta $contentMeta,
                                ContentTaxonomy $contentTaxonomy,
                                ContentTaxonomyTerm $contentTaxonomyTerm) {
        $this->content     = $content;
        $this->contentType = $contentType;
        $this->contentMeta = $contentMeta;
        $this->contentTaxonomy     = $contentTaxonomy;
        $this->contentTaxonomyTerm = $contentTaxonomyTerm;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        // É preciso especificar o que deve ser exibido.
        if (!$request->has('contentType') && !$request->has('taxonomyTerm')) {
            abort(404);
        }
        
        // Não consigo encontrar um termo sem saber qual é a taxonomia.
        if ($request->has('taxonomy') && !$request->has('taxonomyTerm')) {
            abort(404);
        }
        if ($request->has('taxonomyTerm') && !$request->has('taxonomy')) {
            abort(404);
        }
        
        $data = [
            'statuses' => $this->statuses,
        ];
        
        $searchContent = $this->content
                              ->select(['m2p_contents.*', 'value'])
                              ->with('user')
                              ->join('m2p_content_meta', 'm2p_content_meta.content_id', '=', 'm2p_contents.id')
                              ->where("m2p_content_meta.key", 'plan');
        
        // Busca
        $searchContent = $this->searchContent($searchContent, $request);
        
        // Ordenação
        $searchContent = $this->sortTable($searchContent, $request);
        
        // Procurando por ContentType
        if ($request->has('contentType')) {
            $search = $this->contentType->where('name', $request->contentType)->firstOrFail();
            $searchContent->where('content_type_id', $search->id);
            $data['title'] = $search->description;
        }
        
        // Procurando por TaxonomyTerm
        if ($request->has('taxonomy') && $request->has('taxonomyTerm')) {
            // #TODO implementar pesquisa por slug ao inves do ID
            $term = $this->contentTaxonomyTerm->where('id', $request->taxonomyTerm)->firstOrFail();
            
            $contentIds = $this->contentMeta->taxonomies()
                                ->where('value->'.$request->taxonomy, $request->taxonomyTerm)
                                ->pluck('id');
            $searchContent->whereIn('id', $contentIds);
            $data['title'] = $term->term;
        }
        
        $data['content'] = $searchContent->paginate(10);

        return view('m2p.plan.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        // É preciso especificar o que deve ser criado.
        if (!$request->has('contentType')) {
            abort(404);
        }
        
        // Não consigo encontrar um termo sem saber qual é a taxonomia.
        if ($request->has('taxonomy') && !$request->has('taxonomyTerm')) {
            abort(404);
        }
        if ($request->has('taxonomyTerm') && !$request->has('taxonomy')) {
            abort(404);
        }
        
        $data = [
            'contentType'     => $request->contentType,
            'taxonomy'        => $request->taxonomy,
            'taxonomyTerm'    => $request->taxonomyTerm,
        ];
        
        $searchContent = $this->content->with('user');

        $search = $this->contentType->where('name', $request->contentType)->firstOrFail();
        $data['content'] = $searchContent->where('content_type_id', $search->id)->get();
        $data['title'] = $search->description;
        
        return view('m2p.plan.create', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //$this->authorize('create', $this->content->class);
        
        $data = $this->getPreparedData($request);
        
        $validation = Validator::make($data, [
            'site_id'            => 'required|exists:m2p_sites,id',
            'author'             => 'required|exists:m2p_users,id',
            'title'              => 'required|string',
            'content'            => 'required|string',
            'meta.plan.cost'    => 'required|numeric',
        ]);
        
        if ($validation->fails()) {
            return redirect()->back()
                        ->withErrors($validation)
                        ->withInput();
        }
        
        $content = $this->content->create($data);
        $this->saveMeta($request, $content);
        
        flash()->success('Plano criado.');
        
        return $this->sendRedirectResponse($request);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $content
     * @return \Illuminate\Http\Response
     */
    public function show(Content $content)
    {
        //$this->authorize('view', $content);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($content)
    {
        //$this->authorize('update', $content);
        
        $data = [];
        
        $data['content']  = $this->content->where('slug', $content)->firstOrFail();
        $data['meta']     = $data['content']->meta()->where('key', 'plan')->first();

        $contentType = $this->contentType->where('id', $data['content']['content_type_id'])->firstOrFail();
        $data['title']        = $contentType->description;
        $data['contentType']  = $contentType->name;
        
//        dd($data['meta']->value['cost']);
        
        return view('m2p.plan.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  Content $content
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $contentSlug)
    {
        $content = $this->content->where('slug', $contentSlug)->first();
        
        //$this->authorize('update', $content);
        
        $data = $this->getPreparedData($request, $content);
        
        $validation = Validator::make($data, [
            'site_id'            => 'required|exists:m2p_sites,id',
            'author'             => 'required|exists:m2p_users,id',
            'title'              => 'required|string',
            'content'            => 'required|string',
            'meta.plan.cost'    => 'required|numeric',
        ]);
        if ($validation->fails()) {
            return redirect()->back()
                        ->withErrors($validation)
                        ->withInput();
        }
        
        $content->update($data);
        $this->saveMeta($request, $content);

        flash()->success('Plano atualizado.');
        
        return $this->sendRedirectResponse($request);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($slug)
    {
        //$this->authorize('delete', $content);
        $content = $this->content->where('slug', $slug)->firstOrFail();
        
        $deleted = $content->delete();

        return redirect()->back();
    }
    
    /**
     * Prepara os dados para serem inseridos no banco.
     * 
     * @param Request $request
     * @param Content $content em atualiação, passe o objeto sendo atualizado
     * 
     * @return array
     * 
     * @see Mind2Press\Modules\Core\Traits\AuthorizesSitesTrait getPreparedData
     */
    protected function getPreparedData($request, Content $content = null) {
        $data = $this->getPreparedDataTrait($request);
        
        // Durante este método o content_type e content_type_id são necessários
        if ($request->has('content_type')) {
            $data['content_type_id'] = $this->getContentTypeByName($request->content_type)->id;
        } elseif ($request->has('content_type_id')) {
            $data['content_type'] = $this->getContentTypeById($request->content_type_id)->slug;
        }
        
        // Esta instância é criada pois os métodos de validação abaixo precisam de uma
        $contentInstance = $content;
        if ($contentInstance == null) {
            $contentInstance = new Content();
            $contentInstance->fill($data);
        }
        
        if ($request->has('author')) {
            //$this->authorize('assignAuthor', $contentInstance);
        } else {
            $data['author'] = Auth::user()->id;
        }
        
        if ($request->has('status') && $request->status == 'published') {
            //$this->authorize('publishTheContent', $contentInstance);
        }
        
        if (!$request->has('status')) {
            $data['status'] = 'published';
        }
        
        if (!$request->has('comment_status')) {
            $data['comment_status'] = 'closed';
        }
        
        $data['slug'] = $this->getPreparedSlug($request, $content);
        
        return $data;
    }
    
    /**
     * Método auxiliar para o getPreparedData. Retorna a slug para o content.
     * Nota: se o usuário não está enviando um slug na requisição, significa que um novo
     * slug deve ser gerado com base no título do content.
     * 
     * @param $array  $data dados da requisição tratados pelo método getPreparedData
     * @param Content $content
     * 
     * @return string
     * 
     * @see getPreparedData
     */
    protected function getPreparedSlug($data, Content $content = null)
    {
        $contentId = (null === $content) ? null : $content->id;
        
        if (! empty($data['slug'])) {
            $slug = (new SlugController())->generateAsString($data['content_type'], $data['slug'], $contentId);
        } else {
            $slug = (new SlugController())->generateAsString($data['content_type'], $data['title'], $contentId);
        }
        
        return $slug;
    }
    
    /**
     * Este método está neste controller pois somente aqui deve-se retornar essa
     * exception caso o content type não seja encontrado.
     * 
     * @param type $name
     * 
     * @return Repository
     * 
     * @throws ParameterNotFoundException
     */
    protected function getContentTypeByName($name)
    {
        $search = $this->contentType->where('name', $name)->get();
        
        if ($search->isEmpty()) {
            throw new ParameterNotFoundException('Content type not found.');
        }
        
        return $search->first();
    }
    
    /**
     * Este método está neste controller pois somente aqui deve-se retornar essa
     * exception caso o content type não seja encontrado.
     * 
     * @param type $id
     * 
     * @return type
     * 
     * @throws ParameterNotFoundException
     */
    protected function getContentTypeById($id)
    {
        $search = $this->contentType->where('id', $id)->get();
        
        if ($search->isEmpty()) {
            throw new ParameterNotFoundException('Content type not found.');
        }
        
        return $search->first();
    }
    
    /**
     * 
     * @param Request $request
     * @param Content $content
     * 
     * @return boolean
     */
    protected function saveMeta(Request $request, Content $content)
    {
        if (! $request->has('meta')) {
            return null;
        }
        
        $contentMeta = $request->meta;
        
        if (in_array('plan', array_keys($contentMeta))) {
            $meta = $this->contentMeta->firstOrCreate(['content_id' => $content->id, 'key' => 'plan']);
            $meta->update(['value' => $contentMeta['plan']]);
        }
        
        return true;
    }
    
    /**
     * 
     * @param Request $request
     * 
     * @return \Illuminate\Routing\Redirector|\Illuminate\Http\RedirectResponse
     */
    protected function sendRedirectResponse(Request $request)
    {
        $routeParameters = ['contentType'  => $request['content_type']];
        
        if ($request->has('taxonomy')) {
            $routeParameters['taxonomy'] = $request->taxonomy;
        }
        
        if ($request->has('taxonomyTerm')) {
            $routeParameters['taxonomyTerm'] = $request->taxonomyTerm;
        }
        
        $route = route('m2p::plan.index', $routeParameters);
        
        return redirect($route);
    }
}
