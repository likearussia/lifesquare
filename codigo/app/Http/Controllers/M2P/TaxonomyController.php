<?php

namespace Mind2Press\Http\Controllers\M2P;

use Validator;
use Illuminate\Http\Request;
use Mind2Press\Http\Controllers\Controller;
use Mind2Press\Modules\Content\Models\ContentTaxonomy;
use Mind2Press\Modules\Content\Models\ContentTaxonomyTerm;
use Mind2Press\Modules\Core\Exceptions\ParameterNotFoundException;
use Mind2Press\Modules\Content\Http\Controllers\Utils\SlugController;
use Mind2Press\Providers\Table\TaxonomyTermTableSortTrait;
use Mind2Press\Modules\Content\Traits\SearchContentTrait;

class TaxonomyController extends Controller
{
    use TaxonomyTermTableSortTrait, SearchContentTrait;
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $data = [];
        
        if (!$request->has('contentTaxonomy')) {
            abort(404);
        }
        
        $searchTerms = ContentTaxonomyTerm::select('*');
        
        // Busca
        $searchTerms = $this->searchContent($searchTerms, $request);
        
        // Ordenação
        $searchTerms = $this->sortTable($searchTerms, $request);

        $searchTaxonomy = ContentTaxonomy::where('name', $request->contentTaxonomy)->firstOrFail();
        $data['title']  = $searchTaxonomy->description;
        
        $data['terms'] = $searchTerms->where('content_taxonomy_id', $searchTaxonomy->id)->paginate(10);

        return view('m2p.taxonomy.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $data = [];
        
        $searchTerms = ContentTaxonomyTerm::orderBy('id', 'DESC');

        if ($request->has('contentTaxonomy')) {
            $searchTaxonomy = ContentTaxonomy::where('name', $request->contentTaxonomy)->firstOrFail();
            $data['terms']  = $searchTerms->where('content_taxonomy_id', $searchTaxonomy->id)->get();
            $data['title']  = $searchTaxonomy->description;
            $data['contentTaxonomy'] = $request->contentTaxonomy;
        } else {
            $data['terms'] = $searchTerms->get();
            $data['title'] = 'Termo da taxonomia';
        }
        
        return view('m2p.taxonomy.create', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //$this->authorize('create', Content::class);
        
        $data = $this->getPreparedData($request);
        
        $validation = Validator::make($data, [
            'content_taxonomy_id' => 'required|exists:m2p_content_taxonomies,id',
            'term' => 'required|string|unique:m2p_content_taxonomy_terms,term',
            'slug' => 'required|string|unique:m2p_content_taxonomy_terms,slug',
        ]);
        
        if ($validation->fails()) {
            return redirect()->back()
                        ->withErrors($validation)
                        ->withInput();
        }
        
        ContentTaxonomyTerm::create($data);
        
        if ($request->content_taxonomy_id == 2) {
            $mensagem = 'Categoria criada.';
        } else {
            $mensagem = 'Taxonomia criada.';
        }
        
        flash()->success($mensagem);
        
        return redirect(route('m2p::taxonomy.index', ['contentTaxonomy' => $data['content_taxonomy']]));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $content
     * @return \Illuminate\Http\Response
     */
    public function show(Content $content)
    {
        //$this->authorize('view', $content);
        
        $contentFound = $this->contentRepository->find($content->id);

        return response()->json([
            'result'  => 'success',
            'data'    => $contentFound->toArray(),
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($term)
    {
        //$this->authorize('update', $content);

        $data = [];
        
        $data['term'] = ContentTaxonomyTerm::where('slug', $term)->firstOrFail();

        $search = ContentTaxonomy::where('id', $data['term']['content_taxonomy_id'])->firstOrFail();
        $data['title'] = $search->description;
        $data['contentTaxonomy'] = $search->name;
        
        return view('m2p.taxonomy.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  Content $content
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $term)
    {
        //$this->authorize('update', $content);
        
        $taxonomyTerm = ContentTaxonomyTerm::where('slug', $term)->first();
        
        $data = $this->getPreparedData($request, $taxonomyTerm);
        
        $validation = Validator::make($data, [
            'content_taxonomy_id' => 'required|exists:m2p_content_taxonomies,id',
            'term' => 'required|string|unique:m2p_content_taxonomy_terms,term,' . $taxonomyTerm->id,
            'slug' => 'required|string|unique:m2p_content_taxonomy_terms,slug,' . $taxonomyTerm->id,
        ]);
        
        if ($validation->fails()) {
            return redirect()->back()
                        ->withErrors($validation)
                        ->withInput();
        }
        
        $taxonomyTerm->update($data);
        
        if ($request->content_taxonomy_id == 2) {
            $mensagem = 'Categoria atualizada.';
        } else {
            $mensagem = 'Taxonomia atualizada.';
        }
        
        flash()->success($mensagem);
        
        return redirect(route('m2p::taxonomy.index', ['contentTaxonomy' => $data['content_taxonomy']]));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($term)
    {
        //$this->authorize('delete', $content);
        $term = ContentTaxonomyTerm::where('slug', $term)->firstOrFail();
        
        $term->delete();

        return redirect()->back();
    }
    
    /**
     * Prepara os dados para serem inseridos no banco.
     * 
     * @param Request $request
     * @param Content $content em atualiação, passe o objeto sendo atualizado
     * 
     * @return array
     * 
     */
    protected function getPreparedData($request, ContentTaxonomyTerm $term = null) {
        $data = $request->all();
        
        // Durante este método o content_type e content_type_id são necessários
        if ($request->has('content_taxonomy')) {
            $data['content_taxonomy_id'] = $this->getTaxonomyByName($request->content_taxonomy)->id;
        } elseif ($request->has('content_type_id')) {
            $data['content_taxonomy'] = $this->getTaxonomyById($request->content_taxonomy_id)->name;
        }
        
        if ($term !== null) {
            $data['slug'] = $this->generateSlug($request->term, $term->id);
        } else {
            $data['slug'] = $this->generateSlug($request->term);
        }
        
        return $data;
    }
    
    /**
     * Generates a new slug per Taxonomy term
     * 
     * @param string $text
     * @param int $termId the id for a existent taxonomy term (util in update)
     * @param bool $forceNew
     * 
     * @return string
     * 
     * @TODO este método deve ficar em outro controller, como o método que gera slug por ContentType
     */
    public function generateSlug($text = null, $termId = null, $forceNew = false)
    {
        if ($text === null) {
            // str_random(30) porque 100 caracteres aleatórios ficam bem estranhos em uma URL
            $slug = str_random(30);
        } elseif ($forceNew === true) {
            $slug = str_limit(str_slug($text, '-'), 90, '') . str_random(10);
        } else {
            $slug = str_limit(str_slug($text, '-'), 100, '');
        }
        
        $searchParameters = ($termId === null) ? ['slug'=> $slug] : ['slug'=> $slug, ['id', '!=', $termId]];
        $search = ContentTaxonomyTerm::where($searchParameters)->get();
        if (! $search->isEmpty()) {
            return $this->generateSlug($text, $termId, true);
        }
        
        return $slug;
    }
    
    /**
     * Retorna uma instância do repository do content type cujo name é $name.
     * Este método está neste controller pois somente aqui deve-se retornar essa
     * exception caso o content type não seja encontrado.
     * 
     * @param type $name
     * 
     * @return Repository
     * 
     * @throws ParameterNotFoundException
     */
    protected function getTaxonomyByName($name)
    {
        $search = ContentTaxonomy::where('name', $name)->first();
        
        if ($search == null) {
            throw new ParameterNotFoundException('Taxonomy not found.');
        }
        
        return $search;
    }
    
    /**
     * Retorna uma instância do repository do content type cujo id é $id.
     * Este método está neste controller pois somente aqui deve-se retornar essa
     * exception caso o content type não seja encontrado.
     * 
     * @param type $id
     * 
     * @return type
     * 
     * @throws ParameterNotFoundException
     */
    protected function getTaxonomyById($id)
    {
        $search = ContentTaxonomy::where('id', $id)->first();
        
        if ($search == null) {
            throw new ParameterNotFoundException('Taxonomy not found.');
        }
        
        return $search;
    }
}
