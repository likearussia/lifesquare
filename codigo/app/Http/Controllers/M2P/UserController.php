<?php

namespace Mind2Press\Http\Controllers\M2P;

use Auth;
use Validator;
use Illuminate\Http\Request;
use Mind2Press\Modules\Core\Models\User;
use Mind2Press\Modules\Core\Models\Role;
use Mind2Press\Http\Controllers\Controller;
use Illuminate\Auth\Access\AuthorizationException;
use Mind2Press\Modules\Core\Traits\AuthorizesSitesTrait;

class UserController extends Controller
{
    use AuthorizesSitesTrait;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //$this->authorize('search', User::class);
        
        $data = [];
        
        $data['users'] = User::orderBy('id', 'desc')->paginate(10);

        return view('m2p.user.index', $data);
    }
    
    public function create()
    {
        $data = [];
        
        $data['roles'] = Role::pluck('display_name', 'id')->toArray();
        
        return view('m2p.user.create', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  UserCreateRequest $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //$this->authorize('create', User::class);
        
        $data = $this->getPreparedData($request);
        
        $validation = Validator::make($data, [
            'site_id'    => 'required|exists:m2p_sites,id',
            'first_name' => 'required|min:5|max:100',
            'last_name'  => 'sometimes|max:100',
            'login'      => 'required|min:3|max:50|alpha_dash|unique:m2p_users,login',
            'email'      => 'required|email|max:100',
            'password'   => 'required|min:5|confirmed',
            'role'       => 'required|exists:m2p_roles,id',
        ]);
        if ($validation->fails()) {
            return redirect()->back()
                        ->withErrors($validation)
                        ->withInput();
        }
        
        if (array_key_exists('password', $data) && !empty($data['password'])) {
            $data['password'] = bcrypt($data['password']);
        } else {
            unset($data['password']);
        }
        
        $user = User::create($data);
        $user->roles()->attach($data['role']);
        
        flash()->success('Usuário criado.');
        
        return redirect(route('m2p::user.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int $user
     *
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        //$this->authorize('view', $user);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $user
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($user)
    {
        //$this->authorize('update', $user);
        
        $data = [];
        
        $data['roles'] = Role::pluck('display_name', 'id')->toArray();
        $data['user']  = User::findOrFail($user);
        
        return view('m2p.user.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  UserUpdateRequest $request
     * @param  string            $user
     *
     * @return Response
     */
    public function update(Request $request, $user)
    {
        //$this->authorize('update', $user);
        
        $data = $this->getPreparedData($request);
        
        $user = User::where('id', $user)->firstOrFail();
                
        $validation = Validator::make($data, [
            'site_id'    => 'sometimes|exists:m2p_sites,id',
            'first_name' => 'sometimes|min:5|max:100',
            'last_name'  => 'sometimes|max:100',
            'login'      => 'sometimes|min:3|max:50|alpha_dash|unique:m2p_users,login,'.$user->id,
            'email'      => 'sometimes|email|max:100',
            'password'   => 'sometimes|min:5|confirmed',
            'role'       => 'required|exists:m2p_roles,id',
        ]);
        if ($validation->fails()) {
            return redirect()->back()
                        ->withErrors($validation)
                        ->withInput();
        }
        
        if (array_key_exists('password', $data) && !empty($data['password'])) {
            $data['password'] = bcrypt($data['password']);
        } else {
            unset($data['password']);
        }
        
        $user->update($data);
        $user->roles()->detach($user->roles()->first()->id);
        $user->roles()->attach($data['role']);
        
        flash()->success('Usuário atualizado.');
        
        return redirect(route('m2p::user.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $user
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($user)
    {
        //$this->authorize('delete', $user);
        
        User::where('id', $user)->delete();

        flash()->success('Usuário deletado.');
        
        return redirect(route('m2p::user.index'));
    }
}
