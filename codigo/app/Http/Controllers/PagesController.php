<?php

namespace Mind2Press\Http\Controllers;

use View;
use Illuminate\Http\Request;
use Mind2Press\Http\Requests;
use Mind2Press\Modules\Content\Models\Content;
use Mind2Press\Modules\Content\Models\ContentTaxonomy;
use Mind2Press\Modules\Content\Models\ContentTaxonomyTerm;

class PagesController extends Controller
{
    /**
     *
     * @var Content
     */
    protected $content;
    
    /**
     *
     * @var ContentTaxonomy
     */
    protected $contentTaxonomy;
    
    /**
     *
     * @var ContentTaxonomyTerm
     */
    protected $contentTaxonomyTerm;
    
    public function __construct(Content $content,
                                ContentTaxonomy $contentTaxonomy,
                                ContentTaxonomyTerm $contentTaxonomyTerm)
    {
        $this->content         = $content;
        $this->contentTaxonomy = $contentTaxonomy;
        $this->contentTaxonomyTerm = $contentTaxonomyTerm;
    }
    
    public function index(Request $request, $categorySlug, $contentSlug = null)
    {
        $data = [];
        
        $term = $this->contentTaxonomyTerm->where('slug', $categorySlug)
                                          ->with(['taxonomy' => function ($query) {
                                              $query->where('name', 'category');
                                          }])->firstOrFail();

        // #TODO por algum motivo a query teve que ser feita manualmente e com uso de RAW porque a query gerada pelo
        // eloquent, apesar de estar correta, não retornava resultados. Bug no Eloquent?
        $searchContent = $this->content->select('m2p_contents.*')
                              ->join('m2p_content_types as type', 'type.id', '=', 'm2p_contents.content_type_id')
                              ->join('m2p_content_meta as meta', 'meta.content_id', '=', 'm2p_contents.id')
                              ->where('m2p_contents.status', 'published')
                              ->where('type.name', 'page')
                              ->where('meta.key', 'taxonomies')
                              ->whereRaw("meta.value->\"$.category\" = '$term->id'")
                              ->orderBy('m2p_contents.id', 'ASC');
        
        if ($contentSlug !== null) {
            // Estou procurando um conteúdo específico
            $content = $searchContent->where('m2p_contents.slug', $contentSlug)->firstOrFail();
        } else {
            // Estou procurando uma categoria
            $content = $searchContent->get();
            if ($content->isEmpty()) {
                abort(404);
            }
        }
        
        $data['content'] = $content;
        
        // Obtendo os terms
        $count = count($content);
        if ($count == 1) {
            $data['content']['taxonomy'] = [
                'terms' => $content->getTaxonomiesTerms()->toArray(),
            ];
        } elseif ($count > 1) {
            foreach ($content as $k => $v) {
                $data['content'][$k]['taxonomy'] = [
                    'terms' => $v->getTaxonomiesTerms()->toArray(),
                ];
            }
        }
        
        return view($this->resolvesView($categorySlug, $contentSlug), $data);
    }
    
    /**
     * 
     * @param string $categorySlug
     * @param string $contentSlug
     * 
     * @return string file path
     */
    public function resolvesView($categorySlug, $contentSlug = null)
    {
        $basePath = 'site/categories';
        
        $file = ($contentSlug === null) ? 'index' : 'content';
        
        $view = "$basePath/{$categorySlug}/$file";
        if (!View::exists($view)) {
            $view = "$basePath/default/$file";
        }
        
        return $view;
    }
}
