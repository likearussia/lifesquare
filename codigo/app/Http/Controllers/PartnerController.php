<?php

namespace Mind2Press\Http\Controllers;

use Mind2Press\Modules\Content\Models\Content;

class PartnerController extends Controller
{
    /**
     *
     * @var Content
     */
    protected $content;
    
    public function __construct(Content $content)
    {
        $this->content = $content;
    }
    
    public function getData()
    {
        $data = [];

        $data['partners'] = $this->content->ofContentType('partner')
                                          ->published()
                                          ->get();
        
        return $data;
    }
}
