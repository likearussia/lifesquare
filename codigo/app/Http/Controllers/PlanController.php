<?php

namespace Mind2Press\Http\Controllers;

use Mind2Press\Mail\Plan;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Mind2Press\Modules\Content\Models\Content;
use Mind2Press\Modules\Content\Models\ContentType;

class PlanController extends Controller
{
    /**
     *
     * @var Content
     */
    protected $content;
    
    public function __construct(Content $content, ContentType $contentType)
    {
        $this->content = $content;
        $this->contentType = $contentType;
    }
    
    public function index()
    {
        $data = [];

        $data['contentType'] = $this->contentType->where('name', 'plan')->firstOrFail();

        $data['plans'] = $this->content->ofContentType($data['contentType']->id)
                                       ->published()
                                       ->with('meta')
                                       ->get();
        
        return view('site.pages.planos', $data);
    }
    
    /**
     * Send the message.
     * 
     * @param Request $request
     */
    public function sendMessage(Request $request)
    {
        $this->validateMessage($request);
        
        $send = Mail::to(config('mail.contact.address'))->send(new Plan($request->all()));
        
        if ($send === null) {
            flash()->success('E-mail enviado com sucesso!');
        } else {
            flash()->error('Ocorreu um erro ao enviar a mensagem.');
        }
        
        return redirect()->back();
    }
    
    /**
     * Validate the message.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return void
     */
    protected function validateMessage(Request $request)
    {
        $messages = [
            'plano.required' => 'O campo plano é obrigatório. Por favor, selecione um.',
        ];
        
        $this->validate($request, [
            'nome'      => 'required|min:3|max:100',
            'email'     => 'required|email',
            'telefone'  => 'required',
            'plano'     => 'required',
            'mensagem'  => 'sometimes|min:3|max:2000'
        ], $messages);
    }
}
