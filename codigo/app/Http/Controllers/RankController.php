<?php

namespace Mind2Press\Http\Controllers;

use DB;
use Illuminate\Http\Request;
use Mind2Press\Modules\Content\Models\Content;
use Mind2Press\Modules\Content\Models\ContentType;
use Mind2Press\Modules\Content\Models\ContentTaxonomy;
use Mind2Press\Modules\Content\Models\ContentTaxonomyTerm;

class RankController extends Controller
{
    /**
     *
     * @var Content
     */
    protected $content;
    
    /**
     *
     * @var ContentType
     */
    protected $contentType;
    
    /**
     *
     * @var ContentTaxonomy
     */
    protected $contentTaxonomy;
    
    /**
     *
     * @var ContentTaxonomyTerm
     */
    protected $contentTaxonomyTerm;
    
    public function __construct(Content $content,
                                ContentType $contentType,
                                ContentTaxonomy $contentTaxonomy,
                                ContentTaxonomyTerm $contentTaxonomyTerm)
    {
        $this->content         = $content;
        $this->contentType     = $contentType;
        $this->contentTaxonomy = $contentTaxonomy;
        $this->contentTaxonomyTerm = $contentTaxonomyTerm;
    }
    
    public function index(Request $request)
    {
        $data = [];
        
        $data['contentType'] = $this->contentType->where('name', 'player')->firstOrFail();
        $data['taxonomy']    = $this->contentTaxonomy->where('name', 'rank')->firstOrFail();
        $data['ranks']       = $this->contentTaxonomyTerm->where('content_taxonomy_id', $data['taxonomy']->id)
                                                         ->orderBy('term', 'ASC')->get();

        // Os valores da coluna m2p_content_meta.value estavam vindo entre ", por isso utilizo REPLACE
        $data['players'] = $this->content->select(['m2p_contents.*',
                                        DB::raw("REPLACE(meta.value->'$.name', '\"', '')  as player_name"),
                                        DB::raw("REPLACE(meta.value->'$.rank', '\"', '')  as player_rank"),
                                        DB::raw("REPLACE(meta.value->'$.points', '\"', '')  as player_points")])
                                        ->join('m2p_content_meta as meta', 'meta.content_id', '=', 'm2p_contents.id')
                                        ->where('m2p_contents.content_type_id', $data['contentType']->id)
                                        ->where('meta.key', 'player')
                                        ->get();
        
        return view('site.pages.rank', $data);
    }
    
    /**
     * Retorna os dados necessários para montar o mini rank exibido no rodapé de serviços.
     * 
     * @return array
     */
    public function getFooterData()
    {
        $data = [];
        
        $data['contentType'] = $this->contentType->where('name', 'player')->first();
        $data['taxonomy']    = $this->contentTaxonomy->where('name', 'rank')->first();
        $data['ranks']       = $this->contentTaxonomyTerm->where('content_taxonomy_id', $data['taxonomy']->id)
                                                         ->orderBy('term', 'ASC')->get();
        
        $data['ranks_femininos'] = $data['ranks']->filter(function ($value, $key) {
            return str_contains($value, 'feminino');
        });
        $data['ranks_masculinos'] = $data['ranks']->filter(function ($value, $key) {
            return str_contains($value, 'masculino');
        });
        
        $data['players'] = [];
        foreach ($data['ranks'] as $rank) {
        // Os valores da coluna m2p_content_meta.value estavam vindo entre ", por isso utilizo REPLACE
        $data['players'][$rank->id] = $this->content->select(['m2p_contents.title',
                                        DB::raw("JSON_UNQUOTE(meta.value->'$.name')  as player_name"),
                                        DB::raw("JSON_UNQUOTE(meta.value->'$.rank')  as player_rank"),
                                        DB::raw("JSON_UNQUOTE(meta.value->'$.points')  as player_points")])
                                        ->join('m2p_content_meta as meta', 'meta.content_id', '=', 'm2p_contents.id')
                                        ->where('m2p_contents.content_type_id', $data['contentType']->id)
                                        ->where('meta.key', 'player')
                                        ->whereRaw("meta.value->'$.rank' = '{$rank->id}'")
                                        ->orderByRaw("CAST(JSON_EXTRACT(value, '$.points') AS UNSIGNED) DESC")
                                        ->first();
        }
        
        return $data;
    }
}
