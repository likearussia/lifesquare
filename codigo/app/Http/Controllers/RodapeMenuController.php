<?php

namespace Mind2Press\Http\Controllers;

use Mind2Press\Modules\Content\Models\Content;
use Mind2Press\Modules\Content\Models\ContentMeta;
use Mind2Press\Modules\Content\Models\ContentType;
use Mind2Press\Modules\Content\Models\ContentTaxonomy;
use Mind2Press\Modules\Content\Models\ContentTaxonomyTerm;

class RodapeMenuController extends Controller
{
    /**
     *
     * @var Content
     */
    protected $content;
    
    /**
     *
     * @var ContentType
     */
    protected $contentType;
    
    /**
     *
     * @var ContentMeta
     */
    protected $contentMeta;
    
    /**
     *
     * @var ContentTaxonomy
     */
    protected $contentTaxonomy;
    
    /**
     *
     * @var ContentTaxonomyTerm
     */
    protected $contentTaxonomyTerm;

    public function __construct(Content $content,
                                ContentType $contentType,
                                ContentMeta $contentMeta,
                                ContentTaxonomy $contentTaxonomy,
                                ContentTaxonomyTerm $contentTaxonomyTerm)
    {
        $this->content         = $content;
        $this->contentType     = $contentType;
        $this->contentMeta     = $contentMeta;
        $this->contentTaxonomy = $contentTaxonomy;
        $this->contentTaxonomyTerm = $contentTaxonomyTerm;
    }
    
    /**
     * Retorna dados necessários para a geração do menu do rodapé
     * 
     * @return array
     */
    public function getData()
    {
        $data = [];
        
        $data['contentType'] = $this->contentType->where('name', 'page')->firstOrFail();
        $data['category']    = $this->contentTaxonomy->where('name', 'category')->firstOrFail();
        $data['categories']  = $this->contentTaxonomyTerm->where('content_taxonomy_id', $data['category']->id)->get();
        
        $data['content'] = [];
        foreach ($data['categories'] as $category) {
            $data['content'][$category->id] = $this->content
                                        ->select(['title', 'slug'])
                                        ->where('content_type_id', $data['contentType']->id)
                                        ->join('m2p_content_meta as meta', 'meta.content_id', '=', 'm2p_contents.id')
                                        ->whereRaw("meta.value->\"$.category\" = '{$category->id}'")
                                        ->orderBy('parent')
                                        ->get();
        }
        
        return $data;
    }
}
