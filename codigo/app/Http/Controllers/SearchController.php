<?php

namespace Mind2Press\Http\Controllers;

use Illuminate\Http\Request;
use Mind2Press\Http\Requests;
use Mind2Press\Modules\Content\Models\Content;
use Mind2Press\Modules\Content\Models\ContentType;

class SearchController extends Controller
{
    /**
     *
     * @var Content
     */
    protected $content;
    
    /**
     *
     * @var ContentType
     */
    protected $contentType;
    
    public function __construct(Content $content,
                                ContentType $contentType)
    {
        $this->content = $content;
        $this->contentType = $contentType;
    }
    
    public function search(Request $request)
    {
        if (!$request->has('q')) {
            abort(404);
        }
        
        $data      = [];
        $data['q'] = $request->q;
        
        $data['contentTypes'] = $this->contentType->whereIn('name', ['page', 'post'])->get();
        
        $data['content'] = $this->content
                                ->whereIn('content_type_id', $data['contentTypes']->pluck('id'))
                                ->whereRaw("MATCH(content, title) AGAINST(? IN BOOLEAN MODE)", ['*' . $data['q'] . '*'])
                                ->paginate(10);
        
        return view('site.pages.busca', $data);
    }
}
