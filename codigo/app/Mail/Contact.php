<?php

namespace Mind2Press\Mail;

use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class Contact extends Mailable
{
    use SerializesModels;

    /**
     * Contact data
     * 
     * @var $data
     */
    protected $data;
    
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(array $data)
    {
        $this->data = $data;
        
        if (isset($this->data['mensagem'])) {
            $this->data['mensagem'] = nl2br(strip_tags($this->data['mensagem']));
        }
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('Contato pelo site')
                    ->view('site.emails.contact')
                    ->with('data', $this->data);
    }
}
