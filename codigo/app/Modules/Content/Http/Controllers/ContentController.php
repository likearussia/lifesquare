<?php

namespace Mind2Press\Modules\Content\Http\Controllers;

use Auth;
use Validator;
use Illuminate\Http\Request;
use Mind2Press\Modules\Content\Models\Content;
use Mind2Press\Modules\Core\Traits\AuthorizesSitesTrait;
use Mind2Press\Modules\Core\Repositories\OptionRepository;
use Mind2Press\Modules\Content\Repositories\ContentRepository;
use Mind2Press\Modules\Content\Repositories\ContentTypeRepository;
use Mind2Press\Modules\Core\Exceptions\ParameterNotFoundException;
use Mind2Press\Modules\Content\Http\Controllers\Utils\SlugController;

use Mind2Press\Modules\Core\Models\Option;

class ContentController extends Controller
{
    use AuthorizesSitesTrait {
        getPreparedData as getPreparedDataTrait;
    }
    
    /**
     *
     * @var OptionRepository
     */
    protected $optionRepository;
    
    /**
     *
     * @var ContentRepository
     */
    protected $contentRepository;
    
    /**
     *
     * @var ContentTypeRepository
     */
    protected $contentTypeRepository;
    
    public function __construct(OptionRepository $optionRepository,
                                ContentRepository $contentRepository,
                                ContentTypeRepository $contentTypeRepository) {
        $this->optionRepository = $optionRepository;
        $this->contentRepository = $contentRepository;
        $this->contentTypeRepository = $contentTypeRepository;
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->contentRepository->pushCriteria(app('Prettus\Repository\Criteria\RequestCriteria'));
        $options = $this->contentRepository->paginate($this->optionRepository->getResultsPerPage());

        return response()->json([
            'result' => 'success',
            'data' => $options,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->authorize('create', Content::class);
        
        $data = $this->getPreparedData($request);
        
        $validation = Validator::make($data, [
            'site_id'         => 'required|exists:m2p_sites,id',
            'author'          => 'required|exists:m2p_users,id',
            'title'           => 'required|string',
            'excerpt'         => 'sometimes|string',
            'content'         => 'required|string',
            'content_type_id' => 'required|exists:m2p_content_types,id',
            'status'          => 'required|string|in:published,draft,pending,trash',
            'slug'            => 'required|string|unique:m2p_contents,slug',
            'comment_status'  => 'required|in:opened,closed',
        ]);
        
        if ($validation->fails()) {
            return response()->json([
                'result' => 'error',
                'message' => ['validation' => $validation->errors()]
            ], 200);
        }
        
        $option = $this->contentRepository->create($data);
        
        $response = [
            'result'  => 'success',
            'message' => 'Conteúdo criado.',
            'data'    => $option->toArray(),
        ];

        return response()->json($response);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $content
     * @return \Illuminate\Http\Response
     */
    public function show(Content $content)
    {
        //$this->authorize('view', $content);
        
        $contentFound = $this->contentRepository->find($content->id);

        return response()->json([
            'result'  => 'success',
            'data'    => $contentFound->toArray(),
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Content $content)
    {
        $this->authorize('update', $content);
        
        $contentFound = $this->contentRepository->find($content->id);

        $response = [
            'result'  => 'success',
            'data'    => $contentFound->toArray(),
        ];

        return response()->json($response);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  Content $content
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Content $content)
    {
        $this->authorize('update', $content);
        
        $data = $this->getPreparedData($request, $content);
        
        $validation = Validator::make($data, [
            'site_id'         => 'sometimes|exists:m2p_sites,id',
            'author'          => 'sometimes|exists:m2p_users,id',
            'title'           => 'sometimes|string',
            'excerpt'         => 'sometimes|string',
            'content'         => 'sometimes|string',
            'content_type_id' => 'sometimes|exists:m2p_content_types,id',
            'status'          => 'required|string|in:published,draft,pending,trash',
            'slug'            => 'sometimes|string|unique:m2p_contents,slug,'.$content->id,
            'comment_status'  => 'sometimes|in:opened,closed',
        ]);
        if ($validation->fails()) {
            return response()->json([
                'result' => 'error',
                'message' => ['validation' => $validation->errors()]
            ], 200);
        }
        
        $contentUpdated = $this->contentRepository->update($data, $content->id);

        $response = [
            'result'  => 'success',
            'message' => 'Conteúdo atualizado.',
            'data'    => $contentUpdated->toArray(),
        ];

        return response()->json($response);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Content $content)
    {
        $this->authorize('delete', $content);
        
        $deleted = $this->contentRepository->delete($content->id);

        return response()->json([
            'result' => 'success',
            'message' => 'Conteúdo deletado.',
            'deleted' => $deleted,
        ]);
    }
    
    /**
     * Prepara os dados para serem inseridos no banco.
     * 
     * @param Request $request
     * @param Content $content em atualiação, passe o objeto sendo atualizado
     * 
     * @return array
     * 
     * @see Mind2Press\Modules\Core\Traits\AuthorizesSitesTrait getPreparedData
     */
    protected function getPreparedData($request, Content $content = null) {
        $data = $this->getPreparedDataTrait($request);
        
        
        // Durante este método o content_type e content_type_id são necessários
        if ($request->has('content_type')) {
            $data['content_type_id'] = $this->getContentTypeByName($request->content_type)->id;
        } elseif ($request->has('content_type_id')) {
            $data['content_type'] = $this->getContentTypeById($request->content_type_id)->slug;
        }
        
        // Esta instância é criada pois os métodos de validação abaixo precisam de uma
        $contentInstance = $content;
        if ($contentInstance == null) {
            $contentInstance = new Content();
            $contentInstance->fill($data);
        }
        
        if ($request->has('author')) {
            $this->authorize('assignAuthor', $contentInstance);
        } else {
            $data['author'] = Auth::user()->id;
        }
        
        if ($request->has('status') && $request->status == 'published') {
            $this->authorize('publishTheContent', $contentInstance);
        }
        
        $data['slug'] = $this->getPreparedSlug($request, $content);
        
        return $data;
    }
    
    /**
     * Método auxiliar para o getPreparedData. Retorna a slug para o content.
     * Nota: se o usuário não está enviando um slug na requisição, significa que um novo
     * slug deve ser gerado com base no título do content.
     * 
     * @param $array  $data dados da requisição tratados pelo método getPreparedData
     * @param Content $content
     * 
     * @return string
     * 
     * @see getPreparedData
     */
    protected function getPreparedSlug($data, Content $content = null)
    {
        $contentId = (null === $content) ? null : $content->id;
                
        if (! empty($data['slug'])) {
            $slug = (new SlugController())->generateAsString($data['content_type'], $data['slug'], $contentId);
        } else {
            $slug = (new SlugController())->generateAsString($data['content_type'], $data['title'], $contentId);
        }
        
        return $slug;
    }
    
    /**
     * Retorna uma instância do repository do content type cujo name é $name.
     * Este método está neste controller pois somente aqui deve-se retornar essa
     * exception caso o content type não seja encontrado.
     * 
     * @param type $name
     * 
     * @return Repository
     * 
     * @throws ParameterNotFoundException
     */
    protected function getContentTypeByName($name)
    {
        $search = $this->contentTypeRepository->findByField('name', $name);
        
        if ($search->isEmpty()) {
            throw new ParameterNotFoundException('Content type not found.');
        }
        
        return $search->first();
    }
    
    /**
     * Retorna uma instância do repository do content type cujo id é $id.
     * Este método está neste controller pois somente aqui deve-se retornar essa
     * exception caso o content type não seja encontrado.
     * 
     * @param type $id
     * 
     * @return type
     * 
     * @throws ParameterNotFoundException
     */
    protected function getContentTypeById($id)
    {
        $search = $this->contentTypeRepository->findByField('id', $id);
        
        if ($search->isEmpty()) {
            throw new ParameterNotFoundException('Content type not found.');
        }
        
        return $search->first();
    }
}
