<?php

namespace Mind2Press\Modules\Content\Http\Controllers\Utils;

use Mind2Press\Modules\Content\Http\Controllers\Controller;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Mind2Press\Modules\Content\Repositories\ContentRepository;

class SlugController extends Controller
{
    /**
     * Generates a new slug per ContentType
     * 
     * @param string $contentType the content type slug
     * @param string $text
     * @param int $contentId the id for a existent content type (util in update)
     * 
     * @return Response|json
     * 
     * @see generateAsString
     */
    public function generate($contentType, $text = null, $contentId = null)
    {
        $slug = $this->generateAsString($contentType, $text, $contentId);
        
        return response()->json([
            'result'  => 'success',
            'data' => ['slug' => $slug],
        ]);
    }
    
    /**
     * Generates a new slug per ContentType
     * 
     * @param string $contentType the content type slug
     * @param string $text
     * @param int $contentId the id for a existent content type (util in update)
     * @param bool $forceNew
     * 
     * @return string
     */
    public function generateAsString($contentType, $text = null, $contentId = null, $forceNew = false)
    {
        if ($text === null) {
            // str_random(30) porque 100 caracteres aleatórios ficam bem estranhos em uma URL
            $slug = str_random(30);
        } elseif ($forceNew === true) {
            $slug = str_limit(str_slug($text, '-'), 90, '') . str_random(10);
        } else {
            $slug = str_limit(str_slug($text, '-'), 100, '');
        }
        
        $searchParameters = ($contentId === null) ? ['slug'=> $slug] : ['slug'=> $slug, ['id', '!=', $contentId]];
        $search = $this->resolveRepository($contentType)->findWhere($searchParameters);
        if (! $search->isEmpty()) {
            return $this->generateAsString($contentType, $text, $contentId, true);
        }
        
        return $slug;
    }
    
    /**
     * Gets the correct repository for the content type
     * 
     * @param type $contentType
     * 
     * @return type
     * 
     * @throws NotFoundHttpException
     */
    protected function resolveRepository($contentType)
    {
        return app(ContentRepository::class);
    }
}


