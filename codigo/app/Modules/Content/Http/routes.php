<?php

/*
|--------------------------------------------------------------------------
| Routes
|--------------------------------------------------------------------------
|
| Here is where you can register routes for your Content module. These
| routes are loaded by the ContentServiceProvider within a group which
| is assigned the "api" middleware group.
|
*/

Route::group(['prefix' => 'v0', 'middleware' => 'm2p-cookie'], function() {
    Route::get('/content/{content}', [
        'uses' => 'ContentController@show', 'as' => 'content.show',
    ]);
    
    /**
     * Rotas autenticadas
     */
    Route::group(['middleware' => 'auth'], function() {
        Route::resource('content', 'ContentController', ['except' => 'show']);
        
        Route::group(['prefix' => 'utils', 'namespace' => 'Utils', 'as' => 'utils::'], function() {
            Route::match(['get'], '/slug/generate/{contentType}/{text?}/{contentId?}', [
                'uses' => 'SlugController@generate', 'as' => 'slug.generate'
            ]);
        });
    });
});
