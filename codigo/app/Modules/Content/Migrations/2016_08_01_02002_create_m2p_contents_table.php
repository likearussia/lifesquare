<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateM2pContentsTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('m2p_contents', function(Blueprint $table) {
            $table->increments('id');
            $table->integer('site_id')->unsigned();
            $table->foreign('site_id')->references('id')->on('m2p_sites')->onUpdate('cascade')->onDelete('cascade');
            $table->integer('author')->unsigned();
            $table->foreign('author')->references('id')->on('m2p_users')->onUpdate('cascade')->onDelete('cascade');
            $table->text('title');
            $table->text('excerpt')->nullable();
            $table->text('content');
            $table->text('filtered')->nullable();
            $table->text('main_image')->nullable();
            $table->integer('content_type_id')->unsigned();
            $table->foreign('content_type_id')->references('id')->on('m2p_content_types')->onUpdate('cascade')->onDelete('cascade');
            $table->string('status', 20)->index();
            $table->string('slug', 200)->index();
            $table->string('comment_status', 20);
            $table->integer('comment_count')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::drop('m2p_contents');
    }

}
