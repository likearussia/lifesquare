<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateM2pContentTaxonomiesTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('m2p_content_taxonomies', function(Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('slug', 50)->index();
            $table->integer('content_type_id')->unsigned();
            $table->foreign('content_type_id')->references('id')->on('m2p_content_types')->onUpdate('cascade')->onDelete('cascade');
            $table->json('capabilities')->nullable();
            $table->integer('site_id')->unsigned();
            $table->foreign('site_id')->references('id')->on('m2p_sites')->onUpdate('cascade')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::drop('m2p_content_taxonomies');
    }

}
