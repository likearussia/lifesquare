<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateM2pContentTaxonomyTermsTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('m2p_content_taxonomy_terms', function(Blueprint $table) {
            $table->increments('id');
            $table->integer('content_taxonomy_id')->unsigned();
            $table->foreign('content_taxonomy_id')->references('id')->on('m2p_content_taxonomies')->onUpdate('cascade')->onDelete('cascade');
            $table->text('term');
            $table->string('slug', 50)->index();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::drop('m2p_content_taxonomy_terms');
    }

}
