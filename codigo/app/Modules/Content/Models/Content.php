<?php

namespace Mind2Press\Modules\Content\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Mind2Press\Modules\Content\Models\ContentType;
use Spatie\MediaLibrary\HasMedia\Interfaces\HasMedia;
use Spatie\MediaLibrary\HasMedia\Interfaces\HasMediaConversions;

class Content extends Model implements HasMediaConversions
{
    use HasMediaTrait;
    
    public $table = 'm2p_contents';
    
    protected $fillable = ['author', 'title', 'excerpt', 'content', 'filtered', 'content_type_id', 'status', 'slug',
                          'comment_status', 'site_id'];
    
    /**
     * Get the route key for the model.
     * 
     * @return string
     */
    public function getRouteKeyName() {
        return 'slug';
    }
    
    /**
     * Obtem o valor do atributo 'created_at'
     *
     * @param  string  $value
     * @return string
     */
    public function getCreatedAtAttribute($value)
    {
        return Carbon::parse($value)->format('d/m/Y H:i:s');
    }
    
    /**
     * Obtem o relacionamento com a tabela de usuários
     * 
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function user() {
        return $this->belongsTo('Mind2Press\Modules\Core\Models\User', 'author');
    }
    
    /**
     * Obtem o relacionamento com a tabela de meta informações
     * 
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function meta()
    {
       return $this->hasMany('Mind2Press\Modules\Content\Models\ContentMeta');
    }
    
    /**
     * Obtem o relacionamento com a tabela ContentTypes
     * 
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function type()
    {
        return $this->belongsTo('Mind2Press\Modules\Content\Models\ContentType', 'content_type_id', 'id');
    }
    
    /**
     * Cria um scopo para que a query retorne apenas contents com a slug fornecida.
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeOfSlug($query, $slug)
    {
        return $query->where($this->table . '.slug', $slug);
    }
    
    /**
     * Cria um scopo para que a query retorne apenas contents com a slug fornecida.
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopePublished($query)
    {
        return $query->where($this->table . '.status', 'published');
    }
    
    /**
     * Cria um scopo para que a query retorne resultados ordenados pela trait ReorderingTrait
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeReorder($query)
    {
        return $query->orderBy($this->table . '.parent', 'ASC');
    }
    
    /**
     * Cria um scopo para que a query retorne apenas contents cujo content type é $contentType
     *
     * @param mixed $contentType ID ou name do content type
     * 
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeOfContentType($query, $contentType)
    {
        if (is_numeric($contentType)) {
            $contentTypeId = $contentType;
        } else {
            $search = ContentType::where('name', $contentType)->first();
            $contentTypeId = (is_null($search)) ? null : $search->id;
        }
        
        return $query->where($this->table . '.content_type_id', $contentTypeId);
    }
    
    public function scopeOfTaxonomy($query, $taxonomy) {
        
    }
    
    public function scopeOfTaxonomyTerm($query, $taxonomyTerm) {
        
    }
    
    /**
     * Faz conversões de tamanho das imagens vinculadas a um content.
     * 
     * Nota: ao alterar as configurações de uma conversão existente execute: php artisan medialibrary:regenerate
     * 
     * @TODO este método deve ler os tamanhos de imagem especificados nas configurações do M2P.
     */
    public function registerMediaConversions()
    {
        $this->addMediaConversion('adminThumb')
             ->setManipulations(['w' => 368, 'h' => 232, 'fm' => 'src', 'fit' => 'contain'])
             ->performOnCollections('main', 'chamada', 'banner')
             ->nonQueued();
        
        $this->addMediaConversion('adminListThumb')
             ->setManipulations(['h' => 70, 'fm' => 'png', 'fit' => 'max'])
             ->performOnCollections('main', 'banner', 'partner', 'infrastructure')
             ->nonQueued();
        
        $this->addMediaConversion('chamada')
             ->setManipulations(['w' => 740, 'h' => 395, 'fm' => 'src', 'fit' => 'stretch'])
             ->performOnCollections('chamada')
             ->nonQueued();
        
        $this->addMediaConversion('banner')
             ->setManipulations(['w' => 1872, 'h' => 773, 'fm' => 'png', 'fit' => 'fill'])
             ->performOnCollections('main')
             ->nonQueued();
        
        $this->addMediaConversion('playerThumb')
             ->setManipulations(['w' => 200, 'h' => 200, 'fm' => 'png', 'fit' => 'crop'])
             ->performOnCollections('player')
             ->nonQueued();
        
        $this->addMediaConversion('blogThumb')
             ->setManipulations(['w' => 386, 'h' => 240, 'fm' => 'png', 'fit' => 'crop'])
             ->performOnCollections('chamada')
             ->nonQueued();
        $this->addMediaConversion('blog')
             ->setManipulations(['w' => 700, 'fm' => 'png', 'fit' => 'fill'])
             ->performOnCollections('main')
             ->nonQueued();
        
        $this->addMediaConversion('galeryThumb')
             ->setManipulations(['w' => 290, 'h' => 150, 'fm' => 'png', 'fit' => 'crop'])
             ->performOnCollections('galery')
             ->nonQueued();
        $this->addMediaConversion('galery')
             ->setManipulations(['w' => 900, 'h' => 560, 'fm' => 'png', 'fit' => 'max'])
             ->performOnCollections('galery')
             ->nonQueued();
        
        $this->addMediaConversion('banner')
             ->setManipulations(['w' => 1600, 'h' => 500, 'fm' => 'png', 'fit' => 'crop'])
             ->performOnCollections('banner')
             ->nonQueued();
        
        $this->addMediaConversion('partner')
             ->setManipulations(['w' => 220, 'h' => 110, 'fm' => 'jpg', 'fit' => 'crop'])
             ->performOnCollections('partner')
             ->nonQueued();
        
        $this->addMediaConversion('infrastructureThumb')
             ->setManipulations(['w' => 262, 'h' => 200, 'fm' => 'png', 'fit' => 'crop'])
             ->performOnCollections('infrastructure')
             ->nonQueued();
        $this->addMediaConversion('infrastructure')
             ->setManipulations(['w' => 1024, 'h' => 768, 'fm' => 'png', 'fit' => 'max'])
             ->performOnCollections('infrastructure')
             ->nonQueued();
    }
    
    /**
     * 
     * @param string $metaKey
     * @param array $metaJsonKey
     * @param boolean $onlyOne
     * 
     * @return Eloquent
     */
    public function getMeta($metaKey, $metaJsonKey = null, $onlyOne = true)
    {
        $search = $this->meta()->where('key', $metaKey);
        
        if ($metaJsonKey !== null) {
            $search->where($metaJsonKey);
        }
        
        return ($onlyOne === true) ? $search->first() : $search->get();
    }
    
    /**
     * Retorna uma collection contendo um indice para cada taxonomy e nele os valores para os terms.
     * 
     * Nota: se a taxonomia for category, apenas um resultado será retornado.
     * 
     * @return Colletion
     */
    public function getTaxonomiesTerms()
    {
        $results = $this->getMeta('taxonomies');
        
        $taxonomies = [];
        // $results->value é um campo json que é transformado em array pelo casting do laravel.
        // Quando a key é taxonomies as chaves do json são os nomes das taxonomias.
        foreach (array_keys($results->value) as $taxonomyName) {
            $termIds = $results->value[$taxonomyName];
            $searchTerm = ContentTaxonomyTerm::whereIn('id', [$termIds]);
            
            $termFound = ($taxonomyName == 'category') ? $searchTerm->first()->toArray() : $searchTerm->get()->toArray();
            
            $taxonomies[$taxonomyName] = $termFound;
        }
        
        return collect($taxonomies);
    }
}
