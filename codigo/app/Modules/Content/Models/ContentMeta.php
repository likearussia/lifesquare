<?php

namespace Mind2Press\Modules\Content\Models;

use Illuminate\Database\Eloquent\Model;

class ContentMeta extends Model
{
    protected $table = 'm2p_content_meta';
    
    protected $fillable = ['content_id', 'key', 'value'];
    
    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'value' => 'array',
    ];
    
    /**
     * Define um scopo para que a query retorne apenas registros de taxonomias.
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeTaxonomies($query)
    {
        return $query->where('key', 'taxonomies');
    }
}
