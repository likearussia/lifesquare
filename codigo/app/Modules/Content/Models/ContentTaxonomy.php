<?php

namespace Mind2Press\Modules\Content\Models;

use Illuminate\Database\Eloquent\Model;

class ContentTaxonomy extends Model
{
    protected $table = 'm2p_content_taxonomies';
    
    protected $fillable = ['name', 'slug', 'content_type_id', 'capabilities'];
    
    /**
     * Obtem o relacionamento com o model ContentTaxonomyTerm
     * 
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function terms()
    {
        return $this->hasMany('Mind2Press\Modules\Content\Models\ContentTaxonomyTerm');
    }
}
