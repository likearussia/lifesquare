<?php

namespace Mind2Press\Modules\Content\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class ContentTaxonomyTerm extends Model
{
    public $table = 'm2p_content_taxonomy_terms';
    
    protected $fillable = ['content_taxonomy_id', 'term', 'slug', 'description'];
    
    /**
     * Get the route key for the model.
     * 
     * @return string
     */
    public function getRouteKeyName() {
        return 'slug';
    }
    
    /**
     * Obtem o valor do atributo 'created_at'
     *
     * @param  string  $value
     * @return string
     */
    public function getCreatedAtAttribute($value)
    {
        return Carbon::parse($value)->format('d/m/Y H:i:s');
    }
    
    /**
     * Obtem o relacionamento com o model ContentTaxonomy
     * 
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function taxonomy()
    {
        return $this->belongsTo('Mind2Press\Modules\Content\Models\ContentTaxonomy', 'content_taxonomy_id');
    }
    
    /**
     * Cria um scopo para que a query retorne apenas terms com a slug fornecida.
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeOfSlug($query, $slug)
    {
        return $query->where($this->table . '.slug', $slug);
    }
}
