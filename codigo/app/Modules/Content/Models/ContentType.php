<?php

namespace Mind2Press\Modules\Content\Models;

use Illuminate\Database\Eloquent\Model;

class ContentType extends Model
{
    protected $table = 'm2p_content_types';
    
    protected $fillable = ['name', 'description', 'slug'];
}
