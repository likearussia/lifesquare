<?php

namespace Mind2Press\Modules\Content\Policies;

use Mind2Press\Modules\Core\Models\User;
use Mind2Press\Modules\Content\Models\Content;
use Illuminate\Auth\Access\HandlesAuthorization;
use Mind2Press\Modules\Content\Traits\ContentTrait;

class ContentPolicy
{
    use HandlesAuthorization, ContentTrait;

    /**
     * Determine whether the user can view the content.
     *
     * @param  Mind2Press\Modules\Core\Models\User $userLoggedLogged
     * @param  Mind2Press\Modules\Content\Models\Content $content
     * 
     * @return mixed
     */
    public function view(User $userLogged, Content $content)
    {
        return true;
    }

    /**
     * Determine whether the user can create contents.
     *
     * @param  Mind2Press\Modules\Core\Models\User $userLoggedLogged
     * @param  Mind2Press\Modules\Content\Models\Content $content
     * 
     * @return mixed
     */
    public function create(User $userLogged)
    {
        if ($userLogged->can('edit_contents')) {
            return true;
        }
        
        if ($this->contentIsPage($content) && $userLogged->can('edit_pages')) {
            return true;
        }
        
        if ($this->contentIsPost($content) && $userLogged->can('edit_posts')) {
            return true;
        }
        
        return false;
    }

    /**
     * Determine whether the user can update the content.
     *
     * @param  Mind2Press\Modules\Core\Models\User $userLoggedLogged
     * @param  Mind2Press\Modules\Content\Models\Content $content
     * 
     * @return mixed
     * 
     * @see canEditMyNotPublishedContent
     * @see canEditOthersNotPublishedContent
     * @see canEditMyPublishedContent
     * @see canEditOthersPublishedContent
     */
    public function update(User $userLogged, Content $content)
    {
        // Conteúdo pertence ao usuário logado
        if ($this->contentIsMine($content)) {
            if ($this->contentIsPublished($content)) {
                return $this->canEditMyPublishedContent($userLogged, $content);
            }
            
            return $this->canEditMyNotPublishedContent($userLogged, $content);
        }
        
        // Conteúdo não pertence ao usuário logado
        if ($this->contentIsPublished($content)) {
            return $this->canEditOthersPublishedContent($userLogged, $content);
        }
        
        return $this->canEditOthersNotPublishedContent($userLogged, $content);
    }

    /**
     * Determine whether the user can delete the content.
     *
     * @param  Mind2Press\User $userLogged
     * @param  Mind2Press\Content $content
     * 
     * @return mixed
     * 
     * @see canDeleteMyNotPublishedContent
     * @see canDeleteOthersNotPublishedContent
     * @see canDeleteMyPublishedContent
     * @see canDeleteOthersPublishedContent
     */
    public function delete(User $userLogged, Content $content)
    {
        // Conteúdo pertence ao usuário logado
        if ($this->contentIsMine($content)) {
            if ($this->contentIsPublished($content)) {
                return $this->canDeleteMyPublishedContent($userLogged, $content);
            }
            
            return $this->canDeleteMyNotPublishedContent($userLogged, $content);
        }
        
        // Conteúdo não pertence ao usuário logado
        if ($this->contentIsPublished($content)) {
            return $this->canDeleteOthersPublishedContent($userLogged, $content);
        }
        
        return $this->canDeleteOthersNotPublishedContent($userLogged, $content);
    }
    
    /**
     * Determina se o usuário pode definir o author de um conteúdo.
     * 
     * @param User $userLogged
     * @param Content $content uma instância com pelo menos o atributo content_type_id
     * 
     * @return boolean
     */
    public function assignAuthor(User $userLogged, Content $content)
    {
       if ($userLogged->id == $this->getContentAuthorId($content)) {
           return true;
       }
       
       if ($this->contentIsPublished($content)) {
           return $this->canEditOthersPublishedContent($userLogged, $content);
       }
       
       return $this->canEditOthersNotPublishedContent($userLogged, $content);
    }
    
    /**
     * Determina se o usuário pode publicar o conteúdo.
     * 
     * @param User $userLogged
     * @param Content $content uma instância com pelo menos o atributo content_type_id
     * 
     * @return boolean
     */
    public function publishTheContent(User $userLogged, Content $content = null)
    {
        if ($userLogged->can('public_contents')) {
            return true;
        }
        
        if ($this->contentIsPage($content) && $userLogged->can('publish_pages')) {
            return true;
        }
        
        if ($this->contentIsPost($content) && $userLogged->can('publish_posts')) {
            return true;
        }
        
        return false;
    }
    
    /**
     * 
     * @param User $userLogged
     * @param Content $content
     * 
     * @return boolean
     * 
     * @see update
     */
    public function canEditMyNotPublishedContent(User $userLogged, Content $content)
    {
        if (! $this->contentIsMine($content) || $this->contentIsPublished($content)) {
            return false;
        }
        
        if ($userLogged->can('edit_contents')) {
            return true;
        }
        
        if ($this->contentIsPage($content) && $userLogged->can('edit_pages')) {
            return true;
        }
        
        if ($this->contentIsPost($content) && $userLogged->can('edit_posts')) {
            return true;
        }
        
        return false;
    }
    
    /**
     * 
     * @param User $userLogged
     * @param Content $content
     * 
     * @return boolean
     * 
     * @see update
     */
    public function canEditMyPublishedContent(User $userLogged, Content $content)
    {
        if (! $this->contentIsMine($content) || ! $this->contentIsPublished($content)) {
            return false;
        }
        
        if ($userLogged->can('edit_published_contents')) {
            return true;
        }
        
        if ($this->contentIsPage($content) && $userLogged->can('edit_published_pages')) {
            return true;
        }
        
        if ($this->contentIsPost($content) && $userLogged->can('edit_published_posts')) {
            return true;
        }
        
        return false;
    }
    
    /**
     * 
     * @param User $userLogged
     * @param Content $content
     * 
     * @return boolean
     * 
     * @see update
     */
    public function canEditOthersNotPublishedContent(User $userLogged, Content $content)
    {
        if ($this->contentIsMine($content) || $this->contentIsPublished($content)) {
            return false;
        }
        
        if ($userLogged->can('edit_others_contents')) {
            return true;
        }
        
        if ($this->contentIsPage($content) && $userLogged->can('edit_others_pages')) {
            return true;
        }
        
        if ($this->contentIsPost($content) && $userLogged->can('edit_others_posts')) {
            return true;
        }
        
        return false;
    }
    
    /**
     * 
     * @param User $userLogged
     * @param Content $content
     * 
     * @return boolean
     * 
     * @see update
     */
    public function canEditOthersPublishedContent(User $userLogged, Content $content)
    {
        if ($this->contentIsMine($content) || ! $this->contentIsPublished($content)) {
            return false;
        }
        
        if ($userLogged->can(['edit_others_contents', 'edit_published_contents'], true)) {
            return true;
        }
        
        if ($this->contentIsPage($content) && $userLogged->can(['edit_others_pages', 'edit_published_pages'], true)) {
            return true;
        }
        
        if ($this->contentIsPost($content) && $userLogged->can(['edit_others_posts', 'edit_published_posts'])) {
            return true;
        }
        
        return false;
    }
    
    /**
     * 
     * @param User $userLogged
     * @param Content $content
     * 
     * @return boolean
     * 
     * @see delete
     */
    public function canDeleteMyNotPublishedContent(User $userLogged, Content $content)
    {
        if (! $this->contentIsMine($content) || $this->contentIsPublished($content)) {
            return false;
        }
        
        if ($userLogged->can('delete_contents')) {
            return true;
        }
        
        if ($this->contentIsPage($content) && $userLogged->can('delete_pages')) {
            return true;
        }
        
        if ($this->contentIsPost($content) && $userLogged->can('delete_posts')) {
            return true;
        }
        
        return false;
    }
    
    /**
     * 
     * @param User $userLogged
     * @param Content $content
     * 
     * @return boolean
     * 
     * @see delete
     */
    public function canDeleteMyPublishedContent(User $userLogged, Content $content)
    {
        if (! $this->contentIsMine($content) || ! $this->contentIsPublished($content)) {
            return false;
        }
        
        if ($userLogged->can(['edit_published_contents', 'delete_contents'], true)) {
            return true;
        }
        
        if ($this->contentIsPage($content) && $userLogged->can(['edit_published_pages', 'delete_pages'], true)) {
            return true;
        }
        
        if ($this->contentIsPost($content) && $userLogged->can(['edit_published_posts', 'delete_posts'], true)) {
            return true;
        }
        
        return false;
    }
    
    /**
     * 
     * @param User $userLogged
     * @param Content $content
     * 
     * @return boolean
     * 
     * @see delete
     */
    public function canDeleteOthersNotPublishedContent(User $userLogged, Content $content)
    {
        if ($this->contentIsMine($content) || $this->contentIsPublished($content)) {
            return false;
        }
        
        if ($userLogged->can('delete_others_contents')) {
            return true;
        }
        
        if ($this->contentIsPage($content) && $userLogged->can('delete_others_pages')) {
            return true;
        }
        
        if ($this->contentIsPost($content) && $userLogged->can('delete_others_posts')) {
            return true;
        }
    }
    
    /**
     * 
     * @param User $userLogged
     * @param Content $content
     * 
     * @return boolean
     * 
     * @see delete
     */
    public function canDeleteOthersPublishedContent(User $userLogged, Content $content)
    {
        if ($this->contentIsMine($content) || ! $this->contentIsPublished($content)) {
            return false;
        }
        
        if ($userLogged->can(['delete_others_contents', 'edit_published_contents'], true)) {
            return true;
        }
        
        if ($this->contentIsPage($content) &&
                $userLogged->can(['delete_others_pages', 'edit_published_pages'], true)) {
            return true;
        }
        
        if ($this->contentIsPost($content) &&
                $userLogged->can(['delete_others_posts', 'edit_published_posts'], true)) {
            return true;
        }
    }
}
