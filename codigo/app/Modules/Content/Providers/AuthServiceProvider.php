<?php

namespace Mind2Press\Modules\Content\Providers;

use Illuminate\Support\Facades\Gate;
use Mind2Press\Modules\Content\Models\Content;
use Mind2Press\Modules\Content\Models\ContentMeta;
use Mind2Press\Modules\Content\Policies\ContentPolicy;
use Mind2Press\Modules\Content\Policies\ContentMetaPolicy;
use Mind2Press\Modules\Content\Repositories\ContentRepository;
use Mind2Press\Modules\Content\Repositories\ContentMetaRepository;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     * @TODO fazer bind do UserRepository para poder utilizá-lo no lugar do model e corrigir os arquivos *Policy
     */
    protected $policies = [
        Content::class => ContentPolicy::class,
        ContentRepository::class => ContentPolicy::class,
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();
    }
}
