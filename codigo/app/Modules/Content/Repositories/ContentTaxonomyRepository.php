<?php

namespace Mind2Press\Modules\Content\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;

class ContentTaxonomyRepository extends BaseRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return "Mind2Press\\Modules\\Content\\Models\\ContentTaxonomy";
    }
}
