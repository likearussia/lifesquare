<?php

namespace Mind2Press\Modules\Content\Traits;

use Auth;
use Mind2Press\Modules\Content\Models\Content;
use Mind2Press\Modules\Content\Repositories\ContentRepository;
use Mind2Press\Modules\Content\Repositories\ContentTypeRepository;

trait ContentTrait
{
    /**
     * 
     * @param Content|ContentRepository $content
     * 
     * @return void
     * 
     * @throws \InvalidArgumentException
     */
    public function isContent($content)
    {
        if (! $content instanceof Content && ! $content instanceof ContentRepository) {
            throw new \InvalidArgumentException('Parameter is not a content.');
        }
    }
    
    /**
     * 
     * @param mixed $content
     * 
     * @return bool
     * 
     * @throws \InvalidArgumentException
     */
    public function contentIsPublished($content)
    {
        $this->isContent($content);
        
        return ('published' == $content->status);
    }
    
    /**
     * 
     * @param Content $content
     * 
     * @return string
     */
    public function getContentTypeAsString($content)
    {
        $search = app(ContentTypeRepository::class)->findWhere(['id' => $content->content_type_id])->first();
        
        return $search->name;
    }
    
    /**
     * 
     * @param mixed $content
     * 
     * @return bool
     * 
     * @throws \InvalidArgumentException
     */
    public function contentIsPage($content)
    {
        $this->isContent($content);
        
        return ('page' == $this->getContentTypeAsString($content));
    }
    
    /**
     * 
     * @param mixed $content
     * 
     * @return bool
     * 
     * @throws \InvalidArgumentException
     */
    public function contentIsPost($content)
    {
        $this->isContent($content);
        
        return ('post' == $this->getContentTypeAsString($content));
    }
    
    /**
     * Dado uma variável representando um content, tenta retornar o ID do usuário dono.
     * 
     * @param mixed $content
     * 
     * @return int
     * 
     * @throws \InvalidArgumentException
     */
    public function getContentAuthorId($content)
    {
        $this->isContent($content);
        
        return $content->author;
    }
    
    public function contentBelongsTo($content, $user)
    {
        return ($user->id == $this->getContentAuthorId($content));
    }
    
    public function contentIsMine($content)
    {
        $userLogged = Auth::user();
        
        return $this->contentBelongsTo($content, $userLogged);
    }
}

