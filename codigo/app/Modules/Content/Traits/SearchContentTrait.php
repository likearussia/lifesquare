<?php

namespace Mind2Press\Modules\Content\Traits;

use Validator;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Builder;

trait SearchContentTrait {
    /**
     *
     * @var Request
     */
    private $request;
    
    /**
     * Adiciona critérios de busca em $query conforme passados por GET
     * 
     * @param Builder $query
     * @param Request $request
     * 
     * @return Builder $query com critérios de ordenação
     * 
     * @TODO possibilitar definir o tipo de busca (e.g. < > <=)
     */
    public function searchContent(Builder $query, Request $request)
    {
        $this->request = $request;
        
        if (!$this->request->has('s')) {
            return $query;
        }
        
        $this->validateSearchRequest();
        
        foreach ($this->request->s as $key => $value) {
            $query->where($key, 'like', "%$value%");
        }
        
        return $query;
    }
    
    /**
     * Verifica se os parâmetros necessários estão presentes na requisição.
     * 
     * @return Response
     */
    private function validateSearchRequest()
    {
        Validator::make($this->request->all(), [
            's' => 'required|array',
        ])->validate();
    }
}
