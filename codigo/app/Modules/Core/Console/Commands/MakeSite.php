<?php

namespace Mind2Press\Modules\Core\Console\Commands;

use Illuminate\Console\Command;
use Mind2Press\Modules\Core\Repositories\SiteRepository;

class MakeSite extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'm2p:make_site'
                         . ' {name : Nome}'
                         . ' {slug : Identificador único}'
                         . ' {url : URL}'
                         . ' {--description= : Descrição}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Cria um site no Mind2Press';
    
    /**
     *
     * @var SiteRepository
     */
    protected $siteRepository;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(SiteRepository $siteRepository)
    {
        parent::__construct();
        
        $this->siteRepository = $siteRepository;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $data = [
            'name' => $this->argument('name'),
            'description' => $this->option('description'),
            'url' => $this->argument('url'),
            'slug' => $this->argument('slug'),
        ];
        
        try {
            $return = $this->siteRepository->create($data);
            if ($return) {
                $this->info('Feito');
            } else {
                $this->error('Falha. Retorno: '.$return);
            }
        } catch (\Exception $e) {
            $this->error('Erro. Detalhes: '.$e->getMessage());
        }
    }
}
