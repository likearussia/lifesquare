<?php

namespace Mind2Press\Modules\Core\Exceptions;

use RuntimeException;

class InvalidParameterException extends RuntimeException { }

