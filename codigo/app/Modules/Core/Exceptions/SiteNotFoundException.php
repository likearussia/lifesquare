<?php

namespace Mind2Press\Modules\Core\Exceptions;

use RuntimeException;

class SiteNotFoundException extends RuntimeException { }

