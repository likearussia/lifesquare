<?php

namespace Mind2Press\Modules\Core\Http\Controllers;

use Validator;
use Illuminate\Http\Request;
use Mind2Press\Modules\Core\Models\Option;
use Illuminate\Auth\Access\AuthorizationException;
use Mind2Press\Modules\Core\Traits\AuthorizesSitesTrait;
use Mind2Press\Modules\Core\Repositories\OptionRepository;

class OptionController extends Controller
{
    use AuthorizesSitesTrait;
    
    /**
     *
     * @var OptionRepository
     */
    protected $optionRepository;

    public function __construct(OptionRepository $optionRepository) {
        $this->optionRepository = $optionRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->authorize('search', Option::class);
        
        $this->optionRepository->pushCriteria(app('Prettus\Repository\Criteria\RequestCriteria'));
        $options = $this->optionRepository->paginate($this->optionRepository->getResultsPerPage());

        return response()->json([
            'result' => 'success',
            'data' => $options,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->authorize('create', Option::class);
        
        $data = $this->getPreparedData($request);
        
        $validation = Validator::make($data, [
            'key'       => 'required|min:3|max:255|alpha_dash|unique:m2p_options,key',
            'value'     => 'required|json',
            'is_public' => 'sometimes|boolean',
            'site_id'   => 'required|exists:m2p_sites,id',
        ]);
        
        if ($validation->fails()) {
            return response()->json([
                'result' => 'error',
                'message' => ['validation' => $validation->errors()]
            ], 200);
        }
        
        $option = $this->optionRepository->create($data);
        
        $response = [
            'message' => 'Opção criada.',
            'data'    => $option->toArray(),
        ];

        return response()->json($response);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $option
     *
     * @return \Illuminate\Http\Response
     */
    public function show(Option $option)
    {
        if (! $option->is_public) {
            $this->authorize('view', $option);
        }
        
        $optionFound = $this->optionRepository->find($option->id);

        return response()->json([
            'data' => $optionFound->toArray(),
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $option
     *
     * @return \Illuminate\Http\Response
     */
    public function edit(Option $option)
    {
        $this->authorize('update', $option);
        
        $optionFound = $this->optionRepository->find($option->id);

        $response = [
            'result'  => 'success',
            'data'    => $optionFound->toArray(),
        ];

        return response()->json($response);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request $request
     * @param  string            $option
     *
     * @return Response
     */
    public function update(Request $request, Option $option)
    {
        $this->authorize('update', $option);
        
        $data = $this->getPreparedData($request);
        
        $validation = Validator::make($data, [
            'key'       => 'sometimes|min:3|max:255|alpha_dash|unique:m2p_options,key,'.$option->id,
            'value'     => 'sometimes|json',
            'is_public' => 'sometimes|boolean',
            'site_id'   => 'sometimes|exists:m2p_sites,id',
        ]);
        if ($validation->fails()) {
            return response()->json([
                'result' => 'error',
                'message' => ['validation' => $validation->errors()]
            ], 200);
        }
        
        $optionUpdated = $this->optionRepository->update($data, $option->id);

        $response = [
            'result'  => 'success',
            'message' => 'Opção atualizada.',
            'data'    => $optionUpdated->toArray(),
        ];

        return response()->json($response);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $option
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy(Option $option)
    {
        $this->authorize('delete', $option);
        
        $deleted = $this->optionRepository->delete($option->id);

        return response()->json([
                'result' => 'success',
                'message' => 'Opção deletada.',
                'deleted' => $deleted,
            ]);
    }
}
