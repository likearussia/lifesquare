<?php

namespace Mind2Press\Modules\Core\Http\Controllers;

use Validator;
use Illuminate\Http\Request;
use Mind2Press\Modules\Core\Models\Site;
use Illuminate\Auth\Access\AuthorizationException;
use Mind2Press\Modules\Core\Repositories\SiteRepository;
use Mind2Press\Modules\Core\Repositories\OptionRepository;

class SiteController extends Controller
{
    /**
     *
     * @var SiteRepository
     */
    protected $siteRepository;
    
    /**
     *
     * @var OptionRepository
     */
    protected $optionRepository;

    public function __construct(SiteRepository $siteRepository,
                                OptionRepository $optionRepository) {
        $this->siteRepository = $siteRepository;
        $this->optionRepository = $optionRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->authorize('search', Site::class);
        
        $this->siteRepository->pushCriteria(app('Prettus\Repository\Criteria\RequestCriteria'));
        $sites = $this->siteRepository->paginate($this->optionRepository->getResultsPerPage());

        return response()->json([
            'result' => 'success',
            'data' => $sites,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->authorize('create', Site::class);
        
        $validation = Validator::make($request->all(), [
            'name'   => 'required|min:3|max:100',
            'title'  => 'required|min:3',
            'url'    => 'required|url',
            'slug'   => 'required|min:3|max:20|alpha_dash|unique:m2p_sites,slug',
        ]);
        if ($validation->fails()) {
            return response()->json([
                'result' => 'error',
                'message' => ['validation' => $validation->errors()]
            ], 200);
        }
        
        $site = $this->siteRepository->create($request->all());
        
        $response = [
            'message' => 'Site criado.',
            'data'    => $site->toArray(),
        ];

        return response()->json($response);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $site
     *
     * @return \Illuminate\Http\Response
     */
    public function show(Site $site)
    {
        // Todas as requisições são autorizadas
        //$this->authorize('view', $site);
        
        $siteFound = $this->siteRepository->find($site->id);

        return response()->json([
            'data' => $siteFound->toArray(),
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $site
     *
     * @return \Illuminate\Http\Response
     */
    public function edit(Site $site)
    {
        $this->authorize('update', $site);
        
        $siteFound = $this->siteRepository->find($site->id);

        $response = [
            'result'  => 'success',
            'data'    => $siteFound->toArray(),
        ];

        return response()->json($response);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request $request
     * @param  string            $site
     *
     * @return Response
     */
    public function update(Request $request, Site $site)
    {
        $this->authorize('update', $site);
        
        $validation = Validator::make($request->all(), [
            'name'   => 'sometimes|min:3|max:100',
            'title'  => 'sometimes|min:3',
            'url'    => 'sometimes|url',
            'slug'   => 'sometimes|min:3|max:20|alpha_dash|unique:m2p_sites,slug,'.$site->id,
        ]);
        if ($validation->fails()) {
            return response()->json([
                'result' => 'error',
                'message' => ['validation' => $validation->errors()]
            ], 200);
        }
        
        $siteUpdated = $this->siteRepository->update($request->all(), $site->id);

        $response = [
            'result'  => 'success',
            'message' => 'Site atualizado.',
            'data'    => $siteUpdated->toArray(),
        ];

        return response()->json($response);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $site
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy(Site $site)
    {
        $this->authorize('delete', $site);
        
        $deleted = $this->siteRepository->delete($site->id);

        return response()->json([
                'result' => 'success',
                'message' => 'Site deletado.',
                'deleted' => $deleted,
            ]);
    }
}
