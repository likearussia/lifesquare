<?php

namespace Mind2Press\Modules\Core\Http\Controllers;

use Auth;
use Validator;
use Illuminate\Http\Request;
use Mind2Press\Modules\Core\Models\User;
use Illuminate\Auth\Access\AuthorizationException;
use Mind2Press\Modules\Core\Traits\AuthorizesSitesTrait;
use Mind2Press\Modules\Core\Repositories\OptionRepository;
use Mind2Press\Modules\Core\Repositories\UserRepository;

class UserController extends Controller
{
    use AuthorizesSitesTrait;
    
    /**
     *
     * @var UserRepository
     */
    protected $userRepository;
    
    /**
     *
     * @var OptionRepository
     */
    protected $optionRepository;

    public function __construct(UserRepository $userRepository,
                                OptionRepository $optionRepository) {
        $this->userRepository = $userRepository;
        $this->optionRepository = $optionRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->authorize('search', User::class);
        
        $this->userRepository->pushCriteria(app('Prettus\Repository\Criteria\RequestCriteria'));
        $users = $this->userRepository->paginate($this->optionRepository->getResultsPerPage());

        return response()->json([
            'result' => 'success',
            'data' => $users,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  UserCreateRequest $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->authorize('create', User::class);
        
        $data = $this->getPreparedData($request);
        
        $validation = Validator::make($data, [
            'site_id'    => 'required|exists:m2p_sites,id',
            'first_name' => 'required|min:5|max:100',
            'last_name'  => 'sometimes|max:100',
            'login'      => 'required|min:3|max:50|alpha_dash|unique:m2p_users,login',
            'email'      => 'required|email|max:100',
            'password'   => 'required|min:5',  
        ]);
        if ($validation->fails()) {
            return response()->json([
                'result' => 'error',
                'message' => ['validation' => $validation->errors()]
            ], 200);
        }
        
        $user = $this->userRepository->create($data);
        
        $response = [
            'message' => 'Usuário criado.',
            'data'    => $user->toArray(),
        ];

        return response()->json($response);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $user
     *
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        $this->authorize('view', $user);
        
        $userFound = $this->userRepository->find($user->id);

        return response()->json([
            'result' => 'success',
            'data' => $userFound->toArray(),
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $user
     *
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        $this->authorize('update', $user);
        
        $userFound = $this->userRepository->find($user->id);

        $response = [
            'result'  => 'success',
            'data'    => $userFound->toArray(),
        ];

        return response()->json($response);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  UserUpdateRequest $request
     * @param  string            $user
     *
     * @return Response
     */
    public function update(Request $request, User $user)
    {
        $this->authorize('update', $user);
        
        $data = $this->getPreparedData($request);
        
        $validation = Validator::make($data, [
            'site_id'    => 'sometimes|exists:m2p_sites,id',
            'first_name' => 'sometimes|min:5|max:100',
            'last_name'  => 'sometimes|max:100',
            'login'      => 'sometimes|min:3|max:50|alpha_dash|unique:m2p_users,login,'.$user->id,
            'email'      => 'sometimes|email|max:100',
            'password'   => 'sometimes|min:5',
        ]);
        if ($validation->fails()) {
            return response()->json([
                'result' => 'error',
                'message' => ['validation' => $validation->errors()]
            ], 200);
        }
        
        $userUpdated = $this->userRepository->update($data, $user->id);

        $response = [
            'result'  => 'success',
            'message' => 'Usuário atualizado.',
            'data'    => $userUpdated->toArray(),
        ];

        return response()->json($response);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $user
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        $this->authorize('delete', $user);
        
        $deleted = $this->userRepository->delete($user->id);

        return response()->json([
                'result' => 'success',
                'message' => 'Usuário deletado.',
                'deleted' => $deleted,
            ]);
    }
    
    /**
     * Retorna json dizendo que o usuário tem a(s) permissão(ões) necessária(s).
     * 
     * @request $permissions pode ser string ou array de strings
     * @request $require_all boolean
     * 
     * @param Request $request ver @request
     * @return Response
     */
    public function can(Request $request)
    {
        $validation = Validator::make($request->all(), [
            'permissions' => 'required',
            'require_all' => 'sometimes|in:true,false',
        ]);
        if ($validation->fails()) {
            return response()->json([
                'result' => 'error',
                'message' => ['validation' => $validation->errors()]
            ], 200);
        }
        
        $permissions = (is_array($request->permissions)) ? $request->permissions : [$request->permissions];
        $requireAll = ($request->require_all == 'true') ? true  : false;
        
        return response()->json([
            'result' => 'success',
            'data' => [
                'can' => Auth::user()->can($permissions, $requireAll),
            ],
        ]);
    }
}
