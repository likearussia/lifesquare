<?php

/*
|--------------------------------------------------------------------------
| Routes
|--------------------------------------------------------------------------
|
| Here is where you can register routes for your Content module. These
| routes are loaded by the CoreServiceProvider within a group which
| is assigned the "api" middleware group.
|
*/

Route::group(['prefix' => 'v0', 'middleware' => 'm2p-cookie'], function() {
    Route::group(['namespace' => 'Auth', 'prefix' => 'auth'], function() {
        Route::post('/', 'AuthController@authenticate');
    });

    Route::get('/site/{site}', [
        'uses' => 'SiteController@show', 'as' => 'site.show',
    ]);
    
    Route::get('/option/{option}', [
        'uses' => 'OptionController@show', 'as' => 'option.show',
    ]);
    
    /**
     * Rotas autenticadas
     */
    Route::group(['middleware' => 'auth'], function() {
        Route::post('/user/can', 'UserController@can');
        Route::resource('user', 'UserController');
            
        Route::resource('site', 'SiteController', ['except' => 'show']);
        
        Route::resource('option', 'OptionController', ['except' => 'show']);
    });
});
