<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateM2pSitesTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('m2p_sites', function(Blueprint $table) {            
            $table->increments('id');
            $table->string('name', 100);
            $table->text('title');
            $table->text('description')->nullable();
            $table->string('url', 255);
            $table->char('slug', 20)->unique();
            $table->boolean('is_default')->default(false);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::drop('m2p_sites');
    }

}
