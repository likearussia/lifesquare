<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateM2pOptionsTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('m2p_options', function(Blueprint $table) {
            $table->increments('id');
            $table->string('key')->unique();
            $table->json('value');
            $table->boolean('is_public')->default(0);
            $table->integer('site_id')->unsigned()->nullable();
            $table->foreign('site_id')->references('id')->on('m2p_sites')->onUpdate('NO ACTION')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::drop('m2p_options');
    }

}
