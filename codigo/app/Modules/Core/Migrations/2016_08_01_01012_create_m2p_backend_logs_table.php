<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateM2pBackendLogsTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('m2p_backend_logs', function(Blueprint $table) {
            $table->increments('id');
            $table->integer('site_id')->unsigned();
            $table->foreign('site_id')->references('id')->on('m2p_sites')->onUpdate('NO ACTION')->onDelete('cascade');
            $table->string('category', 50);
            $table->string('level', 50)->index();
            $table->text('message');
            $table->mediumText('details')->nullable();
            $table->timestamp('created_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::drop('m2p_backend_logs');
    }

}
