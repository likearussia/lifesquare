<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateM2pBackendRequestLogsTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('m2p_backend_request_logs', function(Blueprint $table) {
            $table->increments('id');
            $table->integer('status_code');
            $table->string('content_type', 10)->nullable();
            $table->string('url', 2000);
            $table->string('referer', 2000)->nullable();
            $table->timestamp('created_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::drop('m2p_backend_request_logs');
    }

}
