<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateM2pUserGroupsTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        // Desabilitado porque não é usado ainda
        return true;
        
        Schema::create('m2p_user_groups', function(Blueprint $table) {
            $table->increments('id');
            $table->integer('site_id')->unsigned();
            $table->foreign('site_id')->references('id')->on('m2p_sites')->onUpdate('NO ACTION')->onDelete('cascade');
            $table->string('name', 100);
            $table->text('description')->nullable();
            $table->json('permissions');
            $table->boolean('is_default')->default(0);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        // Desabilitado porque não é usado ainda
        return true;
        
        Schema::drop('m2p_user_groups');
    }

}
