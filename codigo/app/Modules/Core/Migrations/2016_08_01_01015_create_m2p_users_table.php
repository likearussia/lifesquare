<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateM2pUsersTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('m2p_users', function(Blueprint $table) {
            $table->increments('id');
            $table->integer('site_id')->unsigned();
            $table->foreign('site_id')->references('id')->on('m2p_sites')->onUpdate('NO ACTION')->onDelete('cascade');
            $table->string('first_name', 100);
            $table->string('last_name', 100)->nullable();
            $table->string('login', 100)->index();
            $table->string('email', 200)->index();
            $table->string('password', 255);
//            $table->integer('user_group_id')->unsigned();
//            $table->foreign('user_group_id')->references('id')->on('m2p_user_groups')->onUpdate('cascade')->onDelete('cascade');
            $table->string('activation_code', 255)->nullable();
            $table->string('remember_token', 255)->nullable();
            $table->string('reset_password_code', 255)->nullable();
            $table->timestamps();
            $table->dateTime('activated_at')->nullable();
            $table->dateTime('last_login_at')->nullable();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::drop('m2p_users');
    }

}
