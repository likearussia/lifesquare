<?php

namespace Mind2Press\Modules\Core\Models;

use Illuminate\Database\Eloquent\Model;

class BackendLog extends Model
{
    protected $table = 'm2p_backend_logs';
    
    protected $fillable = ['category', 'level', 'message', 'details'];
}
