<?php

namespace Mind2Press\Modules\Core\Models;

use Illuminate\Database\Eloquent\Model;

class BackendRequestLog extends Model
{
    protected $table = 'm2p_backend_request_logs';
    
    protected $fillable = ['status_code', 'content_type', 'url', 'referer'];
}
