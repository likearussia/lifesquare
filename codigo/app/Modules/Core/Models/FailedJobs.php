<?php

namespace Mind2Press\Modules\Core\Models;

use Illuminate\Database\Eloquent\Model;

class FailedJobs extends Model
{
    protected $table = 'failed_jobs';
    
    protected $fillable = ['connection', 'queue', 'payload', 'failed_at'];
}
