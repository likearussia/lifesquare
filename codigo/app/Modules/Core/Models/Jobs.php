<?php

namespace Mind2Press\Modules\Core\Models;

use Illuminate\Database\Eloquent\Model;

class Jobs extends Model
{
    protected $table = 'jobs';
    
    protected $fillable = ['queue', 'payload', 'attempts', 'reserved', 'reserved_at', 'available_at', 'created_at'];
}
