<?php

namespace Mind2Press\Modules\Core\Models;

use Illuminate\Database\Eloquent\Model;

class Option extends Model
{
    protected $table = 'm2p_options';
    
    protected $fillable = ['key', 'value', 'autoload', 'is_public', 'site_id'];
    
    /**
     * Get the route key for the model.
     * 
     * @return string
     */
    public function getRouteKeyName()
    {
        return 'key';
    }
}
