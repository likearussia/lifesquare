<?php

namespace Mind2Press\Modules\Core\Models;

use Illuminate\Database\Eloquent\Model;

class UserGroup extends Model
{   
    protected $table = 'm2p_user_groups';
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'description', 'permissions', 'is_default'];
}
