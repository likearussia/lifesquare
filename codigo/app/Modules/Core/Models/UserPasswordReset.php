<?php

namespace Mind2Press\Modules\Core\Models;

use Illuminate\Database\Eloquent\Model;

class UserPasswordReset extends Model
{
    protected $table = 'm2p_user_password_resets';
    
    protected $fillable = ['user_id', 'email', 'token'];
}
