<?php

namespace Mind2Press\Modules\Core\Policies;

use Mind2Press\Modules\Core\Models\User;
use Mind2Press\Modules\Core\Models\Option;
use Illuminate\Auth\Access\HandlesAuthorization;
use Mind2Press\Modules\Core\Repositories\OptionRepository;

class OptionPolicy
{
    use HandlesAuthorization;
    
    /**
     * Determina se o usuário pode procurar opções.
     *
     * @param  Mind2Press\Modules\Core\Models\User $userLogged
     * @return boolean
     */
    public function search(User $userLogged)
    {
        if ($userLogged->can(['manage_options'])) {
            return true;
        }
        
        return false;
    }
    
    /**
     * Determina se o usuário pode ver a opção.
     *
     * @param  Mind2Press\Modules\Core\Models\User $userLogged
     * @param  mixed $option
     * @return boolean
     */
    public function view(User $userLogged, $option)
    {
        if ($userLogged->can(['manage_options'])) {
            return true;
        }
        
        return false;
    }

    /**
     * Determina se o usuário pode criar a opção.
     *
     * @param  Mind2Press\Modules\Core\Models\User $userLogged
     * @return boolean
     */
    public function create(User $userLogged)
    {
        if ($userLogged->can(['manage_options'])) {
            return true;
        }
        
        return false;
    }

    /**
     * Determina se o usuário pode fazer a atualização da opção.
     *
     * @param  Mind2Press\Modules\Core\Models\User $userLogged
     * @param  mixed $option
     * @return boolean
     * @see updateChangeRole
     */
    public function update(User $userLogged, $option)
    {
        if ($userLogged->can(['manage_options'])) {
            return true;
        }
        
        return false;
    }

    /**
     * Determina se o usuário pode deletar a opção.
     *
     * @param  Mind2Press\Modules\Core\Models\User $userLogged
     * @param  mixed $option
     * @return bollean
     */
    public function delete(User $userLogged, $option)
    {
        if ($userLogged->can(['manage_options'])) {
            return true;
        }
        
        return false;
    }
    
    /**
     * Dado uma variável representando uma opção, tenta retornar o ID que a representa.
     * 
     * @param mixed $option
     * @return int
     */
    protected function getId($option)
    {
        if ($option instanceof Option) {
            return $option->id;
        }
        
        return (int) $option;
    }
}
