<?php

namespace Mind2Press\Modules\Core\Policies;

use Mind2Press\Modules\Core\Models\User;
use Mind2Press\Modules\Core\Models\Site;
use Illuminate\Auth\Access\HandlesAuthorization;
use Mind2Press\Modules\Core\Repositories\SiteRepository;

class SitePolicy
{
    use HandlesAuthorization;
    
    /**
     * Determina se o usuário pode procurar sites.
     *
     * @param  Mind2Press\Modules\Core\Models\User $userLogged
     * @return boolean
     */
    public function search(User $userLogged)
    {
        if ($userLogged->can(['list_sites', 'edit_sites'])) {
            return true;
        }
        
        return false;
    }
    
    /**
     * Determina se o usuário pode ver o site.
     *
     * @param  Mind2Press\Modules\Core\Models\User $userLogged
     * @param  mixed $site
     * @return boolean
     */
    public function view(User $userLogged, $site)
    {
        return true;
    }

    /**
     * Determina se o usuário pode criar um site.
     *
     * @param  Mind2Press\Modules\Core\Models\User $userLogged
     * @return boolean
     */
    public function create(User $userLogged)
    {
        if ($userLogged->can(['create_sites'])) {
            return true;
        }
        
        return false;
    }

    /**
     * Determina se o usuário pode fazer uma atualização simples do site.
     *
     * @param  Mind2Press\Modules\Core\Models\User $userLogged
     * @param  mixed $site
     * @return boolean
     * @see updateChangeRole
     */
    public function update(User $userLogged, $site)
    {
        if ($userLogged->can(['edit_sites'])) {
            return true;
        }
        
        return false;
    }
    
    /**
     * Determina se o usuário pode atribuir sites para outros recursos (e.g. usuários, opções).
     * 
     * @param  Mind2Press\Modules\Core\Models\User $userLogged
     * @return boolean
     */
    public function assignSite(User $userLogged)
    {
        if ($userLogged->can(['assing_sites'])) {
            return true;
        }
        
        return false;
    }

    /**
     * Determina se o usuário pode deletar o site.
     *
     * @param  Mind2Press\Modules\Core\Models\User $userLogged
     * @param  mixed $site
     * @return bollean
     */
    public function delete(User $userLogged, $site)
    {
        if ($userLogged->can(['delete_sites'])) {
            return true;
        }
        
        return false;
    }
    
    /**
     * Dado uma variável representando um site, tenta retornar o ID que o representa.
     * 
     * @param mixed $site
     * @return int
     */
    protected function getId($site)
    {
        if ($site instanceof Site) {
            return $site->id;
        }
        
        return (int) $site;
    }
}
