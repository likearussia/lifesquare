<?php

namespace Mind2Press\Modules\Core\Policies;

use Mind2Press\Modules\Core\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;
use Mind2Press\Modules\Core\Repositories\UserRepository;

class UserPolicy
{
    use HandlesAuthorization;
    
    /**
     * Determina se o usuário pode procurar usuários.
     *
     * @param  Mind2Press\Modules\Core\Models\User $userLogged
     * @return boolean
     */
    public function search(User $userLogged)
    {
        if ($userLogged->can(['list_users', 'edit_users'])) {
            return true;
        }
        
        return false;
    }
    
    /**
     * Determina se o usuário pode ver o usuário.
     *
     * @param  Mind2Press\Modules\Core\Models\User $userLogged
     * @param  mixed $user
     * @return boolean
     */
    public function view(User $userLogged, $user)
    {
        if ($userLogged->id == $this->getId($user)) {
            return true;
        }
        
        if ($userLogged->can(['list_users', 'edit_users'])) {
            return true;
        }
        
        return false;
    }

    /**
     * Determina se o usuário pode criar um usuário.
     *
     * @param  Mind2Press\Modules\Core\Models\User $userLogged
     * @return boolean
     */
    public function create(User $userLogged)
    {
        if ($userLogged->can(['create_users'])) {
            return true;
        }
        
        return false;
    }

    /**
     * Determina se o usuário pode fazer uma atualização simples do usuário.
     *
     * @param  Mind2Press\Modules\Core\Models\User $userLogged
     * @param  mixed $user
     * @return boolean
     * @see updateChangeRole
     */
    public function update(User $userLogged, $user)
    {
        if ($userLogged->id == $this->getId($user)) {
            return true;
        }
        
        if ($userLogged->can(['edit_users'])) {
            return true;
        }
        
        return false;
    }
    
    /**
     * Determina se o usuário pode atualizar roles de usuários
     * 
     * @param  Mind2Press\Modules\Core\Models\User $userLogged
     * @return boolean
     */
    public function changeRole(User $userLogged)
    {
        if ($userLogged->can(['edit_users'])) {
            return true;
        }
        
        return false;
    }

    /**
     * Determina se o usuário pode deletar o usuário.
     *
     * @param  Mind2Press\Modules\Core\Models\User $userLogged
     * @param  mixed $user
     * @return bollean
     */
    public function delete(User $userLogged, $user)
    {
        if ($userLogged->id == $this->getId($user)) {
            return true;
        }
        
        if ($userLogged->can(['delete_users'])) {
            return true;
        }
        
        return false;
    }
    
    /**
     * Dado uma variável representando um usuário, tenta retornar o ID que o representa.
     * 
     * @param mixed $user
     * @return int
     */
    protected function getId($user)
    {
        if ($user instanceof User) {
            return $user->id;
        }
        
        return (int) $user;
    }
}
