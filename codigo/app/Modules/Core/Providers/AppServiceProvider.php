<?php

namespace Mind2Press\Modules\Core\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * All commands registered by this module
     * 
     * @var array
     */
    protected $commandsList = [
        'Mind2Press\Modules\Core\Console\Commands\MakeSite',
    ];
    
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadMigrationsFrom(__DIR__.'/../Migrations');
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->commands($this->commandsList);
    }
}
