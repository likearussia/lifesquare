<?php

namespace Mind2Press\Modules\Core\Providers;

use Illuminate\Support\Facades\Gate;
use Mind2Press\Modules\Core\Models\Site;
use Mind2Press\Modules\Core\Models\User;
use Mind2Press\Modules\Core\Models\Option;
use Mind2Press\Modules\Core\Repositories\SiteRepository;
use Mind2Press\Modules\Core\Repositories\UserRepository;
use Mind2Press\Modules\Core\Repositories\OptionRepository;
use Mind2Press\Modules\Core\Policies\SitePolicy;
use Mind2Press\Modules\Core\Policies\UserPolicy;
use Mind2Press\Modules\Core\Policies\OptionPolicy;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     * @TODO fazer bind do UserRepository para poder utilizá-lo no lugar do model e corrigir os arquivos *Policy
     */
    protected $policies = [
        Site::class => SitePolicy::class,
        SiteRepository::class => SitePolicy::class,
        
        User::class => UserPolicy::class,
        UserRepository::class => UserPolicy::class,
        
        Option::class => OptionPolicy::class,
        OptionRepository::class => OptionPolicy::class,
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();
    }
}
