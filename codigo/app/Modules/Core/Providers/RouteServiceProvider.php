<?php

namespace Mind2Press\Modules\Core\Providers;

use Illuminate\Support\Facades\Route;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * This namespace is applied to your controller routes.
     *
     * In addition, it is set as the URL generator's root namespace.
     *
     * @var string
     */
    protected $namespace = 'Mind2Press\Modules\Core\Http\Controllers';
    
    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();
        
        //Route::model('option', 'Mind2Press\Modules\Core\Repositories\OptionRepository');
    }
    
    /**
     * Define the routes for the module.
     *
     * @return void
     */
    public function map()
    {
        Route::group([
            'namespace' => $this->namespace,
            'prefix' => config('m2p.api.prefix'),
        ], function ($router) {
            require __DIR__.'/../Http/routes.php';
        });
    }
}
