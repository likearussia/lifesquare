<?php

namespace Mind2Press\Modules\Core\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;

class BackendRequestLogRepository extends BaseRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return "Mind2Press\\Modules\\Core\\Models\\BackendRequestLog";
    }
}