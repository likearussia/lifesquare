<?php

namespace Mind2Press\Modules\Core\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;

class OptionRepository extends BaseRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return "Mind2Press\\Modules\\Core\\Models\\Option";
    }
    
    /**
     * Retorna o número de resultados que devem ser retornados por vez.
     * 
     * @param type $contentType
     * 
     * @return int
     * 
     * @TODO implementar resultados diferentes para tipos diferentes de conteúdo
     */
    public function getResultsPerPage($contentType = null)
    {
        $result = $this->findWhere(['key' => 'm2p_public'],
                                    ['value->results_per_page as results_per_page'])
                        ->first();
        return $result->results_per_page;
    }
}