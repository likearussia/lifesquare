<?php

namespace Mind2Press\Modules\Core\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Mind2Press\Modules\Core\Exceptions\SiteNotFoundException;

class SiteRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = ['id', 'name', 'title', 'url', 'slug'];
    
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return "Mind2Press\\Modules\\Core\\Models\\Site";
    }
    
    /**
     * Retorna o site padrão
     * 
     * @return SiteRepository
     * @throws SiteNotFoundException
     */
    public function getDefault()
    {
        $search = $this->findByField('is_default', true);
        
        if ($search->isEmpty()) {
            throw new SiteNotFoundException('The default site has not found.');
        }
        
        return $search->get(0);
    }
}
