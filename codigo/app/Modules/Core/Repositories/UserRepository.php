<?php

namespace Mind2Press\Modules\Core\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;

class UserRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = ['id', 'first_name', 'last_name', 'login', 'email'];
    
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return "Mind2Press\\Modules\\Core\\Models\\User";
    }
    
    /**
     * Cria um usuário
     * 
     * {@inheritDoc}
     */
    public function create(array $attributes)
    {
        if (array_key_exists('password', $attributes)) {
            $attributes['password'] = bcrypt($attributes['password']);
        }
        
        return parent::create($attributes);
    }
    
    /**
     * Atualiza um usuário
     * 
     * {@inheritDoc}
     */
    public function update(array $attributes, $id)
    {
        if (array_key_exists('password', $attributes)) {
            $attributes['password'] = bcrypt($attributes['password']);
        }
        
        return parent::update($attributes, $id);
    }
}