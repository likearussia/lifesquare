<?php

namespace Mind2Press\Modules\Core\Traits;

use Mind2Press\Modules\Core\Models\Site;
use Mind2Press\Modules\Core\Repositories\SiteRepository;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

trait AuthorizesSitesTrait
{
    use AuthorizesRequests;
    
    /**
     * Analisa uma requisição a procura de uma definição de site e toma as devidas providências.
     * 
     * @param $request
     * @return array
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function getPreparedData($request)
    {
        $data = $request->all();
        
        if ($request->has('site_id')) {
            $this->authorize('assignSite', Site::class);
        } else {
            $data['site_id'] = app(SiteRepository::class)->getDefault()->id;
        }
        
        return $data;
    }
}

