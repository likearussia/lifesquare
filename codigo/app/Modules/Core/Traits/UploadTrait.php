<?php

namespace Mind2Press\Modules\Core\Traits;

trait UploadTrait {
    /**
     * Retorna, de acordo com $unit, o tamanho máximo do upload permitido no php.
     * 
     * @param $unit pode ser B, K, M e G
     * 
     * @return int
     */
    public function getMaxUploadSize($unit = 'B')
    {
        $bytes = $this->stringToBytes(ini_get('upload_max_filesize'));
        
        return $this->convertBytes($bytes, $unit);
    }
    
    /**
     * Dado uma string como "2M", retorna o número de bytes
     * 
     * @param string $size
     * 
     * @return int
     */
    protected function stringToBytes ($size) {
        switch (substr ($size, -1)) {
            case 'M': case 'm': return (int)$size * 1048576;
            case 'K': case 'k': return (int)$size * 1024;
            case 'G': case 'g': return (int)$size * 1073741824;
            default: return null;
        }
    }
    
    /**
     * Convert $bytes para a unidade especificada em $unit
     * 
     * @param int $bytes
     * @param string $unit (i.e. B, K, M, G)
     * 
     * @return int
     */
    protected function convertBytes ($bytes, $unit) {
        switch ($unit) {
            case 'B': case 'b': return "$bytes";
            case 'K': case 'k': return (int)$bytes / 1024;
            case 'M': case 'm': return (int)$bytes / 1048576;
            case 'G': case 'g': return (int)$bytes / 1073741824;
            default: return null;
        }
    }
    
    /**
     * 
     * @param int $bytes
     * @param int $precision
     * 
     * @return string
     */
    protected function formatBytes($bytes, $precision = 2) {
        $units = array('B', 'KB', 'MB', 'GB', 'TB');

        $bytes = max($bytes, 0);
        $pow = floor(($bytes ? log($bytes) : 0) / log(1024));
        $pow = min($pow, count($units) - 1);

        // Uncomment one of the following alternatives
        // $bytes /= pow(1024, $pow);
        // $bytes /= (1 << (10 * $pow));

        return round($bytes, $precision) . ' ' . $units[$pow];
    }
}