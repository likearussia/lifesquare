<?php

namespace Mind2Press\Providers\Table;

use DB;
use Validator;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Builder;
use Mind2Press\Modules\Content\Models\Content;

/**
 * Contém a lógica para ser utilizada em controllers para interpretar o que foi gerado pelo TableServiceProvider
 */
trait ContentTableSortTrait {
    /**
     *
     * @var Request
     */
    private $request;
    
    /**
     *
     * @var string
     */
    private $table;
    
    /**
     * Adiciona critérios de ordenação em $query conforme definidos na view
     * 
     * @param Builder $query
     * @param Request $request
     * 
     * @return Builder $query com critérios de ordenação
     * 
     * @TODO possibilitar ordenação por qualquer relacionamento
     */
    public function sortTable(Builder $query, Request $request)
    {
        $this->request = $request;
        
        $this->table = (new Content())->table;
        
        $sortBy = $request->sort;
        $sortDirection = $request->direction;
        
        if (empty($sortBy) || empty($sortDirection)) {
            // Ordenação padrão
            return $query->orderBy($this->table . '.parent', 'ASC')->orderBy($this->table . '.id', 'DESC');
        }
        
        $this->validateRequest();
        
        return $query->orderBy($this->resolveSortTableField($sortBy), $sortDirection);
    }
    
    /**
     * Transforma o "nome fantasia" de um campo no valor que identifica a coluna para o método orderBy
     * 
     * @param string $sortField string contendo o "nome fantasia" do campo pelo qual o conteúdo será ordenado
     * 
     * @return string|\Illuminate\Database\Query\Expression
     */
    public function resolveSortTableField($sortField)
    {
        // Sort por um campo na tabela de meta informações
        if (strpos($sortField, 'meta.') === 0) {
            $metaField = str_replace('meta.', '', $sortField);
            
            // O campo json do MySQL ainda não detecta automaticamente o tipo do dado,
            // por isso aqui faço cast, caso definido na view
            if ($this->request->has('sortAs')) {
                $cast = strtoupper($this->request->sortAs);
                return DB::raw("CAST(JSON_EXTRACT(m2p_content_meta.value, '$.{$metaField}') AS {$cast})");
            }
            
            return 'm2p_content_meta.value->' . $metaField;
        }
        
        return $this->table . '.' . $sortField;
    }
    
    /**
     * Verifica se os parâmetros necessários estão presentes na requisição.
     * 
     * @return Response
     */
    private function validateRequest()
    {
        Validator::make($this->request->all(), [
            'sort'      => 'required|string',
            'direction' => 'required|in:asc,desc',
            'sortAs'    => 'sometimes|in:string,unsigned'
        ])->validate();
    }
}