<?php

namespace Mind2Press\Providers\Table;
use Illuminate\Pagination\LengthAwarePaginator;

class Table {
        /**
         * Retorna html um item de cabeçalho de tabela com parametros para ordenação, paginação, etc.
         * 
         * Exemplo de uso no template: @tableHeader('Nome', 'name'). Sendo nome a string visível ao usuário e name
         * a string que será passada como parâmetro da URL.
         * 
         * @param string $fieldName nome do campo utilizado pelo sistema
         * @param string $fieldDisplay o nome do campo que será visto pelo usuário
         * @param array $options indices possiveis: sortAs
         * 
         * @return html
         */
        public function header($fieldName, $fieldDisplay, $options = [])
        {
            $actualFilteredQueryParams = collect(request()->query())
                                         ->except(['sort', 'direction', 'sortAs']);
            
            $actualSort = request()->query('sort');
            $actualSortDirection = request()->query('direction');
            
            // Valores padrão
            $sortDirection = 'asc';
            $arrow = '';
            
            if ($actualSort == $fieldName) {
                if ($actualSortDirection == 'asc') {
                    $sortDirection = 'desc';
                    $arrow = '↓';
                } else {
                    $sortDirection = 'asc';
                    $arrow = '↑';
                }
            }
            
            $sortQueryParams = ['sort' => $fieldName, 'direction' => $sortDirection];
            
            if (isset($options['sortAs'])) {
                $sortQueryParams['sortAs'] = $options['sortAs'];
            }
            
            $queryParams = array_merge($actualFilteredQueryParams->toArray(), $sortQueryParams);

            $generatedUrl = request()->url() . '?' . http_build_query($queryParams);
            
            $html = <<<EOT
                    <th>
                        <a href="{$generatedUrl}">{$fieldDisplay} {$arrow}</a>
                    </th>
EOT;
            
            return $html;
        }
        
        /**
         * Retorna html de paginação.
         * 
         * @param LengthAwarePaginator $var conteúdo que está sendo paginado.
         * 
         * @return string html
         */
        public function pagination(LengthAwarePaginator $var)
        {
            $queryParams = request()->query();
            
            return $var->appends($queryParams)->links();
        }
}
