<?php

namespace Mind2Press\Providers\Table;

use DB;
use Validator;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Builder;
use Mind2Press\Modules\Content\Models\ContentTaxonomyTerm;

/**
 * Contém a lógica para ser utilizada em controllers para interpretar o que foi gerado pelo TableServiceProvider
 */
trait TaxonomyTermTableSortTrait {
    /**
     *
     * @var Request
     */
    private $request;
    
    /**
     *
     * @var string
     */
    private $table;
    
    /**
     * Adiciona critérios de ordenação em $query conforme definidos na view
     * 
     * @param Builder $query
     * @param Request $request
     * 
     * @return Builder $query com critérios de ordenação
     * 
     * @TODO possibilitar ordenação por qualquer relacionamento
     */
    public function sortTable(Builder $query, Request $request)
    {
        $this->request = $request;
        
        $this->table = (new ContentTaxonomyTerm())->table;
        
        $sortBy = $request->sort;
        $sortDirection = $request->direction;
        
        if (empty($sortBy) || empty($sortDirection)) {
            // Ordenação padrão
            return $query->orderBy($this->table . '.id', 'DESC');
        }
        
        $this->validateRequest();
        
        return $query->orderBy($this->resolveSortTableField($sortBy), $sortDirection);
    }
    
    /**
     * Transforma o "nome fantasia" de um campo no valor que identifica a coluna para o método orderBy
     * 
     * @param string $sortField string contendo o "nome fantasia" do campo pelo qual o conteúdo será ordenado
     * 
     * @return string|\Illuminate\Database\Query\Expression
     */
    public function resolveSortTableField($sortField)
    {
        return $this->table . '.' . $sortField;
    }
    
    /**
     * Verifica se os parâmetros necessários estão presentes na requisição.
     * 
     * @return Response
     */
    private function validateRequest()
    {
        Validator::make($this->request->all(), [
            'sort'      => 'required|string',
            'direction' => 'required|in:asc,desc',
        ])->validate();
    }
}