<?php

namespace Mind2Press\Traits;

use Validator;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Mind2Press\Modules\Content\Models\Content;
use Mind2Press\Modules\Content\Models\ContentMeta;

trait ReorderingTrait {
    /**
     *
     * @var Request
     */
    private $request;
    
    /**
     * Executa a lógica de reordenação, se necessário.
     * 
     * @param Request $request
     * 
     * @return null|Response retorna null se não for necessário executar a reordenação.
     */
    public function enableReordering(Request $request) {
        $this->request = $request;
        
        if (!$this->shouldBeProcessed()) {
            return null;
        }
        
        $this->validateReorderParameters();
        
        $reorder = $this->reorder();

        if ($reorder === true) {
            $response = response()->json(['success' => true]);
        } else {
            $response = response()->json(
                ['success' => false,
                'message' => 'Falha na ordenação!'], 500);
        }
        
        return $response->throwResponse();
    }
    
    /**
     * Define se a requisição atual deve ser processada por esta trait.
     * 
     * @return boolean
     */
    private function shouldBeProcessed()
    {
        return $this->request->has('reorder');
    }
    
    /**
     * Verifica se os parâmetros necessários estão presentes na requisição.
     * 
     * @return Response
     */
    private function validateReorderParameters()
    {
        Validator::make($this->request->all(), [
            'contentType'           => 'required|string|exists:m2p_content_types,name',
            'taxonomy'              => 'sometimes|string',
            'taxonomyTerm'          => 'sometimes|numeric|exists:m2p_content_taxonomy_terms,id',
            'reorder.contentId'     => 'required|numeric',
            'reorder.startPosition' => 'required|numeric',
            'reorder.finalPosition' => 'required|numeric',
        ])->validate();
    }
    
    /**
     * Cria a query para procurar os conteúdos que devem ser reordenados.
     * 
     * @return QueryBuilder
     */
    private function createReorderSearch()
    {
        $searchContent = Content::ofContentType($this->request->contentType);

        if ($this->request->has('taxonomy') && $this->request->has('taxonomyTerm')) {
            $contentIds = ContentMeta::taxonomies()
                                ->where('value->'.$this->request->taxonomy, $this->request->taxonomyTerm)
                                ->pluck('content_id');
            $searchContent->whereIn('id', $contentIds);
        }
        
        return $searchContent;
    }
    
    /**
     * Reordena o conteúdo.
     * 
     * @return boolean
     */
    private function reorder()
    {
        $searchContent = $this->createReorderSearch();

        $contentId = $this->request->reorder['contentId'];
        $startPosition = $this->request->reorder['startPosition'];
        $finalPosition = $this->request->reorder['finalPosition'];

        if ($startPosition > $finalPosition) {
                // O conteúdo está vindo de baixo e indo para cima
                $searchContent->where('parent', '>=', $finalPosition)->increment('parent');
        } else {
            // O conteúdo está vindo de cima e indo para baixo
            $searchContent->where('parent', '<=', $finalPosition)->decrement('parent');
        }
        
        // Normaliza os dados para não ocorrer decremento infinito
        $searchContent->where('parent', '<', '0')->update(['parent' => 0]);
        
        Content::where('id', $contentId)
                 ->update(['parent' => $finalPosition]);
        
        return true;
    }
}
