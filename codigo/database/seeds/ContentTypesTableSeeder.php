<?php

use Illuminate\Database\Seeder;
use Mind2Press\Modules\Content\Models\ContentType;

class ContentTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        ContentType::create([
            'name' => 'page',
            'description' => 'Páginas do Mind2Press',
            'slug' => 'pagina',
        ]);
        
        ContentType::create([
            'name' => 'post',
            'description' => 'Posts do Mind2Press',
            'slug' => 'post',
        ]);
    }
}
