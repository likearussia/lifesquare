<?php

use Illuminate\Database\Seeder;
use Mind2Press\Modules\Core\Models\Site;
use Mind2Press\Modules\Core\Models\User;
use Mind2Press\Modules\Core\Models\Role;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(SitesTableSeeder::class);
        $this->call(OptionsTableSeeder::class);
        $this->call(RolesAndPermissionsSeeder::class);
        
        $site = Site::find(1)->id;
        
        $user = User::create([
            'site_id' => $site,
            'first_name' => 'Super administrador',
            'login' => 'admin',
            'email' => 'admin@localhost',
            'password' => bcrypt('default'),
        ]);
        
        $superAdminRole = Role::where('name', 'super-admin')->first()->id;
        
        $user->attachRole($superAdminRole);
        
        $this->call(ContentTypesTableSeeder::class);
    }
}
