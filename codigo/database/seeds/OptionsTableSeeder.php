<?php

use Illuminate\Database\Seeder;
use Mind2Press\Modules\Core\Models\Option;

class OptionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $m2pPublicOptions = [
            'results_per_page' => 10,
        ];
        
        Option::create([
            'key'       => 'm2p_public',
            'value'     => json_encode($m2pPublicOptions),
            'is_public' => true,
        ]);
    }
}
