<?php

use Illuminate\Database\Seeder;
use Mind2Press\Modules\Core\Models\Role;
use Mind2Press\Modules\Core\Models\Permission;

class RolesAndPermissionsSeeder extends Seeder
{   
    /**
     * All the M2P permissions
     * 
     * @var array
     */
    protected $permissions = [
        'list_sites', 'create_sites', 'edit_sites', 'delete_sites', 'assing_sites',
        'list_users', 'create_users', 'edit_users', 'delete_users',
        'manage_categories', 'manage_options',
        'edit_contents', 'delete_contents',
        'edit_pages', 'delete_pages',
        'edit_posts', 'delete_posts',
        'edit_others_contents', 'edit_others_pages', 'edit_others_posts',
        'edit_published_contents', 'edit_published_pages', 'edit_published_posts',
        'delete_others_contents', 'delete_others_pages', 'delete_others_posts', 
        'public_contents', 'publish_pages', 'publish_posts',
        'moderate_comments',
        'upload_files', 'edit_files',
        'read',
    ];
    
    /**
     * Run the database seeds.
     *
     * @return void
     * @TODO terminar permissões
     */
    public function run()
    {
        // Create permissions
        for ($i = 0; $i < count($this->permissions); $i++) {
            $permission = &$this->permissions[$i];
            
            $permissionObj = new Permission();
            $permissionObj->name = $permission;
            $permissionObj->save();
        }
        
        /**
         * Create roles and assign permissions to it
         */
        $superAdmin = new Role();
        $superAdmin->name         = 'super-admin';
        $superAdmin->display_name = 'Super Admin';
        $superAdmin->description  = 'Alguém com acesso à administração de todos os sites e todos os recursos';
        $superAdmin->save();
        $perms = new Permission;
        $superAdminPerms = $perms->get();
        foreach ($superAdminPerms as $p) {
            $superAdmin->attachPermissions([$p['id']]);
        }
        
        $admin = new Role();
        $admin->name         = 'admin';
        $admin->display_name = 'Administrador';
        $admin->description  = 'Alguém com acesso a todos os recursos de administração de um único site.';
        $admin->save();
        
        $editor = new Role();
        $editor->name         = 'editor';
        $editor->display_name = 'Editor';
        $editor->description  = 'Alǵuem que pode publicar e gerir conteúdos de sua autoria ou de outros.';
        $editor->save();
        
        $author = new Role();
        $author->name         = 'author';
        $author->display_name = 'Autor';
        $author->description  = 'Alguém que pode publicar e gerir seus próprios conteúdos.';
        $author->save();
        
        $contributor = new Role();
        $contributor->name         = 'contributor';
        $contributor->display_name = 'Contributor';
        $contributor->description  = 'Alguém que pode escrever e gerir seus próprios conteúdos '
                                     . 'mas não pode publicá-los.';
        $contributor->save();
        
        $subscriber = new Role();
        $subscriber->name         = 'subscriber';
        $subscriber->display_name = 'Subscriber';
        $subscriber->description  = 'Alguém que pode apenas gerir seu próprio perfil.';
        $subscriber->save();
    }
}
