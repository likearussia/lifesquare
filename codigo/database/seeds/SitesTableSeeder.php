<?php

use Illuminate\Database\Seeder;
use Mind2Press\Modules\Core\Models\Site;

class SitesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Site::create([
            'name'  => 'Default',
            'title' => 'Default site',
            'description' => 'A default website',
            'url' => 'http://localhost:8000',
            'slug' => 'default',
            'is_default' => true,
        ]);
    }
}
