$(document).ready(function () {

    function posicion2(){
        tam1 = $('.box-flex-inner').height() / 2;
        tam2 = $('.itens-r').height() / 2;
        
        margem = tam2 - tam1;

        $('.box-flex-inner').css('margin-top', margem);
    }

    function posicionaTexto() {

        tam1 = $('.box-flex-inner').height() / 2;


        if ($(window).width() > 1200) {
            tam2 = $('.fb--flex-item__A').height() / 2;
        }else if ($(window).width() > 0) {
            tam2 = $('.fb--flex-item__A').height() / 6;
        }
        margem = tam2 - tam1;

        $('.box-flex-inner').css('margin-top', margem);
    }

    if ($(window).width() > 1200) {

        posicionaTexto();
        boxHeight = $('.fb--flex-item__A').height();

        $('.itens-r').height(boxHeight);

        $(window).resize(function () {

            boxHeight = $('.fb--flex-item__A').height();
            $('.itens-r').height(boxHeight);
            posicionaTexto();
        });

    } else if ($(window).width() > 767) {
        posicionaTexto();

        boxHeight = $('.fb--flex-item__A').height();

        newboxHeight = boxHeight / 3;

        console.log(newboxHeight);

        $('.itens-r').css('height', newboxHeight);

        /*$(window).resize(function () {
         
         boxHeight = $('.fb--flex-item__A').height();
         $('.itens-r').height(boxHeight);
         posicionaTexto();
         });*/
    }else if ($(window).width() > 0){
        posicion2();
    }



    var str = $(location).attr('href');

    if (str.match(/fitness/) || str.match(/academia-de-tenis/) || str.match(/bem-estar/)) {
        $('#footer').hide();
    }

    $('.open-search-mobile').click(function () {
        $('.input-busca').fadeToggle().focus();
    });

    $('#owl-mob').owlCarousel({
        loop: true,
        nav: false,
        center: true,
        pagination: true,
        responsive: {
            0: {
                items: 1
            },
            600: {
                items: 1
            },
            768: {
                items: 2
            }

        }
    });

    $('#owl-ranking').owlCarousel({
        loop: true,
        singleItem: true,
        responsiveClass: true,
        responsive: {
            0: {
                items: 1
            },
            600: {
                items: 1
            }


        }
    });

    $('#owl-quadras').owlCarousel({
        loop: true,
        singleItem: true,
        responsiveClass: true,
        nav: true,
        pagination: false,
        dots: false,
        navText: ["<i class='fa fa-angle-left'></i>", "<i class='fa fa-angle-right'></i>"],
        responsive: {
            0: {
                items: 1
            },
            600: {
                items: 1
            }


        }
    });

    $('#owl-loja').owlCarousel({
        loop: true,
        singleItem: true,
        responsiveClass: true,
        responsive: {
            0: {
                items: 1
            },
            600: {
                items: 1
            }


        }
    });


//    $(window).scroll(function () {
//        $('.parallax').css('top', -$(window).scrollTop() / 2);
//        var blur = $(window).scrollTop() / 50;
//        if (blur > 10)
//            blur = 10;
//        $('.parallax').css('-webkit-filter', 'blur(' + blur + 'px) grayscale(' + blur / 5 + ')');
//    });


    if (typeof fancybox !== 'undefined' && $.isFunction(fancybox)) {
        $('#gallery a').fancybox({
            overlayColor: '#060',
            overlayOpacity: 0.4,
            transitionIn: 'elastic',
            transitionOut: 'elastic',
            easingIn: 'easeInSine',
            easingOut: 'easeOutSine',
            titlePosition: 'outside',
            cyclic: true
        });
    }


    $('.search-toggle').addClass('closed');
    $('.search-toggle .search-icon').click(function (e) {
        if ($('.search-toggle').hasClass('closed')) {
            $('.search-toggle').removeClass('closed').addClass('opened');
            $('.search-toggle, .search-container').addClass('opened');
            $('#search-terms').focus();
        } else {
            $('.search-toggle').removeClass('opened').addClass('closed');
            $('.search-toggle, .search-container').removeClass('opened');
        }
    });


    var $grid = $('.grid').masonry({
        itemSelector: '.grid-item',
        percentPosition: true,
        columnWidth: '.grid-sizer'
    });
    if ($grid.length > 0) {
        $grid.imagesLoaded().progress(function () {
            $grid.masonry();
        });
    }
});





