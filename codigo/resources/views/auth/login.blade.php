<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <base href="{{ config('app.url') }}/">
        <title>TenisSquare | Log in</title>

        <link href="m2p_assets/dist/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

        <link href="m2p_assets/css/login.css" rel="stylesheet">

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>

    <body>
    <div class="ball"></div>
        <div class="container">

            <div class="wrapper">
                <form role="form" method="POST" action="{{ url('/login') }}" class="form-signin">      
                    <img src="img/logo.png" alt="Logotipo" class="logo"/>
                    <p>&nbsp;</p>

                    @include('flash::message')
                    @include('m2p.shared.validation')

                    <form role="form" method="POST" action="{{ url('/login') }}">
                        {{ csrf_field() }}
                        <input type="text" class="form-control" name="login" placeholder="Login" required autofocus value="{{ old('login') }}" />
                        <input type="password" class="form-control" name="password" placeholder="Senha" required />     		  

                        <div class="checkbox">
                            <label>
                                <input type="checkbox" name="remember"> Lembrar-me
                            </label>
                        </div>
                        <button class="btn btn-lg btn-primary btn-block btn-login"  name="Submit" value="Login" type="Submit">Entrar</button>  			
                    </form>


                    <!--<a href="{{ url('/password/reset') }}">Esqueci minha senha</a><br>-->	
            </div>

        </div>
                <div class="line-green"></div>

        <script src="m2p_assets/dist/jquery/jquery.min.js"></script>
        <script src="m2p_assets/dist/tether/js/tether.min.js"></script>
        <script src="m2p_assets/dist/bootstrap/js/bootstrap.min.js"></script>
    </body>
</html>
