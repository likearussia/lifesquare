@extends('layouts.site')

@section('title') {{ $content->title }} - em Square News @endsection

@section('conteudo')
<header id="news" class="parallax">
    <h1><span>Square</span> news</h1>
</header>

<section class="blog">
    <header class="container-fluid">
        <div class="container">
            <ol class="breadcrumb">
                <li><a href="{{ route('site::home') }}">home</a></li>
                <li><a href="{{ route('blog::index') }}">Square News</a></li>
                <li class="active">{{ $content->title }}</li>
            </ol>
        </div>
    </header>

    <section class="container" id="pr">
        <div class="row">
            <div class="col-md-8">
                <article class="post">
                    <h3>{{ $content->title }}</h3>
                    <h4>por {{ $content->user->first_name }} |
                        <script>
                        window.document.write(moment("{{ $content->created_at }}", "DD/MM/YYYY hh:mm:ss").format('DD/MM/YYYY'));
                        </script></h4>
                    <p>
                        {!! $content->content !!}
                    </p>
                    <div class="myimage">
                        <img class="post-image" src="{{ asset($content->getMedia()[1]->getUrl('blog')) }}" />
                    </div>
                    <hr>
                </article>
                
                @if ($content->comment_status == 'opened')
                <div class="comments">
                    <div class="fb-comments"
                         colorscheme="light"
                         data-href="{{ url()->current() }}"
                         data-numposts="10"
                         order_by="social">
                    </div>
                </div>
                @endif
            </div>
            <div class="col-md-4" id="sidebar">
                <aside>
                    <div class="widget">
                        <form action="{{ route('blog::index') }}" method="get" class="search-blog">
                            <input type="text" name="q" placeholder="o que você procura?" value="{{ request()->q }}">
                            <button type="submit"><i class="fa fa-search"></i></button>
                        </form>
                    </div>
                    @if (count($more_content) > 0)
                    <div class="widget">
                        <h3>Mais notícias</h3>
                        <button class="btn btn-default"  id="nt-example1-prev"><i class="fa fa-arrow-up"></i></button>
                        <ul class="newsticker">
                            @foreach ($more_content as $content)
                                <li><a href="{{ route('blog::show', ['slug' => $content->slug]) }}">{{ $content->title }}</a> </li>
                            @endforeach
                        </ul>
                        <button class="btn btn-default" id="nt-example1-next"><i class="fa fa-arrow-down"></i></button>
                    </div>
                    @endif
                    <div class="widget">
                        <h3>Redes sociais</h3>

                        <ul class="sociais">
                            <li><a href="https://www.facebook.com/tennissquareipiranga/" target="_blank"><i class="fa fa-facebook"></i></a></li>
                            <li><a href="https://twitter.com/TenniSquare1" target="_blank"><i class="fa fa-twitter"></i></a></li>
                            <li><a href="https://www.instagram.com/square_tennis/"  target="_blank"><i class="fa fa-instagram"></i></a></li>
                            <li><a href="https://www.snapchat.com/add/TennisSquare" target="_blank"><i class="fa fa-snapchat-ghost"></i></a></li>
                            <li><a href="https://www.youtube.com/channel/UCysjlGXsMV1WBJT4FH4---A" target="_blank"><i class="fa fa-youtube"></i></a></li>
                        </ul>

                    </div>

                </aside>
            </div>
        </div>
    </section>
    
    <div id="fb-root"></div>
    <script>(function(d, s, id) {
      var js, fjs = d.getElementsByTagName(s)[0];
      if (d.getElementById(id)) return;
      js = d.createElement(s); js.id = id;
      js.src = "//connect.facebook.net/pt_BR/sdk.js#xfbml=1&version=v2.7&appId=263164730395127";
      fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));</script>
@endsection
