@extends('layouts.site')

@section('title') Square News @endsection

@section('conteudo')
<header id="news" class="parallax">
    <h1><span>Square</span> news</h1>
</header>

<section class="content blog">
    <header class="container-fluid">
        <div class="container">
            <ol class="breadcrumb">
                <li><a href="{{ route('site::home') }}">home</a></li>
                <li class="active">Square News</li>
            </ol>
        </div>
    </header>

    <section id="news" class="container-fluid">
        <div class="container">
            <div class="row"> 
                <div class="col-md-8" id="blog-list">
                    @foreach ($content as $c)
                        <div class="card-column">
                            <article class="card2">
                                <a href="{{ route('blog::show', ['slug' => $c->slug]) }}" class="card-image" style=" background-image: url({{ asset($c->getMedia()[0]->getUrl('blogThumb')) }});"></a>
                                <section class="card-content">
                                    <header class="card-header">
                                        <p class="card-subtitle">
                                            <script>
                                                moment.locale('pt-br');
                                                window.document.write(moment("{{ $c->created_at }}", "DD/MM/YYYY hh:mm:ss").fromNow());
                                            </script>
                                        </p>
                                        <h2 class="card-title">
                                            <a href="{{ route('blog::show', ['slug' => $c->slug]) }}">
                                                {{ $c->title }}
                                            </a>
                                        </h2>
                                    </header>
                                    <section class="card-body">
                                        <p>
                                            {{ $c->excerpt }}
                                        </p>
                                        <p class="read-more"><a href="{{ route('blog::show', ['slug' => $c->slug]) }}">Leia mais...</a></p>
                                        <div class="post-footer">

                                            <h5>Compartilhar:</h5>
                                            <ul class="share">
                                                <li><a target="_blank" class="fa fa-facebook" href="{{ url('/') }}"></a></li>
                                                <li><a href="https://twitter.com/share" class="fa fa-twitter"></a> </li>

                                            </ul>
                                            
                                        </div>
                                    </section>
                                </section>
                            </article>
                        </div>
                    @endforeach
                    
                    <div>
                        {{ $content->links() }}
                    </div>
                </div>
                <div class="col-md-4" id="sidebar">
                    <aside>
                        <div class="widget">
                            <form action="{{ route('blog::index') }}" method="get" class="search-blog">
                                <input type="text" name="q" placeholder="o que você procura?" value="{{ request()->q }}">
                                <button type="submit"><i class="fa fa-search"></i></button>
                            </form>
                        </div>
                        @if (count($more_content) > 0)
                        <div class="widget">
                            <h3>Mais notícias</h3>
                            <button class="btn btn-default"  id="nt-example1-prev"><i class="fa fa-arrow-up"></i></button>
                            <ul class="newsticker">
                                @foreach ($more_content as $c)
                                    <li><a href="{{ route('blog::show', ['slug' => $c->slug]) }}">{{ $c->title }}</a> </li>
                                @endforeach
                            </ul>
                            <button class="btn btn-default" id="nt-example1-next"><i class="fa fa-arrow-down"></i></button>
                        </div>
                        @endif
                        <div class="widget">
                            <h3>Redes sociais</h3>

                            <ul class="sociais">
                                <li><a href="https://www.facebook.com/tennissquareipiranga/" target="_blank"><i class="fa fa-facebook"></i></a></li>
                                <li><a href="http://twitter.com/TenniSquare1" target="_blank"><i class="fa fa-twitter"></i></a></li>
                                <li><a href="http://instagram.com/square_tennis" target="_blank"><i class="fa fa-instagram"></i></a></li>
                                <li><a href="https://www.snapchat.com/add/TennisSquare" target="_blank"><i class="fa fa-snapchat-ghost"></i></a></li>
                                <li><a href="https://www.youtube.com/channel/UCysjlGXsMV1WBJT4FH4---A" target="_blank"><i class="fa fa-youtube"></i></a></li>
                            </ul>

                        </div>

                    </aside>
                </div>
            </div>
    </section>
</section>
@endsection

@push('scripts')
<script>
    $('.newsticker').newsTicker({
        row_height: 48,
        max_rows: 2,
        speed: 600,
        direction: 'up',
        duration: 4000,
        autostart: 1,
        pauseOnHover: 0,
        prevButton: $('#nt-example1-prev'),
        nextButton: $('#nt-example1-next')
    });
</script>

<script>
$(document).ready(function(){
  
var x = $(location).attr('href');  
    $('.fa-facebook').attr('href', "http://www.facebook.com/sharer.php?u="+x);
    
})
</script>


<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>


@endpush