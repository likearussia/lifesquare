@extends('layouts.site')

@section('conteudo')
<header id="error" class="parallax">
    <h1>404</h1>
</header>

<section class="content">
    <header class="container-fluid">
        <div class="container">
            <ol class="breadcrumb">
                <li><a href="{{ route('site::home') }}">home</a></li>
                <li class="active">404</li>

            </ol>
        </div>
    </header>

    <section id="pr" class="container">

        <div class="row">
            <div class="col-md-12">
                <h2>ERRO 404</h2>
                <p>Oops, parece que o que você procurou não existe mais ou aconteceu um erro. Tente novamente mais tarde.</p>
                <a href="{{ route('site::home') }}" class="btn btn-default">Voltar pra home</a>
            </div>
        </div>
    </section>

</section>
@endsection
