<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <base href="{{url('/')}}/">
        
        <title>
            Tennis Square - @yield('title')
        </title>
        
        <meta name="author" content="Tennis Square">
        <meta name="Description" content="A Tênis Square é uma tradicional academia de tênis fundada em 1980. A Academia conta com 6 quadras de tênis, sendo 2 de saibro cobertas e 4 quadras de piso rápido descobertas.">
        
        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css">
        <link rel="stylesheet" href="{{ asset('css/jquery.fancybox-1.3.4.css') }}">
        <link rel="stylesheet" href="{{ asset('css/owl.carousel.css') }}">
        <link rel="stylesheet" href="{{ asset('css/owl.theme.default.css') }}">
        <link rel="stylesheet" type="text/css" href="{{ asset('css/style.css') }}">
        <link rel="stylesheet" href="{{ asset('css/jqcloud.css') }}">
        @stack('css')
        
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
        <script src="{{ asset('js/owl.carousel.min.js') }}"></script>
        <script src="{{ asset('js/masonry.pkgd.min.js') }}"></script>
        <script src="{{ asset('js/jquery.tagcloud.js') }}"></script>
        <script src="{{ asset('js/jquery.newsTicker.min.js') }}"></script>
        <script src="{{ asset('js/jquery.bootstrap.wizard.min.js') }}"></script>
        <script src="{{ asset('js/moment/moment-with-locales.min.js') }}"></script>
        <script src="{{ asset('js/main.js') }}"></script>
    </head>

    <body>
        @include('site.shared.menu')

        @yield('conteudo')

        @include('site.shared.rodape_servicos')

        @include('site.shared.rodape_menu')

        @include('site.shared.google_analytics')
        @stack('scripts')
    </body>
</html>