@extends('m2p.layouts.app')

@section('content-header')
<h2 class="sub-header">{{ $title }}</h2>
<ol class="breadcrumb">
    <li><a href="{{ route('m2p::home') }}"><i class="fa fa-dashboard"></i> Início</a></li>
    <li>{{ $title }}</li>
    <li class="active">Criando</li>
</ol>
@endsection

@section('content')
<div class="row">
    <div class="col-md-12">
        @include('m2p.shared.validation')
        @include('flash::message')
    </div>
</div>

<div class="row">
    <div class="col-md-4">
        {{ Form::open(['route' => ['m2p::banner.store'], 'files' => true, 'role' => 'form']) }}
        <input type="hidden" name="content_type" value="{{ $contentType or null }}" />
        <input type="hidden" name="taxonomy"     value="{{ $taxonomy or null }}" />
        <input type="hidden" name="taxonomyTerm" value="{{ $taxonomyTerm or null }}" />

        <div class="form-group">
            <label for="inputTitle">Título</label>
            {{ Form::text('title', null, ['class' => 'form-control', 'id' => 'inputTitle',
                                                    'placeholder' => 'Título', 'autocomplete' => 'off']) }}
        </div>
        
        <div class="form-group">
            <label for="inputMetaText">Texto sobre a imagem</label>
            {{ Form::text('meta[banner][text]', null, ['class' => 'form-control', 'id' => 'inputMetaText',
                                                    'placeholder' => 'Texto sobre a imagem', 'autocomplete' => 'off']) }}
        </div>

        <div class="form-group">
            <label for="inputFile">Banner</label>
            <input name="main_image" type="file" id="inputFile">
            <br/>

            <p class="help-block">
                Imagem do banner.
                Tamanho máximo do arquivo: {{ $maxUploadSize }}M.
                Resolução: 1600x500 px.
            </p>
        </div>

        <div class="form-group">
            <label for="inputStatus">Status</label>
            {{ Form::select('status', $statuses, null, ['placeholder' => 'Selecione',
                            'required', 'class' => 'form-control', 'id' => 'inputStatus']) }}
        </div>

        <div class="form-group content-send">
            <button type="submit" class="btn btn-primary">Enviar</button>
        </div>
        {!! Form::close() !!}
    </div>
</div>
@endsection