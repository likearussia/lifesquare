@extends('m2p.layouts.app')

@section('content-header')
<h2 class="sub-header">{{ $title }}</h2>
<ol class="breadcrumb">
    <li><a href="{{ route('m2p::home') }}"><i class="fa fa-dashboard"></i> Início</a></li>
    <li>{{ $title }}</li>
    <li class="active">Criando</li>
</ol>
@endsection

@section('content')
<div class="row">
    <div class="col-md-9 content-main">
        @include('m2p.shared.validation')
        @include('flash::message')

        {{ Form::open(['route' => ['m2p::content.store'], 'files' => true, 'role' => 'form',
                                    'autocomplete' => 'off']) }}
        <input type="hidden" name="content_type" value="{{ $contentType or null }}" />
        <input type="hidden" name="taxonomy"     value="{{ $taxonomy or null }}" />
        <input type="hidden" name="taxonomyTerm" value="{{ $taxonomyTerm or null }}" />

        <div class="form-group">
            <label for="inputTitle">Título</label>
            {{ Form::text('title', null, ['class' => 'form-control', 'id' => 'inputTitle', 'placeholder' => 'Título']) }}
        </div>
        <div class="form-group">
            <label for="inputExcerpt">Resumo</label>
            {{ Form::textarea('excerpt', null, ['id' => 'inputExcerpt',
                                'placeholder' => 'Digite aqui o resumo da sua publicação',
                                'style' => 'width: 100%; height: 100px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;']) }}
        </div>
        <div class="form-group">
            <label for="inputText">Texto</label>
            {{ Form::textarea('content', null, ['class' => 'rich-text',
                                'placeholder' => 'Digite aqui o texto da sua publicação',
                                'style' => 'width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;']) }}
        </div>
        
        <div class="form-group content-send">
            <button type="submit" class="btn btn-primary">Enviar</button>
        </div>
    </div>
    
    <div class="col-md-3 content-widgets">
        <div class="panel panel-default" style="display: none;">
            <div class="panel-heading">Categoria</div>
            <div class="panel-body">
                {{ Form::select('meta[taxonomy][category]', $categories, $category, ['placeholder' => 'Selecione',
                                    'class' => 'form-control', 'id' => 'inputCategory']) }}
            </div>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading">Status</div>
            <div class="panel-body">
                {{ Form::select('status', $statuses, null, ['placeholder' => 'Selecione',
                                    'required', 'class' => 'form-control', 'id' => 'inputStatus']) }}
            </div>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading">Comentários</div>
            <div class="panel-body">
                {{ Form::select('comment_status', $commentStatuses, null, ['placeholder' => 'Selecione',
                                            'required', 'class' => 'form-control', 'id' => 'inputComments']) }}
            </div>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading">Imagem para a chamada</div>
            <div class="panel-body">
                <input name="chamada_image" type="file" id="inputFileChamada">
                <br/>

                <p class="help-block">
                    Imagem para a chamada. Tamanho máximo do arquivo: {{ $maxUploadSize }}M.
                    @if (request()->contentType == 'page')
                        Resolução ideal: 740x395 px.
                    @elseif (request()->contentType == 'post')
                        Resolução ideal: 386x240 px.
                    @endif
                </p>
            </div>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading">Banner</div>
            <div class="panel-body">
                <input name="main_image" type="file" id="inputFile">
                <br/>

                <p class="help-block">
                    Imagem de destaque. Tamanho máximo do arquivo: {{ $maxUploadSize }}M.
                    @if (request()->contentType == 'page')
                        Resolução ideal: 1872x773 px.
                    @elseif (request()->contentType == 'post')
                        Resolução ideal: 700 px.
                    @endif
                </p>
            </div>
        </div>
    </div>

    {!! Form::close() !!}
</div>
@endsection

@push('scripts')
<script>
    $(function () {
        //bootstrap WYSIHTML5 - text editor
        $(".rich-text").wysihtml5();
    });
</script>
@endpush