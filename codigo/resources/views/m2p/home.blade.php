@extends('m2p.layouts.app')

@section('content-header')
<h2 class="sub-header">Acessos</h2>
<ol class="breadcrumb">
    <li><a href="{{ route('m2p::home') }}"><i class="fa fa-dashboard"></i> Início</a></li>
    <li class="active">Acessos</li>
</ol>
@endsection

@push('css')
<link href="m2p_assets/dist/morris.js/morris.css" rel="stylesheet">
@endpush

@section('content')
<div class="row">
    <div class="col-md-9">
        <div class="panel panel-default">
            <div class="panel-heading">Visitantes nos últimos sete dias</div>
            <div class="panel-body">
                <div class="chart tab-pane active" id="revenue-chart" style="position: relative; height: 300px;"></div>
            </div>
        </div>
    </div>
    
    <div class="col-md-3">
        <div class="panel panel-default">
            <div class="panel-heading">Páginas mais acessadas</div>
            <div class="panel-body">
                <table class="table table-bordered table-responsive">
                    <tbody>
                        @foreach($mostVisitedPages as $i)
                            <tr>
                                <td>{{ $i['pageTitle'] }}</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection

@push('scripts')
<!-- Morris.js charts -->
<script src="m2p_assets/dist/raphael/raphael.min.js"></script>
<script src="m2p_assets/dist/morris.js/morris.min.js"></script>

<script type="text/javascript">
var area = new Morris.Area({
    element: 'revenue-chart',
    resize: true,
    data: {!! $visitorsAndPageViews !!},
    xkey: 'y',
    ykeys: ['visitors'],
    labels: ['Visitantes'],
    lineColors: ['#a0d0e0', '#3c8dbc'],
    hideHover: 'auto',
    xLabels: ['day'],
    xLabelFormat: function(d) {
        return d.getDate()+'/'+(d.getMonth()+1)+'/'+d.getFullYear(); 
    },
    dateFormat: function (ts) {
        var d = new Date(ts);
        return d.getDate()+'/'+(d.getMonth()+1)+'/'+d.getFullYear(); 
    }
});
</script>
@endpush