@extends('m2p.layouts.app')

@section('content-header')
<h2 class="sub-header">{{ $title }}</h2>
<ol class="breadcrumb">
    <li><a href="{{ route('m2p::home') }}"><i class="fa fa-dashboard"></i> Início</a></li>
    <li class="active">{{ $title }}</li>
</ol>
@endsection

@section('content-options')
<form class="form-inline" action="{{ route('m2p::infrastructure.index') }}" method="GET" autocomplete="off">
    <div class="form-group">
        <input type="text" class="form-control" name="s[title]" placeholder="Título" value="<?= (request()->has('s.title')) ? request()->s['title'] : '' ?>">
        <input type="hidden" name="contentType"  value="{{ request()->contentType }}" />
        <input type="hidden" name="taxonomy"     value="{{ request()->taxonomy }}" />
        <input type="hidden" name="taxonomyTerm" value="{{ request()->taxonomyTerm }}" />
    </div>
    <button type="submit" class="btn btn-default">
        <i class="material-icons">search</i>
    </button>
    
    <div class="form-group pull-right">
        <ul class="list-unstyled">
            <li>
                <a class="btn btn-default" href="{{ route('m2p::infrastructure.create',
                                                            ['contentType' => request()->contentType,
                                                            'taxonomy' => request()->taxonomy,
                                                            'taxonomyTerm' => request()->taxonomyTerm]) }}">
                    <i class="material-icons">add</i> Criar
                </a>
            </li>
        </ul>
    </div>
</form>
@endsection

@section('content')
<div class="row">
    <div class="col-md-12">
        @include('m2p.shared.validation')
        @include('flash::message')

        <table class="table table-striped table-hover table-responsive">
            <thead>
                <tr>
                    {!! Table::header('title', 'Título') !!}
                    {!! Table::header('status', 'Status') !!}
                    {!! Table::header('created_at', 'Criado em') !!}
                    {!! Table::header('author', 'Autor') !!}
                    <th>Imagem</th>
                    <th>Editar</th>
                    <th>Deletar</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($content as $c)
                <tr>
                    <td>{{ $c->title }}</td>
                    <td>{{ $statuses[$c->status] }}</td>
                    <td>{{ $c->created_at }}</td>
                    <td>{{ $c->user->login }}</td>
                    <td>
                        <div>
                            <img src="{{ $c->getFirstMediaUrl('infrastructure', 'adminListThumb') }}" />
                        </div>
                    </td>
                    <td>
                        <a href="{{ route('m2p::infrastructure.edit', ['content' => $c->slug, 'contentType' => request()->contentType]) }}">
                            <i class="material-icons">mode_edit</i>
                        </a>
                    </td>
                    <td>
                        <a href="{{ route('m2p::infrastructure.destroy', ['content' => $c->slug, 'contentType' => request()->contentType]) }}"
                           onclick="return confirm('Tem certeza que deseja deletar o registro ?')">
                            <i class="material-icons">delete_forever</i>
                        </a>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
        
        <div class="pagination-container">
            {!! Table::pagination($content) !!}
        </div>
    </div>
</div>
@endsection
