<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>
            @yield('title', 'Tennis Square | Dashboard')
        </title>
        <base href="{{ config('app.url') }}/">

        <link href="m2p_assets/dist/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <link href="m2p_assets/dist/font-awesome/css/font-awesome.min.css" rel="stylesheet">
        <link href="m2p_assets/css/app.css" rel="stylesheet">
        <link href="m2p_assets/css/menu.css" rel="stylesheet">
        <link href="m2p_assets/css/project.css" rel="stylesheet">
        <link href="m2p_assets/dist/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css" rel="stylesheet">
        @stack('css')

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>

    <body>
        @include('m2p.shared.menu')
        
        <div class="row container-main">
            <!-- Main Content -->
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        @yield('content-header')
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12 content-options">
                        @yield('content-options')
                    </div>
                </div>

                @yield('content')
            </div>
        </div>

        <script src="m2p_assets/dist/jquery/jquery.min.js"></script>
        <script src="m2p_assets/dist/tether/js/tether.min.js"></script>
        <script src="m2p_assets/dist/bootstrap/js/bootstrap.min.js"></script>
        <script src="m2p_assets/dist/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
        <script src="m2p_assets/dist/jquery-ui/jquery-ui.min.js"></script>
        <script src="m2p_assets/js/app.js"></script>
        @stack('scripts')
    </body>
</html>
