@extends('m2p.layouts.app')

@section('content-header')
<h2 class="sub-header">{{ $title }}</h2>
<ol class="breadcrumb">
    <li><a href="{{ route('m2p::home') }}"><i class="fa fa-dashboard"></i> Início</a></li>
    <li>{{ $title }}</li>
    <li class="active">Criando</li>
</ol>
@endsection

@section('content')
<div class="row">
    <div class="col-md-12">
        @include('m2p.shared.validation')
        @include('flash::message')
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        {{ Form::open(['route' => ['m2p::plan.store'], 'files' => true, 'role' => 'form',
                                    'autocomplete' => 'off']) }}
        <input type="hidden" name="content_type" value="{{ $contentType or null }}" />
        <input type="hidden" name="taxonomy"     value="{{ $taxonomy or null }}" />
        <input type="hidden" name="taxonomyTerm" value="{{ $taxonomyTerm or null }}" />

        <div class="form-group">
            <label for="inputTitle">Título</label>
            {{ Form::text('title', null, ['class' => 'form-control', 'id' => 'inputTitle', 'placeholder' => 'Título']) }}
        </div>
        <div class="form-group">
            <label for="inputText">Texto</label>
            {{ Form::textarea('content', null, ['class' => 'rich-text', 'id' => 'inputText',
                                'placeholder' => 'Digite aqui os detalhes do plano',
                                'style' => 'width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;']) }}
        </div>
        <div class="form-group">
            <label for="inputCost">Valor</label>
            {{ Form::number('meta[plan][cost]', null, ['required', 'class' => 'form-control',
                                                                    'id' => 'inputCost', 'step' => 'any']) }}
        </div>

        <div class="form-group content-send">
            <button type="submit" class="btn btn-primary">Enviar</button>
        </div>
        {!! Form::close() !!}
    </div>
</div>
@endsection

@push('scripts')
<script>
    $(function () {
        //bootstrap WYSIHTML5 - text editor
        $(".rich-text").wysihtml5();
    });
</script>
@endpush