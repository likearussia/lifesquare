@extends('m2p.layouts.app')

@section('content-header')
<h2 class="sub-header">{{ $title }}</h2>
<ol class="breadcrumb">
    <li><a href="{{ route('m2p::home') }}"><i class="fa fa-dashboard"></i> Início</a></li>
    <li>{{ $title }}</li>
    <li class="active">Criando</li>
</ol>
@endsection

@section('content')
<div class="row">
    <div class="col-md-12">
        @include('m2p.shared.validation')
        @include('flash::message')
    </div>

    <div class="col-md-6 content-main">
        {{ Form::open(['route' => ['m2p::player.store'], 'files' => true, 'role' => 'form',
                                        'autocomplete' => 'off']) }}
        <input type="hidden" name="content_type" value="{{ $contentType or null }}" />
        <input type="hidden" name="taxonomy"     value="{{ $taxonomy or null }}" />
        <input type="hidden" name="taxonomyTerm" value="{{ $taxonomyTerm or null }}" />

        <div class="form-group">
            <label for="inputName">Nome</label>
            {{ Form::text('meta[player][name]', null, ['class' => 'form-control', 'id' => 'inputName',
                                                    'placeholder' => 'Nome']) }}
        </div>
        <div class="form-group">
            <label for="inputPoints">Pontos</label>
            {{ Form::number('meta[player][points]', null, ['class' => 'form-control', 'id' => 'inputPoints',
                                                        'placeholder' => 'Pontos']) }}
        </div>
        <div class="form-group">
            <label for="inputRank">Ranking</label>
            {{ Form::select('meta[player][rank]', $ranks, null, ['class' => 'form-control', 'id' => 'inputRank',
                                                        'placeholder' => 'Selecione',]) }}
        </div>
        
        <div class="form-group">
            <label for="inputStatus">Status</label>
            {{ Form::select('status', $statuses, 'published', ['placeholder' => 'Selecione',
                            'required', 'class' => 'form-control', 'id' => 'inputStatus']) }}
        </div>

        <div class="form-group content-send">
            <button type="submit" class="btn btn-primary">Enviar</button>
        </div>
    </div>

    <div class="col-md-3 content-widgets">
        <div class="panel panel-default">
            <div class="panel-heading">Imagem</div>
            <div class="panel-body">
                <input name="main_image" type="file" id="inputFile">
                <br/>

                <p class="help-block">Imagem do jogador. Tamanho máximo do arquivo: {{ $maxUploadSize }}M.</p>
            </div>
        </div>
    </div>
    
    {!! Form::close() !!}
</div>
@endsection