@extends('m2p.layouts.app')

@section('content-header')
<h2 class="sub-header">{{ $title }}</h2>
<ol class="breadcrumb">
    <li><a href="{{ route('m2p::home') }}"><i class="fa fa-dashboard"></i> Início</a></li>
    <li class="active">{{ $title }}</li>
</ol>
@endsection

@section('content-options')
<form class="form-inline" action="{{ route('m2p::player.index') }}" method="GET" autocomplete="off">
    <div class="form-group">
        <select class="form-control" name="s[rank]" placeholder="Ranking" onchange="this.form.submit()">
            <option value="">Ranking</option>
            @foreach ($ranks as $id => $name)
                @if (!empty(request()->s['rank']) && request()->s['rank'] == $id)
                    <option selected value="{{ $id }}">{{ $name }}</option>
                @else
                    <option value="{{ $id }}">{{ $name }}</option>
                @endif
            @endforeach
        </select>
        <input type="text" class="form-control" name="s[name]" placeholder="Name" value="<?= (request()->has('s.name')) ? request()->s['name'] : '' ?>">
        <input type="hidden" name="contentType"  value="{{ request()->contentType }}" />
        <input type="hidden" name="taxonomy"     value="{{ request()->taxonomy }}" />
        <input type="hidden" name="taxonomyTerm" value="{{ request()->taxonomyTerm }}" />
    </div>
    <button type="submit" class="btn btn-default">
        <i class="material-icons">search</i>
    </button>
    
    <div class="form-group pull-right">
        <ul class="list-unstyled">
            <li>
                <a class="btn btn-default" href="{{ route('m2p::player.create',
                                                            ['contentType' => request()->contentType,
                                                            'taxonomy' => request()->taxonomy,
                                                            'taxonomyTerm' => request()->taxonomyTerm]) }}">
                    <i class="material-icons">add</i> Criar
                </a>
            </li>
        </ul>
    </div>
</form>
@endsection

@section('content')
<div class="row">
    <div class="col-md-12">
        @include('m2p.shared.validation')
        @include('flash::message')
        
        <table class="table table-striped table-hover table-responsive">
            <thead>
                <tr>
                    {!! Table::header('meta.name', 'Nome') !!}
                    {!! Table::header('meta.points', 'Pontuação', ['sortAs' => 'unsigned']) !!}
                    {!! Table::header('meta.rank', 'Ranking', ['sortAs' => 'unsigned']) !!}
                    {!! Table::header('status', 'Status') !!}
                    {!! Table::header('created_at', 'Criado em') !!}
                    {!! Table::header('author', 'Autor') !!}
                    <th>Foto</th>
                    <th>Editar</th>
                    <th>Deletar</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($content as $c)
                <?php
                $c['value'] = json_decode($c['value']);
                $rank = (isset($c['value']->rank)) ? $c['value']->rank : null;
                $playerThumb = $c->getFirstMediaUrl('player', 'playerThumb');
                if (empty($playerThumb)) {
                    if (str_contains($ranks[$rank], 'mascu')) {
                     $playerThumb = asset('img/players/man.png');
                    } else {
                        $playerThumb = asset('img/players/woman.png');
                    }
                }
                ?>
                <tr>
                    <td>{{ $c['value']->name or null }}</td>
                    <td>
                        <a href="#" class="player-points" data-type="text" data-pk="{{ $c->id }}"
                           data-name="meta.points"
                           data-url="{{ route('m2p::player.storeField') }}" data-title="Pontuação">
                            {{ $c['value']->points or null  }}
                        </a>
                    </td>
                    <td>{{ $ranks[$rank] or '' }}</td>
                    <td>{{ $statuses[$c->status] }}</td>
                    <td>{{ $c->created_at }}</td>
                    <td>{{ $c->user->login }}</td>
                    <td>
                        <div class="thumb-min-jogador">
                            <img src="{{ $playerThumb }}" />
                        </div>
                    </td>
                    <td>
                        <a href="{{ route('m2p::player.edit', ['content' => $c->slug, 'contentType' => 'player']) }}">
                            <i class="material-icons">mode_edit</i>
                        </a>
                    </td>
                    <td>
                        <a href="{{ route('m2p::player.destroy', ['content' => $c->slug, 'contentType' => 'player']) }}"
                           onclick="return confirm('Tem certeza que deseja deletar o registro ?')">
                            <i class="material-icons">delete_forever</i>
                        </a>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
        
        <div class="pagination-container">
            {!! Table::pagination($content) !!}
        </div>
    </div>
</div>
@endsection


@push('css')
<link rel="stylesheet" href="m2p_assets/dist/bootstrap3-editable/bootstrap3-editable/css/bootstrap-editable.css" />
@endpush

@push('scripts')
    <script type="text/javascript" src="m2p_assets/dist/bootstrap3-editable/bootstrap3-editable/js/bootstrap-editable.min.js"></script>
    
    <script type="text/javascript">
    $(document).ready(function() {
        $('.player-points').editable();
    });
    </script>
@endpush