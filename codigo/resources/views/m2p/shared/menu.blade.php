<?php
// #TODO esta lógica não deveria ficar aqui
$actual = [];
$actual['routeName']    = (!empty(Route::current())) ? Route::current()->getName() : null;

/**
 * Verifica se os parâmetros passados são iguais aos enviados por GET na página atual.
 * Esta funcção é utilizada para ver se o menu atual é X
 * 
 * @param string $contentType
 * @param string $contentTaxonomy
 * @param string $taxonomy
 * @param string $taxonomyTerm
 * 
 * @return boolean
 */
function checkParameters($contentType = null, $contentTaxonomy = null, $taxonomy = null, $taxonomyTerm = null, $forceMenu = null) {
    
    if ($contentType == request()->get('contentType') &&
        $contentTaxonomy == request()->get('contentTaxonomy') &&
        $taxonomy == request()->get('taxonomy') &&
        $taxonomyTerm == request()->get('taxonomyTerm')) {
        return true;
    }
    
    return false;
}

/**
 * Esta função verifica se a página atual é do menu "Academia de Tenis".
 * Esta função existe pois foi solicitado que este menu incorporasse itens que não tem correspondência
 * lógica entre si.
 * 
 * @return bool
 */
function actualPageIsAcademiaDeTenis()
{
    $parameterContent = (!empty(Route::current())) ? Route::current()->getParameter('content') : null;
    if ($parameterContent == 'academia-de-tenis') {
        return true;
    }
    
    return checkParameters('page', null, 'category', 2);
}

/**
 * Esta função verifica se a página atual é do menu "Fitness center".
 * Esta função existe pois foi solicitado que este menu incorporasse itens que não tem correspondência
 * lógica entre si.
 * 
 * @return bool
 */
function actualPageIsFitnessCenter()
{
    $parameterContent = (!empty(Route::current())) ? Route::current()->getParameter('content') : null;
    if ($parameterContent == 'fitness-center') {
        return true;
    }
    
    return checkParameters('page', null, 'category', 3);
}
?>

{{--
Utilizado:
http://bootsnipp.com/snippets/featured/responsive-sidebar-menu

Podem ser uteis:
http://bootsnipp.com/snippets/4OjXB
http://bootsnipp.com/snippets/33qKz
--}}

<nav class="navbar navbar-m2p sidebar" role="navigation">
    <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-sidebar-navbar-collapse-1">
                <span class="sr-only">Alterar navegação</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">
                Tennis<b>Square</b>
            </a>
        </div>
        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-sidebar-navbar-collapse-1">
            <ul class="nav navbar-nav">
                <!-- Home -->
                <li class="<?= ($actual['routeName'] == 'm2p::home') ? 'active open' : null; ?>">
                  <a href="{{ route('m2p::home') }}">
                      <span class="pull-right hidden-xs showopacity glyphicon material-icons">av_timer</span> Acessos
                  </a>
                </li>
                <!-- Banner -->
                <li class="dropdown <?= (checkParameters('banner')) ? 'active open' : null ?>">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <span class="menu-icon pull-right hidden-xs showopacity glyphicon material-icons">burst_mode</span>
                        Banners <span class="caret"></span>
                    </a>
                    <ul class="dropdown-menu forAnimate" role="menu">
                        <li><a href="{{ route('m2p::banner.create', ['contentType' => 'banner']) }}"><i class="material-icons">add</i> Criar</a></li>
                        <li><a href="{{ route('m2p::banner.index', ['contentType' => 'banner']) }}"><i class="material-icons">sort</i> Ver todos</a></li>
                    </ul>
                </li>
                <li class="separator">Conteúdo</li>
                <!-- Institucional -->
                <li class="<?= (checkParameters('page', null, 'category', 1)) ? 'active open' : null ?>">
                  <a href="{{ route('m2p::content.index', ['contentType' => 'page', 'taxonomy' => 'category', 'taxonomyTerm' => 1]) }}">
                      <span class="menu-icon pull-right hidden-xs showopacity glyphicon material-icons">perm_contact_calendar</span>Institucional
                  </a>
                </li>
                <!-- Parceiros -->
                <li class="dropdown <?= (checkParameters('partner')) ? 'active open' : null ?>">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <span class="menu-icon pull-right hidden-xs showopacity glyphicon material-icons">work</span>
                        Parceiros <span class="caret"></span>
                    </a>
                    <ul class="dropdown-menu forAnimate" role="menu">
                        <li><a href="{{ route('m2p::partner.create', ['contentType' => 'partner']) }}"><i class="material-icons">add</i> Criar</a></li>
                        <li><a href="{{ route('m2p::partner.index', ['contentType' => 'partner']) }}"><i class="material-icons">sort</i> Ver todos</a></li>
                    </ul>
                </li>
                <!-- Academia de Tênis -->
                <li class="dropdown <?= (actualPageIsAcademiaDeTenis()) ? 'active open' : null ?>">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <span class="menu-icon pull-right hidden-xs showopacity glyphicon material-icons">directions_walk</span>
                        Academia de Tênis <span class="caret"></span>
                    </a>
                    <ul class="dropdown-menu forAnimate" role="menu">
                        <li><a href="{{ route('m2p::taxonomy.edit', ['slug' => 'academia-de-tenis', 'contentTaxonomy' => 'category', 'taxonomyTerm' => 2]) }}"><i class="material-icons">translate</i> Descrição</a></li>
                        <li><a href="{{ route('m2p::content.create', ['contentType' => 'page', 'taxonomy' => 'category', 'taxonomyTerm' => 2]) }}"><i class="material-icons">add</i> Criar</a></li>
                        <li><a href="{{ route('m2p::content.index', ['contentType' => 'page', 'taxonomy' => 'category', 'taxonomyTerm' => 2]) }}"><i class="material-icons">sort</i> Ver todos</a></li>
                    </ul>
                </li>
                <!-- Fitness Center -->
                <li class="dropdown <?= (actualPageIsFitnessCenter()) ? 'active open' : null ?>">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <span class="menu-icon pull-right hidden-xs showopacity glyphicon material-icons">fitness_center</span>
                        Fitness Center <span class="caret"></span>
                    </a>
                    <ul class="dropdown-menu forAnimate" role="menu">
                        <li><a href="{{ route('m2p::taxonomy.edit', ['slug' => 'fitness-center', 'contentTaxonomy' => 'category']) }}"><i class="material-icons">translate</i> Descrição</a></li>
                        <li><a href="{{ route('m2p::content.create', ['contentType' => 'page', 'taxonomy' => 'category', 'taxonomyTerm' => 3]) }}"><i class="material-icons">add</i> Criar</a></li>
                        <li><a href="{{ route('m2p::content.index', ['contentType' => 'page', 'taxonomy' => 'category', 'taxonomyTerm' => 3]) }}"><i class="material-icons">sort</i> Ver todos</a></li>
                    </ul>
                </li>
                <!-- Fitness Center -->
                <li class="dropdown <?= (checkParameters('plan')) ? 'active open' : null ?>">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <span class="menu-icon pull-right hidden-xs showopacity glyphicon material-icons">description</span>
                        Planos <span class="caret"></span>
                    </a>
                    <ul class="dropdown-menu forAnimate" role="menu">
                        <li><a href="{{ route('m2p::plan.create', ['contentType' => 'plan']) }}"><i class="material-icons">add</i> Criar</a></li>
                        <li><a href="{{ route('m2p::plan.index', ['contentType' => 'plan']) }}"><i class="material-icons">sort</i> Ver todos</a></li>
                    </ul>
                </li>
                <!-- Estrutura -->
                <li class="dropdown <?= (checkParameters('infrastructure')) ? 'active open' : null ?>">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <span class="menu-icon pull-right hidden-xs showopacity glyphicon material-icons">business</span>
                        Estrutura <span class="caret"></span>
                    </a>
                    <ul class="dropdown-menu forAnimate" role="menu">
                        <li><a href="{{ route('m2p::infrastructure.create', ['contentType' => 'infrastructure']) }}"><i class="material-icons">add</i> Criar</a></li>
                        <li><a href="{{ route('m2p::infrastructure.index', ['contentType' => 'infrastructure']) }}"><i class="material-icons">sort</i> Ver todos</a></li>
                    </ul>
                </li>
                <!-- Square News -->
                <li class="dropdown <?= (checkParameters('post')) ? 'active open' : null ?>">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <span class="menu-icon pull-right hidden-xs showopacity glyphicon material-icons">chat_bubble_outline</span>
                        Square News <span class="caret"></span>
                    </a>
                    <ul class="dropdown-menu forAnimate" role="menu">
                        <li><a href="{{ route('m2p::content.create', ['contentType' => 'post']) }}"><i class="material-icons">add</i> Criar</a></li>
                        <li><a href="{{ route('m2p::content.index', ['contentType' => 'post']) }}"><i class="material-icons">sort</i> Ver todos</a></li>
                    </ul>
                </li>
                <!-- Tags -->
<!--                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <span style="font-size:16px;" class="pull-right hidden-xs showopacity glyphicon material-icons">label</span>
                        Tags <span class="caret"></span>
                    </a>
                    <ul class="dropdown-menu forAnimate" role="menu">
                        <li><a href="{{ route('m2p::taxonomy.create', ['contentTaxonomy' => 'tag']) }}"> <i class="material-icons">add</i> Criar</a></li>
                        <li><a href="{{ route('m2p::taxonomy.index', ['contentTaxonomy' => 'tag']) }}"> <i class="material-icons">sort</i> Ver todas</a></li>
                    </ul>
                </li>-->
                <li class="separator">Ranking</li>
                <!-- Ranking - jogadores -->
                <li class="dropdown <?= (checkParameters('player')) ? 'active open' : null ?>">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <span class="menu-icon pull-right hidden-xs showopacity glyphicon material-icons">directions_run</span>
                        Jogadores <span class="caret"></span>
                    </a>
                    <ul class="dropdown-menu forAnimate" role="menu">
                        <li><a href="{{ route('m2p::player.create', ['contentType' => 'player']) }}"><i class="material-icons">add</i> Criar</a></li>
                        <li><a href="{{ route('m2p::player.index', ['contentType' => 'player']) }}"><i class="material-icons">sort</i> Ver todos</a></li>
                    </ul>
                </li>
                <!-- Ranking -->
                <li class="dropdown <?= (checkParameters(null, 'rank')) ? 'active' : null ?>">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <span class="menu-icon pull-right hidden-xs showopacity glyphicon material-icons">format_list_numbered</span>
                        Ranking Square <span class="caret"></span>
                    </a>
                    <ul class="dropdown-menu forAnimate" role="menu">
                        <li><a href="{{ route('m2p::taxonomy.create', ['contentTaxonomy' => 'rank']) }}"><i class="material-icons">add</i>  Criar</a></li>
                        <li><a href="{{ route('m2p::taxonomy.index', ['contentTaxonomy' => 'rank']) }}"> <i class="material-icons">sort</i> Ver todos</a></li>
                    </ul>
                </li>
                <li class="separator">Sistema</li>
                <!-- Usuários -->
                <li class="dropdown <?= (strpos($actual['routeName'], 'm2p::user') === 0) ? 'active open' : null; ?>">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <span class="menu-icon pull-right hidden-xs showopacity glyphicon material-icons">group</span>
                        Usuários <span class="caret"></span>
                    </a>
                    <ul class="dropdown-menu forAnimate" role="menu">
                        <li><a href="{{ route('m2p::user.create') }}"><i class="material-icons">add</i> Criar</a></li>
                        <li><a href="{{ route('m2p::user.index') }}"><i class="material-icons">sort</i> Ver todos</a></li>
                    </ul>
                </li>
                <!-- Sair -->
                <li>
                    <a href="{{ route('logout') }}">
                        <span class="menu-icon pull-right hidden-xs showopacity glyphicon material-icons">exit_to_app</span> Sair
                    </a>
                </li>
            </ul>
        </div>
    </div>
</nav>
