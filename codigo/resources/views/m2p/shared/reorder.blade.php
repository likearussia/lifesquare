<script>
$(function() {
    $('tbody').sortable({
        cursor: 'grabbing',
        cursorAt: { left: 5 },
        start: function(event, ui) {
            ui.item.startPosition = ui.item.index();
        },
        update: function(event, ui) {
            var contentId = ui.item.data('id');
            var startPosition = +ui.item.startPosition;
            var finalPosition = +ui.item.index();
            var serviceUrl = $(location).attr('href') +
                        '&reorder[contentId]=' + contentId +
                        '&reorder[startPosition]=' + startPosition +
                        '&reorder[finalPosition]=' + finalPosition;
            
            $.ajax({
                url: serviceUrl,
                error: function(jqXHR, textStatus, errorThrown) {
                    if (jqXHR.responseJSON.hasOwnProperty('message')) {
                        alert(jqXHR.responseJSON.message);
                    } else {
                        alert('Erro!');
                    }
                },
                dataType: 'json'
            });
        }
    });
});
</script>