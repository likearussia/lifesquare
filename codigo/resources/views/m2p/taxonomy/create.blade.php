@extends('m2p.layouts.app')

@section('content-header')
<h2 class="sub-header">{{ $title }}</h2>
<ol class="breadcrumb">
    <li><a href="{{ route('m2p::home') }}"><i class="fa fa-dashboard"></i> Início</a></li>
    <li>{{ $title }}</li>
    <li class="active">Criando</li>
</ol>
@endsection

@section('content')
<div class="row">
    <div class="col-md-12">
        @include('m2p.shared.validation')
        @include('flash::message')
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        {{ Form::open(['route' => ['m2p::taxonomy.store'], 'files' => true, 'role' => 'form',
                                    'autocomplete' => 'off']) }}
        <input type="hidden" name="content_taxonomy" value="{{ $contentTaxonomy }}" />
        <div class="form-group" style="<?= ($contentTaxonomy == 'category') ? 'display: none' : null; ?>">
            <label for="inputTerm">Nome</label>
            {{ Form::text('term', null, ['class' => 'form-control', 'id' => 'inputTerm', 'placeholder' => 'Nome']) }}
        </div>
        <div class="form-group">
            <label for="inputDescription">Descrição</label>
            {{ Form::textarea('description', null, ['id' => 'inputDescription',
                                'style' => 'width: 100%; height: 100px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;']) }}
        </div>
        <div class="form-group content-send">
            <button type="submit" class="btn btn-primary">Enviar</button>
        </div>
        {!! Form::close() !!}
    </div>
</div>
@endsection

