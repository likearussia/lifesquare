@extends('m2p.layouts.app')

@section('content-header')
<h2 class="sub-header">{{ $title }}</h2>
<ol class="breadcrumb">
    <li><a href="{{ route('m2p::home') }}"><i class="fa fa-dashboard"></i> Início</a></li>
    <li class="active">{{ $title }}</li>
</ol>
@endsection

@section('content-options')
<form class="form-inline" action="{{ route('m2p::taxonomy.index') }}" method="GET" autocomplete="off">
    <div class="form-group">
        <input type="text" class="form-control" name="s[term]" placeholder="{{ $title }}" value="<?= (request()->has('s.term')) ? request()->s['term'] : '' ?>">
        <input type="hidden" name="contentTaxonomy"  value="{{ request()->contentTaxonomy }}" />
    </div>
    <button type="submit" class="btn btn-default">
        <i class="material-icons">search</i>
    </button>
    
    <div class="form-group pull-right">
        <ul class="list-unstyled">
            <li>
                <a class="btn btn-default" href="{{ route('m2p::taxonomy.create',
                                                            ['contentTaxonomy' => request()->contentTaxonomy]) }}">
                    <i class="material-icons">add</i> Criar
                </a>
            </li>
        </ul>
    </div>
</form>
@endsection

@section('content')
<div class="row">
    <div class="col-md-12">
        @include('m2p.shared.validation')
        @include('flash::message')

        <table class="table table-striped table-hover table-responsive">
            <thead>
                <tr>
                    @if (isset($terms[0]) && $terms[0]->taxonomy()->first()->name == 'category')
                    {!! Table::header('term', 'Nome da categoria') !!}
                    @else
                    {!! Table::header('term', $title) !!}
                    @endif
                    {!! Table::header('slug', 'URL amigável') !!}
                    {!! Table::header('description', 'Descrição') !!}
                    <th>Editar</th>
                    @if (isset($terms[0]) && $terms[0]->taxonomy()->first()->name != 'category')
                    <th>Deletar</th>
                    @endif
                </tr>
            </thead>
            <tbody>
                @foreach ($terms as $t)
                <tr>
                    <td>{{ $t->term }}</td>
                    <td>{{ $t->slug }}</td>
                    <td>{{ str_limit($t->description, 100) }}</td>
                    <td>
                        <a href="{{ route('m2p::taxonomy.edit', ['content' => $t->slug, 'contentTaxonomy' => request()->contentTaxonomy]) }}">
                            <i class="material-icons">mode_edit</i>
                        </a>
                    </td>
                    @if ($t->taxonomy()->first()->name != 'category')
                    <td>
                        <a href="{{ route('m2p::taxonomy.destroy', ['content' => $t->slug, 'contentTaxonomy' => request()->contentTaxonomy]) }}"
                           onclick="return confirm('Tem certeza que deseja deletar o registro ?')">
                            <i class="material-icons">delete_forever</i>
                        </a>
                    </td>
                    @endif
                </tr>
                @endforeach
            </tbody>
        </table>
        
        <div class="pagination-container">
            {!! Table::pagination($terms) !!}
        </div>
    </div>
</div>
@endsection
