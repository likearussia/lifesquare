@extends('m2p.layouts.app')

@section('content-header')
<h2 class="sub-header">Usuários</h2>
<ol class="breadcrumb">
    <li><a href="{{ route('m2p::home') }}"><i class="fa fa-dashboard"></i> Início</a></li>
    <li>Usuários</li>
    <li class="active">Criando</li>
</ol>
@endsection

@section('content')
<div class="row">
    <div class="col-md-12">
        @include('m2p.shared.validation')
        @include('flash::message')
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        {{ Form::open(['route' => ['m2p::user.store'], 'files' => true, 'role' => 'form',
                                    'autocomplete' => 'off']) }}
        <div class="form-group">
            <label for="inputLogin">Nome</label>
            {{ Form::text('first_name', null, ['class' => 'form-control', 'id' => 'inputLogin', 'placeholder' => 'Nome']) }}
        </div>
        <div class="form-group">
            <label for="inputLogin">Sobrenome</label>
            {{ Form::text('last_name', null, ['class' => 'form-control', 'id' => 'inputLogin', 'placeholder' => 'Sobrenome']) }}
        </div>
        <div class="form-group">
            <label for="inputLogin">Login</label>
            {{ Form::text('login', null, ['class' => 'form-control', 'id' => 'inputLogin', 'placeholder' => 'Login']) }}
        </div>
        <div class="form-group">
            <label for="inputEmail">E-mail</label>
            {{ Form::email('email', null, ['class' => 'form-control', 'id' => 'inputEmail', 'placeholder' => 'E-mail']) }}
        </div>
        <div class="form-group">
            <label for="inputPassword">Senha</label>
            {{ Form::password('password', ['class' => 'form-control', 'id' => 'inputPassword', 'placeholder' => 'Senha']) }}
        </div>
        <div class="form-group">
            <label for="inputPasswordTwo">Confirmação de senha</label>
            {{ Form::password('password_confirmation', ['class' => 'form-control', 'id' => 'inputPasswordTwo', 'placeholder' => 'Confirmação de senha']) }}
        </div>
        <div class="form-group">
            <label for="inputRole">Papel</label>
            {{ Form::select('role', $roles, null, ['class' => 'form-control', 'id' => 'inputRole', 'placeholder' => 'Selecione uma atribuição']) }}
        </div>
        <div class="form-group content-send">
            <button type="submit" class="btn btn-primary">Enviar</button>
        </div>
        {!! Form::close() !!}
    </div>
</div>
@endsection
