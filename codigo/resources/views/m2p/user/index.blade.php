@extends('m2p.layouts.app')

@section('content-header')
<h2 class="sub-header">Usuários</h2>
<ol class="breadcrumb">
    <li><a href="{{ route('m2p::home') }}"><i class="fa fa-dashboard"></i> Início</a></li>
    <li class="active">Usuários</li>
</ol>
@endsection

@section('content')
<div class="row">
    <div class="col-md-12">
        @include('m2p.shared.validation')
        @include('flash::message')

        <table class="table table-striped table-hover table-responsive">
            <thead>
                <tr>
                    <th>Nome</th>
                    <th>Sobrenome</th>
                    <th>Login</th>
                    <th>E-mail</th>
                    <th>Papeis</th>
                    <th>Criado em</th>
                    <th>Editar</th>
                    <th>Deletar</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($users as $u)
                <tr>
                    <td>{{ $u->first_name }}</td>
                    <td>{{ $u->last_name }}</td>
                    <td>{{ $u->login }}</td>
                    <td>{{ $u->email }}</td>
                    <td>{{ implode(',', $u->roles()->get()->pluck('display_name')->toArray())  }}</td>
                    <td>{{ $u->created_at }}</td>
                    <td>
                        <a href="{{ route('m2p::user.edit', ['user' => $u->id]) }}">
                            <i class="material-icons">mode_edit</i>
                        </a>
                    </td>
                    <td>
                        <a href="{{ route('m2p::user.destroy', ['user' => $u->id]) }}"
                           onclick="return confirm('Tem certeza que deseja deletar o registro ?')">
                            <i class="material-icons">delete_forever</i>
                        </a>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
        
        <div class="pagination-container">
            {!! Table::pagination($users) !!}
        </div>
    </div>
</div>
@endsection