@extends('layouts.site')

@section('title') {{ $content['title'] }} @endsection

@section('conteudo')
<section class="banner-servico-interno">
    <img src="{{ asset($content->getFirstMediaUrl('main', 'banner')) }}" alt="{{ $content->title }}">
    @include('site.shared.redes_sociais')
</section>

<section id="content-servico-interno">
    <header class="container-fluid">
        <div class="container">
            <ol class="breadcrumb">
                <li><a href="{{ route('site::home') }}">Home</a></li>
                <li><a href="{{ route('site::page', ['categorySlug' => $content['taxonomy']['terms']['category']['slug']]) }}">{{ $content['taxonomy']['terms']['category']['term'] }}</a></li>
                <li class="active">{{ $content['title'] }}</li>
            </ol>
        </div>
    </header>
    <section class="container" id="container-content">
        <div class="row">
            <div class="col-md-12">
                <h2>{{ $content['title'] }}</h2>
                {!! $content['content'] !!}
            </div>
        </div>
    </section>
    
    @include('site.shared.galeria', ['content' => $content])
</section>
@endsection