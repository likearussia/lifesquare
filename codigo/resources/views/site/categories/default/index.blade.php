@extends('layouts.site')

@section('title') {{ $content[0]['taxonomy']['terms']['category']['term'] }} @endsection

@section('conteudo')
<header id="pil" class="parallax">
    <h1>{{ $content[0]['taxonomy']['terms']['category']['term'] }}</h1>
</header>

<section class="content">
    <header class="container-fluid">
        <div class="container">
            <ol class="breadcrumb">
                <li><a href="{{ route('site::home') }}">Home</a></li>
                <li class="active">{{ $content[0]['taxonomy']['terms']['category']['term'] }}</li>
            </ol>
        </div>
    </header>

    <section class="container" id="container-content">
        <div class="row">
            <div class="col-md-12">
                <h2>{{ $content[0]['taxonomy']['terms']['category']['term'] }}</h2>
                <p>{{ $content[0]['taxonomy']['terms']['category']['description'] }}</p>
            </div>
        </div>
    </section>

    <section class="container-fluid" id="conv">
        <article class="container-fluid">
            @foreach ($content as $c)
                <div class="row painel-ts painel-ts-0{{ $loop->index % 3 }}">
                    @if (($loop->index % 2) == 0 )
                        <div class="col-md-6 sf painel-ts-imagem" style="background-image: url('{{ asset($c->getFirstMediaUrl('chamada', 'chamada')) }}');"></div>
                    @endif
                    
                    @if (($loop->index % 2) == 1 )
                        <div class="col-md-6 sf painel-ts-imagem imagem-esquerda" style="background-image: url('{{ asset($c->getFirstMediaUrl('chamada', 'chamada')) }}');"></div>
                    @endif
                    <div class="col-md-6" id="pad">
                        <h3>{{ $c->title }}</h3>
                        <p>{{ $c->excerpt }}</p>
                        <a href="{{ route('site::page', ['categorySlug' => $c['taxonomy']['terms']['category']['slug'], 'contentSlug' => $c->slug]) }}">
                            <button class="btn btn-ts btn-ts-0{{ $loop->index % 3 }}">Veja mais</button>
                        </a>
                    </div>
                    
                </div>
            @endforeach
        </article>
        @include('site.shared.servicos')
    </section>

</section>

@endsection

@push('scripts-cabecalho')
<link rel="stylesheet" href="https://s3.amazonaws.com/sample.web/css/jquery.fancybox.css">
<script src='https://s3.amazonaws.com/sample.web/js/jquery.easing.1.3.js'></script>
<script src='https://s3.amazonaws.com/sample.web/js/jquery.fancybox.js'></script>
<script src="{{ asset('js/jquery.mousewheel-3.0.4.pack.js') }}"></script>
@endpush
