@extends('layouts.site')

@section('title') {{ $content['title'] }} @endsection

@section('conteudo')
<header id="tennis" class="parallax">
    <h1>A <span>Tennis</span>Square</h1>
</header>

<section class="content">
    <header class="container-fluid">
        <div class="container">
            <ol class="breadcrumb">
                <li><a href="{{ route('site::home') }}">Home</a></li>
                <li><a href="#">{{ $content['taxonomy']['terms']['category']['term'] }}</a></li>
                <li class="active">{{ $content['title'] }}</li>
            </ol>
        </div>
    </header>

    <section class="container" id="container-content">
        <div class="row">
            <div class="col-md-12">
                <h2>{{ $content['title'] }}</h2>
                {!! $content['content'] !!}
            </div>
        </div>
    </section>

    <section class="container-fluid" id="dest-tennis">
        <article class="container">
            <div class="row">
                <div class="col-md-6 col-sm-15 col-xs-15">
                    @include('site.shared.galeria_servicos')
                </div>

                <div class="col-md-6 col-sm-12 col-xs-15">
                    <div class="row" id="listas-servicos">
                        <div id="tagcloud">
                            <img src="{{ asset('img/palavras2.png') }}">
                        </div>
                    </div>
                </div>
            </div>
        </article>
    </section>
    
    @include('site.shared.parceiros')
</section>
@endsection
