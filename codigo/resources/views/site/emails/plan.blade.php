<h3> Solicitação de associação enviada pelo site: </h3>

<ul>
    <li><strong>Nome</strong>: {{ $data['nome'] or '' }} </li>
    <li><strong>E-mail</strong>: {{ $data['email'] or '' }} </li>
    <li><strong>Telefone</strong>: {{ $data['telefone'] or '' }} </li>
    <li><strong>Plano escolhido</strong>: {!! $data['plano'] or '' !!} </li>
    <li><strong>Mensagem</strong>: {!! $data['mensagem'] or '' !!} </li>
</ul>