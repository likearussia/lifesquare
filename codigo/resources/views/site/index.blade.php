@extends('layouts.site')

@section('title') Home @endsection

@section('conteudo')
<section id="slider">
    <div id="owl-principal" class="owl-carousel owl-theme">
        @foreach ($banners as $banner)
            <?php $bannerTitle = (!empty($banner->meta->get(0)->value)) ? $banner->meta->get(0)->value['text'] : null;?>
            <div class="item parallax-home">
                <img src="{{ asset($banner->getFirstMediaUrl('banner', 'banner')) }}" alt="{{ $bannerTitle }}">
                <h1>{{ $bannerTitle }}</h1>
            </div>
        @endforeach
    </div>

</section>
<section id="oquefazemos">
    <h2>O que fazemos</h2>
   
    <!-- /.carousel -->
</section>
<section id="servicos">
    <article class="container">
        <div class="row">
            <div class="col-lg-4 col-md-4">
                <a href="{{ route('site::page', ['categorySlug' => 'academia-de-tenis']) }}"><img src="{{ asset('img/academiatenis2.png') }}" alt=""></a>

                <div class="traco2"></div>
                <div class="titulo"> <a href="{{ route('site::page', ['categorySlug' => 'academia-de-tenis']) }}"><h3>ACADEMIA DE TÊNIS</h3></a></div>
            </div>
            <div class="col-lg-4 col-md-4">

                <a href="{{ route('site::page', ['categorySlug' => 'fitness-center']) }}"><img src="{{ asset('img/academiatenis3.png') }}" alt=""></a>

                <div class="traco2"></div>
                <div class="titulo"> <a href="{{ route('site::page', ['categorySlug' => 'fitness-center']) }}"><h3>FITNESS CENTER</h3></a></div>
            </div>

            <div class="col-lg-4 col-md-4">

                <a href="{{ route('site::page', ['categorySlug' => 'estrutura']) }}"><img src="{{ asset('img/court2.png') }}" alt=""></a>

                <div class="traco2"></div>
                <div class="titulo"> <a href="{{ route('site::page', ['categorySlug' => 'estrutura']) }}"><h3>ESTRUTURA</h3></a></div>
            </div>
        </div>
    </article>
</section>
@endsection

@push('scripts')
<script type="text/javascript">
    $("#owl-principal").owlCarousel({
        {{-- https://github.com/OwlCarousel2/OwlCarousel2/issues/628 --}}
        @if ($banners->count() > 1)
            loop: true,
            autoplay: true,
        @endif
        navigation: false,
        pagination: true,
        slideSpeed: 300,
        paginationSpeed: 400,
        items: 1,
        itemsDesktop: false,
        itemsDesktopSmall: false,
        itemsTablet: false,
        itemsMobile: false
    });
</script>
@endpush