@extends('layouts.site')

@section('title') Resultados de busca @endsection

@section('conteudo')
<header id="error" class="parallax">
    <h1></h1>
</header>

<section class="content">
    <header class="container-fluid">
        <div class="container">
            <ol class="breadcrumb">
                <li><a href="{{ route('site::home') }}">Home</a></li>
                <li class="active">Busca</li>

            </ol>
        </div>
    </header>

    <section id="pr" class="container">
        <div class="row">
            <?php
            $contentTypesIds = [];
            foreach ($contentTypes as $t) {
                $contentTypesIds[$t->name] = $t->id;
            }
            ?>
            @foreach ($content as $c)
                <?php
                // Se o conteúdo atual for uma página de blog
                if ($contentTypesIds['post'] == $c->content_type_id) {
                    $link = route('blog::show', ['slug' => $c->slug]);
                } else {
                    $link = route('site::page', ['categorySlug' => $c->getTaxonomiesTerms()['category']['slug'],
                                                 'contentSlug' => $c->slug]);
                }
                ?>
                <div class="card-column">
                    <article class="card2">
                        @if (!empty($c->getFirstMediaUrl('chamada', 'chamada')))
                            <a href="{{ $link }}" class="card-image" style=" background-image: url({{ asset($c->getFirstMediaUrl('chamada', 'chamada')) }});"></a>
                        @endif
                        <section class="card-content">
                            <header class="card-header">
                                <p class="card-subtitle">
                                    <script>
                                        moment.locale('pt-br');
                                        window.document.write(moment("{{ $c->created_at }}", "DD/MM/YYYY hh:mm:ss").fromNow());
                                    </script>
                                </p>
                                <h2 class="card-title">
                                    <a href="{{ $link }}">
                                        {{ $c->title }}
                                    </a>
                                </h2>
                            </header>
                            <section class="card-body">
                                <p>
                                    {{ $c->excerpt }}
                                </p>
                                <p class="read-more"><a href="{{ $link }}">Veja mais...</a></p>
                                <div class="post-footer">
                                </div>
                            </section>
                        </section>
                    </article>
                </div>
            @endforeach
        </div>
        <div class="row">
            {!! $content->links() !!}
        </div>
    </section>
</section>
@endsection
