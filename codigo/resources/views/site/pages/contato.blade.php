@extends('layouts.site')

@section('title') Contato @endsection

@section('conteudo')
<header id="news" class="parallax">
    <h1>Contato</h1>
</header>

<section class="content">
    <header class="container-fluid">
        <div class="container">
            <ol class="breadcrumb">
                <li><a href="{{ route('site::home') }}">Home</a></li>
                <li class="active">Contato</li>
            </ol>
        </div>
    </header>

    <section class="container-fluid" id="prog">
        <div class="row" id="eq">
            <div class="col-md-12 col-sm-12 col-xs-12" id="cont-infos">
                <div id="informacoes col-md-12 col-sm-12 col-xs-12">
                    <h3>Informações</h3>
                    <div class="row">
                        <div class="col-md-1 col-xs-1">
                            <p><i class="fa fa-map-marker"></i></p>
                        </div>
                        <div class="col-md-11 col-xs-11">
                            <p>Rua Ribeiro do Amaral, 500
                                - Ipiranga<br><small>São Paulo - SP - 
                                CEP 04268-000</small></p>
                        </div>

                        <div class="col-md-1 col-xs-1">
                            <p><i class="fa fa-phone"></i></p>
                        </div>
                        <div class="col-md-11 col-xs-11">
                            <p>(11) 2215-2521</p>
                        </div>

                        <div class="col-md-1 col-xs-1">
                            <p><i class="fa fa-fax"></i></p>
                        </div>
                        <div class="col-md-11 col-xs-11">
                            <p>(11) 2063-6655</p>
                        </div>

                        <div class="col-md-1 col-xs-1">
                            <p><i class="fa fa-envelope"></i></p>
                        </div>
                        <div class="col-md-11 col-xs-11">
                            <p>contato@tennissquare.com.br</p>
                        </div>
                    </div>

                    <div class="row" id="soc">
                        <h3>Redes sociais</h3>
                        <ul>
                            <li><a href="http://facebook.com/tennissquareipiranga"><i class="fa fa-facebook"></i></a></li>
                            <!-- <li><a ><i class="fa fa-youtube"></i></a></li> -->
                            <li><a href="http://instagram.com/square_tennis"><i class="fa fa-instagram"></i></a></li>
                            <li><a href="http://twitter.com/TenniSquare1"><i class="fa fa-twitter"></i></a></li>
                            <li><a href="https://www.snapchat.com/add/tennissquare"><i class="fa fa-snapchat-ghost"></i></a></li>
                            <li><a href="https://www.youtube.com/channel/UCysjlGXsMV1WBJT4FH4---A"><i class="fa fa-youtube-play"></i></a></li>
                        </ul>
                    </div>
                </div>

                <div class="row col-md-12 col-sm-12 col-xs-12" id="mapa">
                    <div id="map">
                        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3656.2332429630446!2d-46.60992528396294!3d-23.59596658466576!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x94ce5becaa088419%3A0x3ea3e294b486b9b4!2sR.+Ribeiro+do+Amaral%2C+500+-+Ipiranga%2C+S%C3%A3o+Paulo+-+SP!5e0!3m2!1spt-BR!2sbr!4v1475764635216" frameborder="0" style="border:0" allowfullscreen></iframe>
                    </div>
                </div>
            </div>

            <div class="col-md-6 col-xs-12" id="prevencao">
                <h3>Entre em contato</h3>
                <p>Preencha o formulário para enviar a sua mensagem para a TennisSquare.</p>
                
                @include('site.shared.validation')
                @include('flash::message')
                
                <form action="{{ route('site::contact.send') }}" method="post" class="contactform">
                    {{ csrf_field() }}
                    
                    <input type="text" name="nome" placeholder="Nome" value="{{ old('nome') }}">
                    
                    <input type="email" name="email" placeholder="Email" value="{{ old('email') }}">
                    
                    <span class="interesse">
                        <div include="form-input-select()">
                            <select name="interesse" required>
                                {-- <!--
                                  This is how we can do "placeholder" options.
                                  note: "required" attribute is on the select
                                --> --}
                                <option value="" hidden>Interesse</option>

                                @foreach ($subjects as $k => $v)
                                    <option <?php print (old('interesse') == $k) ? 'selected' : null; ?>
                                        value="{{ $k }}">{{ $v }}</option>
                                @endforeach
                            </select>
                        </div>
                    </span>
                    
                    <textarea name="mensagem" id="" cols="30" rows="10" placeholder="Mensagem">{{ old('mensagem') }}</textarea>
                    
                    <button type="submit" name="submit"> Enviar </button>
                </form>
            </div>
        </div>
    </section>
</section>
@endsection

@push('scripts')

@endpush