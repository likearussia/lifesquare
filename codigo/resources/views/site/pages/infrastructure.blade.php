@extends('layouts.site')

@section('title') Estrutura @endsection

@section('conteudo')
<!-- The Bootstrap Image Gallery lightbox, should be a child element of the document body -->
<div id="blueimp-gallery" class="blueimp-gallery blueimp-gallery-controls" data-use-bootstrap-modal="false">
    <!-- The container for the modal slides -->
    <div class="slides"></div>
    <!-- Controls for the borderless lightbox -->
    <h3 class="title"></h3>
    <a class="prev">‹</a>
    <a class="next">›</a>
    <a class="close">×</a>
    <a class="play-pause"></a>
    <ol class="indicator"></ol>
    <!-- The modal dialog, which will be used to wrap the lightbox content -->
    <div class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" aria-hidden="true">&times;</button>
                    <h4 class="modal-title"></h4>
                </div>
                <div class="modal-body next"></div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left prev">
                        <i class="glyphicon glyphicon-chevron-left"></i> Previous
                    </button>
                    <button type="button" class="btn btn-primary next">
                        Next
                        <i class="glyphicon glyphicon-chevron-right"></i>
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>

<section class="container" id="container-content">
    <div class="row">
        <div class="col-md-12">
            <h2 class="titulo-h2">Galeria de Fotos</h2>
        </div>
        
            <div id="links" class="gallery-container">
                @foreach ($content as $c)
                <div class="col-sm-3 gallery-col">
                    <a href="{{ $c->getFirstMediaUrl('infrastructure', 'infrastructure') }}"
                        title="{{ $c->title or 'Estrutura da Tennis Square' }}" data-gallery>
                        <img class="gallery-thumb" src="{{ $c->getFirstMediaUrl('infrastructure', 'infrastructureThumb') }}">
                    </a>
                </div>
                @endforeach
            </div>
    </div>
    
    <div class="row">
        <div class="col-md-12">
            {!! $content->links() !!}
        </div>
    </div>
</section>
@endsection

@push('css')
<link rel="stylesheet" href="//blueimp.github.io/Gallery/css/blueimp-gallery.min.css">
<link rel="stylesheet" href="{{ asset('dist/Bootstrap-Image-Gallery/css/bootstrap-image-gallery.min.css') }}">
@endpush

@push('scripts')
<script src="//blueimp.github.io/Gallery/js/jquery.blueimp-gallery.min.js"></script>
<script src="{{ asset('dist/Bootstrap-Image-Gallery/js/bootstrap-image-gallery.min.js') }}"></script>
@endpush
