@extends('layouts.site')

@section('title') Planos @endsection

@section('conteudo')
<header id="planos" class="parallax">
    <h1>Planos</h1>
</header>

<section class="content">
    <header class="container-fluid">
        <div class="container">
            <ol class="breadcrumb">
                <li><a href="{{ route('site::home') }}">Home</a></li>
                <li class="active">{{ $contentType->description }}</li>
            </ol>
        </div>
    </header>

    <section class="container" id="container-content">
        <div class="row">
            <div class="col-md-12">
                <h2>Seja um associado TennisSquare</h2>
                <p>Confira os melhores planos para você e sua família.</p>
                
                @include('site.shared.validation')
                @include('flash::message')

                <div class="navbar-inner">
                    <ul class="nav nav-pills">
                        <li class="active">
                            <a href="#plano-pronto-passo1" data-toggle="tab" data-step="1">Escolha seu plano</a>
                        </li>
                        <li>
                            <a href="#plano-pronto-passo2" data-toggle="tab" data-step="3">Dados para contato</a>
                        </li>
                    </ul>
                </div>

                <div class="tab-content">
                    <div class="tab-pane fade in active" id="plano-pronto-passo1">
                        <div class="well well-planos">
                            <div class="pricing-table">
                                @foreach ($plans as $plan)
                                    <div class="col-md-4">
                                        <div class="pricing-option">
                                            <i class="material-icons"></i>
                                            <h1>{{ $plan->title }}</h1>
                                            <hr />
                                            <p>{!! $plan->content !!}</p>
                                            <hr />
                                            <div class="price">
                                                <div class="front">
                                                    <span class="price">R${{ number_format($plan->meta[0]->value['cost'], 2, ',', '.') }}</span>
                                                </div>
                                                <div class="">
                                                    <label>Selecionar esse</label>
                                                    <input type="radio" name="plano-pronto" class="item-plano-pronto" value="{{ $plan->title }}"
                                                           <?= (old('plano-pronto') == $plan->title) ? 'checked' : null ?>>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                        
                        <a class="btn btn-default btn-lg next1" href="#">Continuar</a>
                    </div>

                    <div class="tab-pane fade" id="plano-pronto-passo2">
                        <div class="well"> 
                            <form action="{{ route('site::plan.send') }}" method="post" class="contactform">
                                {{ csrf_field() }}
                                <input type="text" name="nome" placeholder="Nome" value="{{ old('nome') }}">
                                <input type="email" name="email" placeholder="Email" value="{{ old('email') }}">
                                <input type="tel" name="telefone" placeholder="Telefone" value="{{ old('telefone') }}">
                                <input type="text" name="plano" placeholder="Plano escolhido" readonly value="{{ old('plano') }}">
                                <textarea name="mensagem" cols="30" rows="10" placeholder="Mensagem">{{ old('mensagem') }}</textarea>
                                <button type="submit"> Enviar </button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row" id="monteseuplano">
            <div class="col-md-12">
                <h2>Monte seu plano</h2>
                <p>Customize seu plano para atender melhor suas necessidades.</p>

                <div class="navbar-inner">
                    <ul class="nav nav-pills">
                        <li class="active">
                            <a href="#montar-plano-passo1" data-toggle="tab" data-step="montar-plano-passo1">Escolha as opções</a>
                        </li>
                        <li>
                            <a href="#montar-plano-passo2" data-toggle="tab" data-step="montar-plano-passo2">Visualize</a>
                        </li>
                        <li>
                            <a href="#montar-plano-passo3" data-toggle="tab" data-step="montar-plano-passo3">Dados de contato</a>
                        </li>
                    </ul>
                </div>

                <div class="tab-content">
                    <div class="tab-pane fade in active" id="montar-plano-passo1">
                        <div class="well">
                            <div class="row">
                                <div class="col-md-4" id="montar-plano-tenis">
                                    <h3>Academia de Tênis</h3>
                                    <ul>
                                        <li>
                                            <input name="aula-individual" type="number" value="0" min="0" max="50">
                                            <label> Aula Individual / Semana</label>
                                        </li>
                                        <li>
                                            <input name="aula-grupo" type="number" value="0" min="0" max="50">
                                            <label> Aula em Grupo / Semana</label>
                                        </li>
                                        <li>
                                            <div class="flip-switch flip-switch-icon">
                                                <input name="treinamento-avançado" type="checkbox" id="c2">
                                                <label for="c2"></label>
                                            </div>
                                            <label class="sp-label"> Treinamento Avançado</label>
                                        </li>
                                    </ul>
                                </div>
                                
                                <div class="col-md-4" id="montar-plano-fitness">
                                    <h3>Fitness Center</h3>
                                    <ul>
                                        <li>
                                            <input name="musculação" type="number" value="0" min="0" max="99">
                                            <label> Musculação / Semana</label>
                                        </li>
                                        <li>
                                            <input name="treinamento-funcional" type="number" value="0" min="0" max="99">
                                            <label> Treinamento Funcional / Semana</label>
                                        </li>
                                        <li>
                                            <input name="pilates" type="number" value="0" min="0" max="99">
                                            <label> Pilates / Semana</label>
                                        </li>
                                        <li>
                                            <input name="spining" type="number" value="0" min="0" max="99">
                                            <label> Spining / Semana</label>
                                        </li>
                                        <li>
                                            <input name="jump-step" type="number" value="0" min="0" max="99">
                                            <label> Jump + Step / Semana</label>
                                        </li>
                                        <li>
                                            <input name="gap" type="number" value="0" min="0" max="99">
                                            <label> GAP / Semana</label>
                                        </li>
                                    </ul>
                                </div>
                                
                                <div class="col-md-4" id="montar-plano-fitness">
                                    <h3>&nbsp;</h3>
                                    <ul>
                                        
                                        <li>
                                            <input name="remo" type="number" value="0" min="0" max="99">
                                            <label> Remo / Semana</label>
                                        </li>
                                        <li>
                                            <input name="yoga" type="number" value="0" min="0" max="99">
                                            <label> Yoga / Semana</label>
                                        </li>
                                        <li>
                                            <input name="fisioterapia" type="number" value="0" min="0" max="99">
                                            <label> Fisioterapia Esportiva / Semana</label>
                                        </li>
                                        <li>
                                            <input name="nutricao" type="number" value="0" min="0" max="99">
                                            <label> Nutrição / Semana</label>
                                        </li>
                                        <li>
                                            <input name="max-recovery" type="number" value="0" min="0" max="99">
                                            <label> Max Recovery / Semana</label>
                                        </li>
                                        <div class="flip-switch flip-switch-icon">
                                            <input name="academia-livre" type="checkbox" id="c3">
                                            <label for="c3"></label>
                                        </div>
                                        <label class="sp-label"> Academia livre</label>
                                    </ul>
                                </div>
                                
                            </div>
                        </div>

                        <a class="btn btn-default btn-lg next1" href="#">Continuar</a>
                    </div>
                    
                    <div class="tab-pane fade" id="montar-plano-passo2">
                        <div class="well"> 
                            <h2>Plano escolhido</h2>
                            
                            <div id="plano-montado">
                                <h3>Academia de Tênis</h3>
                                <ul id="exibir-plano-montado-tenis"></ul>

                                <h3>Fitness Center</h3>
                                <ul id="exibir-plano-montado-fitness"></ul>

                                <h3>&nbsp;</h3>
                            </div>
                        </div>
                        <a class="btn btn-default next1" href="#">Continue</a>
                    </div>
                    
                    <div class="tab-pane fade" id="montar-plano-passo3">
                        <div class="well">
                            <form action="{{ route('site::plan.send') }}" method="post" class="contactform">
                                {{ csrf_field() }}
                                <input type="text" name="nome" placeholder="Nome" value="{{ old('nome') }}">
                                <input type="email" name="email" placeholder="Email" value="{{ old('email') }}">
                                <input type="tel" name="telefone" placeholder="Telefone" value="{{ old('telefone') }}">
                                <input type="hidden" name="plano" placeholder="Plano escolhido" value="{{ old('plano') }}">
                                <textarea name="mensagem" cols="30" rows="10" placeholder="Mensagem">{{ old('mensagem') }}</textarea>
                                <button type="submit"> Enviar </button>
                            </form>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </section>
</section>
@endsection

@push('scripts')
<script>
    $('.next1').click(function () {
        var nextId = $(this).parents('.tab-pane').next().attr("id");
        $('[href=#' + nextId + ']').tab('show');
        return false;

    });

    (function () {
        window.inputNumber = function (el) {
            var min = el.attr('min') || false;
            var max = el.attr('max') || false;
            var els = {};
            els.dec = el.prev();
            els.inc = el.next();
            el.each(function () {
                init($(this));
            });
            function init(el) {
                els.dec.on('click', decrement);
                els.inc.on('click', increment);
                function decrement() {
                    var value = el[0].value;
                    value--;
                    if (!min || value >= min) {
                        el[0].value = value;
                    }
                }
                function increment() {
                    var value = el[0].value;
                    value++;
                    if (!max || value <= max) {
                        el[0].value = value++;
                    }
                }
            }
        };
    }());
    inputNumber($('.input-number'));
    //# sourceURL=pen.js
</script>

<!-- Lida com planos prontos -->
<script type="text/javascript">
    $(document).ready(function() {
        $('.item-plano-pronto').on('change', function() {
            if ($(this).is(':checked')) {
                $('#plano-pronto-passo2').find('input[name="plano"]').val($(this).val());
            }
        });
    });
</script>

<!-- Lida com planos personalizados -->
<script type="text/javascript">
String.prototype.capitalizeFirstLetter = function() {
    return this.charAt(0).toUpperCase() + this.slice(1);
};

function itemDoPlanoFoiEscolhido($item) {
    if ($item.val() > 0) {
        return true;
    }
    
    if ($item.is(':checked')) {
        return true;
    }

    return false;
}

function gerarItemDoPlanoParaExibicao($item) {
    var valor = $item.val();
    var valorTratado = '';
    
    if (valor == 'on') {
        valorTratado = 'Sim';
    } else {
        valorTratado = valor;
    }
    
    return '<li>' + tratarNomeItemPlano($item.attr('name')) + ' - ' + valorTratado + '</li>';
}

function tratarNomeItemPlano(item) {
    return item.capitalizeFirstLetter().replace('-', ' ');
}

$(document).ready(function() {
   $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
        var target = $(e.target).attr("href");
        
        if (target == '#montar-plano-passo2') {
            $exibirPlanoTenis = $('#exibir-plano-montado-tenis');
            $exibirPlanoTenis.empty();
            $('#montar-plano-tenis input').each(function() {
                if (itemDoPlanoFoiEscolhido($(this))) {
                    $exibirPlanoTenis.append(gerarItemDoPlanoParaExibicao($(this)));
                }
            });

            $exibirPlanoFitness = $('#exibir-plano-montado-fitness');
            $exibirPlanoFitness.empty();
            $('#montar-plano-fitness input').each(function() {
                if (itemDoPlanoFoiEscolhido($(this))) {
                    $exibirPlanoFitness.append(gerarItemDoPlanoParaExibicao($(this)));
                }
            });

            $exibirPlanoBemEstar = $('#exibir-plano-montado-fitness');
            $exibirPlanoBemEstar.empty();
            $('#montar-plano-fitness input').each(function() {
                if (itemDoPlanoFoiEscolhido($(this))) {
                    $exibirPlanoBemEstar.append(gerarItemDoPlanoParaExibicao($(this)));
                }
            });
        }
        
        if (target == '#montar-plano-passo3') {
            var plano = $('#plano-montado').html();
            $('#montar-plano-passo3').find('input[name="plano"]').val(plano);
        }
    }); 
});  
</script>
@endpush