@extends('layouts.site')

@section('title') Ranking @endsection

@section('conteudo')
<header id="rkg" class="parallax">
    <h1><span>Ranking</span> square</h1>
</header>

<section class="content">
    <header class="container-fluid">
        <div class="container">
            <ol class="breadcrumb">
                <li><a href="{{ route('site::home') }}">Home</a></li>
                <li class="active">{{ $taxonomy->description }}</li>
            </ol>
        </div>
    </header>

    <section id="rkng" class="container-fluid">
        <div class="container" id="container-content">
            <div class="row">
                <div class="col-md-6">
                    <p>Desafio aberto ao público, alunos, locatários e também para os que ainda não são clientes da academia, com realização de um Masters, torneio entre os 16 melhores do ranking.</p>
                </div>
                <div class="col-md-6" id="filtros">
                    <div class="col-md-12">
                        <div class="search-ranking">
                            <input type="text" id="search-player-name" placeholder="pesquisar por nome">
                            <button id="search-player"><i class="fa fa-search"></i></button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6" id="page-message"></div>
            </div>
        </div>

        {{-- antes a cada dois ranks era uma div class="container rankg" --}}
        <div class="container rankg">
            <div class="row">
                @foreach ($ranks as $rank)
                    <?php
                    // Armazena os players deste rank
                    $playersPorRank = $players->filter(function ($value, $key) use ($rank) {
                        return (int)$value->player_rank == (int)$rank->id;
                    }); ?>
                    @if ($playersPorRank->isEmpty())
                        @continue;
                    @endif
                    
                    <?php
                    // Ordena os players do rank por seus pontos
                    $playersPorPontos = $playersPorRank->sortBy('player_name')->sortByDesc('player_points');

                    // Para paginação
                    $totalPlayers = $playersPorPontos->count();
                    $perPage = 5;
                    $totalPages = round($totalPlayers / $perPage);
                    $currentPage = 0;
                    ?>
                    <div class="col-md-6">
                        <div class="tit-tab rank-menu" data-rank="{{ $rank->slug }}" data-page="1">
                            <h2>
                                {{ $rank->term }}
                            </h2>
                        </div>
                        <div class="table">
                            <div class="table-cell">
                                <ul class="leader">
                                    <div id="decoration"></div>
                                    <div id="decoration2"></div>
                                    <div id="decoration3"></div>

                                    @foreach ($playersPorPontos as $player)
                                    <?php
                                    $playerThumb = $player->getFirstMediaUrl('player', 'playerThumb');
                                    if (empty($playerThumb)) {
                                        if (str_contains($rank->term, 'mascu')) {
                                         $playerThumb = asset('img/players/man.png');
                                        } else {
                                            $playerThumb = asset('img/players/woman.png');
                                        }
                                    }
                                    if (($loop->index % $perPage) == 0) {
                                        $currentPage++;
                                    }
                                    ?>
                                    <li class="player-container" data-rank="{{ $rank->slug }}" data-page="{{ $currentPage }}" style="display:none;">
                                        <span class="list_num">{{ $loop->iteration }}</span>
                                        <img src="{{ $playerThumb }}" />
                                        <h2 class="player-name">{{ $player->player_name }}<span class="number">{{ $player->player_points }}</span></h2>
                                        
                                    </li>
                                    
                                    @endforeach
                             <div class="rank-menu" data-rank="{{ $rank->slug }}" data-page="1">
                                <a href="#" class="rank-controller rank-previous">
                                    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"><p>Anterior</p></span>
                                </a>
                                <a href="#" class="rank-controller rank-next"> <p>Ver mais</p>
                                    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                                </a>
                            </div>
                                </ul>

                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </section>
</section>
@endsection

@push('scripts')
<script type="text/javascript" src="{{ asset('dist/jquery.animate-colors/jquery.animate-colors-min.js') }}"></script>
<script type="text/javascript">
    $(document).ready(function() {
        // Carga inicial
        showRanksFirstPage();
        checkRankMenus();
        
        var message = $('#page-message');
        
        $('.rank-next, .rank-previous').click(function(e) {
            e.preventDefault();
            $data = $(this).parents('.rank-menu');
            rank = $data.data('rank');
            page = $data.data('page');
            
            if ($(this).hasClass('rank-next')) {
                newPage = page + 1;
            } else {
                newPage = page - 1;
            }
            
            $('.player-container[data-rank="' + rank + '"][data-page="' + page + '"]').each(function(index, obj) {
                $(this).hide();
            });
            $('.player-container[data-rank="' + rank + '"][data-page="' + newPage + '"]').each(function(index, obj) {
                $(this).show();
            });
            
            $data.data('page', newPage);
            
            checkRankMenus();
        });
        
        // Busca de jogadores
        $('#search-player').on('click', function() {
            var playerName = $('#search-player-name').val().toLowerCase().trim();
            var playerWasFound = false;

            if (playerName) {
                $('.player-container').each(function() {
                    rank = $(this).data('rank');
                    page = $(this).data('page');

                    var actualPlayer = $(this).find('.player-name');
                    p = actualPlayer.text().toLowerCase(); // contem nome e pontuação
                    if (p.indexOf(playerName) !== -1) {
                        playerWasFound = true;

                        showRankPage(rank, page);

                        actualPlayer.animate({'backgroundColor': '#ffff99'}, 1000)
                                    .animate({'backgroundColor': '#2b686e'}, 1000);
                    }
                });
            }
            
            if (playerWasFound == false) {
                showRanksFirstPage();
                message.html('<div class="alert alert-danger">Jogador não encontrado.</div>');
            } else {
                message.empty();
            }
        });
        $('#search-player-name').keypress(function(e) {
            console.log('keypress');
            if (e.which == 13) {
                $('#search-player').click();
            }
        });
        
        /**
         * Verifica todos os menus de rank e ativa/desativa os botões de anterior e próximo
         */
        function checkRankMenus() {
            $('.rank-menu').each(function() {
                rank = $(this).data('rank');
                page = $(this).data('page');
                nextPage = page + 1;
                previousPage = page - 1;
                if (!areThereItemsForPage(rank, nextPage)) {
                    $(this).find('.rank-next').hide();
                } else {
                    $(this).find('.rank-next').show();
                }
                if (!areThereItemsForPage(rank, previousPage)) {
                    $(this).find('.rank-previous').hide();
                } else {
                    $(this).find('.rank-previous').show();
                }
            });
        }
        
        function areThereItemsForPage(rank, page) {
            if ($('.player-container[data-rank="' + rank + '"][data-page="' + page + '"]').length > 0) {
                return true;
            }
            
            return false;
        }
        
        function showRanksFirstPage() {
            $('.player-container[data-page="1"]').each(function(index, obj) {
                $(this).show();
            });
            $('.player-container[data-page!="1"]').each(function(index, obj) {
                $(this).hide();
            });
            checkRankMenus();
        }
    
        function showRankPage(rank, page) {
            $('.player-container').each(function(index, obj) {
                $(this).hide();
            });

            $('.player-container[data-rank="' + rank + '"][data-page="' + page + '"]').each(function(index, obj) {
                $(this).show();
            });
            
            checkRankMenus();
        }
    });
</script>
@endpush