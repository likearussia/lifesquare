@if (isset($content) && ($images = $content->getMedia('galery')) && !$images->isEmpty())
<link rel="stylesheet" href="//blueimp.github.io/Gallery/css/blueimp-gallery.min.css">
<link rel="stylesheet" href="{{ asset('dist/Bootstrap-Image-Gallery/css/bootstrap-image-gallery.min.css') }}">

<script src="//blueimp.github.io/Gallery/js/jquery.blueimp-gallery.min.js"></script>
<script src="{{ asset('dist/Bootstrap-Image-Gallery/js/bootstrap-image-gallery.min.js') }}"></script>

<!-- The Bootstrap Image Gallery lightbox, should be a child element of the document body -->
<div id="blueimp-gallery" class="blueimp-gallery blueimp-gallery-controls" data-use-bootstrap-modal="false">
    <!-- The container for the modal slides -->
    <div class="slides"></div>
    <!-- Controls for the borderless lightbox -->
    <h3 class="title"></h3>
    <a class="prev">‹</a>
    <a class="next">›</a>
    <a class="close">×</a>
    <a class="play-pause"></a>
    <ol class="indicator"></ol>
    <!-- The modal dialog, which will be used to wrap the lightbox content -->
    <div class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" aria-hidden="true">&times;</button>
                    <h4 class="modal-title"></h4>
                </div>
                <div class="modal-body next"></div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left prev">
                        <i class="glyphicon glyphicon-chevron-left"></i> Previous
                    </button>
                    <button type="button" class="btn btn-primary next">
                        Next
                        <i class="glyphicon glyphicon-chevron-right"></i>
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container">
    <div class="row">
        <div class="col-md-12">
            <h2 class="titulo-h2">Galeria de Fotos</h2>
        </div>
            
            <div id="links" class="gallery-container">
                <?php
                $perPage = 12;
                $totalPages = ceil(count($images) / $perPage);
                $page = 0; ?>
                
                @foreach ($images as $image)
                    <?php
                    if (($loop->index) % $perPage == 0) {
                        $page++;
                    } ?>
                    
                    <div class="col-sm-3 gallery-col" data-page="{{ $page }}" style="display: none;">
                        <a href="{{ $image->getUrl('galery') }}" title="Galeria de imagens" data-gallery>
                            <img class="gallery-thumb" src="{{ $image->getUrl('galeryThumb') }}">
                        </a>
                    </div>
                @endforeach
            </div>
        </div>
    
    <div class="row">
        <div class="col-md-12">
            <div class="gallery-menu" data-page="1">
                <a href="#" class="gallery-controller gallery-previous pull-left" style="display: none;">
                    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true">Anterior</span>
                </a>
                <a href="#" class="gallery-controller gallery-next pull-right">
                    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true">Próxima</span>
                </a>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function() {
        showGallerysFirstPage();
        
        function showGallerysFirstPage() {
            $('.gallery-col[data-page="1"]').each(function(index, obj) {
                $(this).show();
            });
            checkGalleryMenus();
        }
        
        $('.gallery-next, .gallery-previous').click(function(e) {
            e.preventDefault();
            $data = $(this).parents('.gallery-menu');
            page = $data.data('page');
            
            if ($(this).hasClass('gallery-next')) {
                newPage = page + 1;
            } else {
                newPage = page - 1;
            }
            
            $('.gallery-col[data-page="' + page + '"]').each(function(index, obj) {
                $(this).hide();
            });
            $('.gallery-col[data-page="' + newPage + '"]').each(function(index, obj) {
                $(this).show(1000);
            });
            
            $data.data('page', newPage);
            
            checkGalleryMenus();
        });
        
        /**
         * Verifica o menu da galeria e ativa/desativa os botões de anterior e próximo
         */
        function checkGalleryMenus() {
            $('.gallery-menu').each(function() {
                page = $(this).data('page');
                nextPage = page + 1;
                previousPage = page - 1;
                if (!areThereItemsForPage(nextPage)) {
                    $(this).find('.gallery-next').hide();
                } else {
                    $(this).find('.gallery-next').show();
                }
                if (!areThereItemsForPage(previousPage)) {
                    $(this).find('.gallery-previous').hide();
                } else {
                    $(this).find('.gallery-previous').show();
                }
            });
        }
        
        function areThereItemsForPage(page) {
            if ($('.gallery-col[data-page="' + page + '"]').length > 0) {
                return true;
            }
            
            return false;
        }
    });
</script>
@endif