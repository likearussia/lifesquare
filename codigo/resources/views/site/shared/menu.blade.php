<?php
// #TODO esta lógica não deveria ficar aqui
$actual = [];
$actual['routeName']    = (!empty(Route::current())) ? Route::current()->getName() : null;
?>
<nav class="navbar navbar-static-top marginBottom-0" role="navigation">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-2 col-md-2  col-sm-4 col-xs-4 nav-mobile nav-large">
                <div class="navbar-header">
                    <a class="navbar-brand" href="{{ route('site::home') }}"><img src="{{ asset('img/logo.png') }}" alt="logo"></a>
                    <button type="button" class="navbar-toggle col-xs-8" data-toggle="collapse" data-target="#navbar-collapse-1">

                        <div class="right-span">
                            <span class="sr-only">Alterar navegação</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </div>
                    </button>

                </div><!-- end nav-bar -->
            </div><!-- col-lg-3 end-->
            <div class="col-lg-10 col-md-10 col-sm-8 col-xs-8" id="topbar">

                <div class="row" id="searchbar">

                    <div class="col-lg-4 col-md-5 col-sm-7 col-xs-12 menusite" id="l-busca">
                        <div class="search-form">
                            <form class="search" method="GET" action="{{ route('site::search') }}">
                                <i class="glyphicon glyphicon-search open-search-mobile"></i>
                                <input type="text" class="input-busca" name="q" placeholder=" Faça sua busca..."
                                       value="<?php print ($actual['routeName'] == 'site::search') ? request()->q : null; ?>"/>
                                
                                <input type="submit" class="mobile-buscar" style="display: none;" value="buscar"/>
                            </form>
                        </div>
                    </div>
                    <div class="sociais-box">
                        <ul class="nav soc-b">
                            <li><a href="http://facebook.com/tennissquareipiranga" target="_blank"><i class="fa fa-facebook"></i></a></li>
                            <li><a href="http://twitter.com/TenniSquare1" target="_blank"><i class="fa fa-twitter"></i></a></li>
                            <!-- <li><a href=""><i class="fa fa-youtube"></i></a></li> -->
                            <li><a href="http://instagram.com/square_tennis" target="_blank"><i class="fa fa-instagram"></i></a></li>
                            <li><a href="https://www.snapchat.com/add/TennisSquare" target="_blank"><i class="fa fa-snapchat-ghost"></i></a></li>
                            <li><a href="https://www.youtube.com/channel/UCysjlGXsMV1WBJT4FH4---A" target="_blank"><i class="fa fa-youtube-play"></i></a></li>
                        </ul>

                    </div>
                </div>

                <div class="row" id="top">
                    <div class="box-mobile-menu menusite" >
                        <div class="collapse navbar-collapse" id="navbar-collapse-1">
                            <ul class="nav navbar-nav">
                                <?php
                                if (!empty(Route::current())) {
                                    $urlAtual = url()->current();
                                    $urlBaseComCategoria = request()->getSchemeAndHttpHost() . request()->getBaseUrl() . '/' . Route::current()->getParameter('categorySlug');
                                } else {
                                    $urlAtual = null;
                                    $urlBaseComCategoria = null;
                                }
                                ?>
                                
                                <?php $rota = route('site::home');
                                print ($urlAtual == $rota) ? '<li class="ativo">' : '<li>'; ?>
                                    <a href="{{ $rota }}">Home</a>
                                </li>
                                
                                <?php $rota = route('site::page', ['categorySlug' => 'institucional', 'contentSlug' => 'a-tennissquare']);
                                print ($urlAtual == $rota) ? '<li class="ativo">' : '<li>'; ?>
                                    <a href="{{ $rota }}">A TennisSquare</a>
                                </li>

                                <?php $rota = route('site::page', ['categorySlug' => 'academia-de-tenis']);
                                print ($urlBaseComCategoria == $rota) ? '<li class="ativo">' : '<li>'; ?>
                                    <a href="{{ $rota }}">Academia de Tênis</a>
                                </li>

                                <?php $rota = route('site::page', ['categorySlug' => 'fitness-center']);
                                print ($urlBaseComCategoria == $rota) ? '<li class="ativo">' : '<li>'; ?>
                                    <a href="{{ $rota }}">Fitness Center</a>
                                </li>

                                <?php $rota = route('site::page', ['categorySlug' => 'estrutura']);
                                print ($urlAtual == $rota) ? '<li class="ativo">' : '<li>'; ?>
                                    <a href="{{ $rota }}">Estrutura</a>
                                </li>

                                <?php $rota = route('site::rank');
                                print ($urlAtual == $rota) ? '<li class="ativo">' : '<li>'; ?>
                                    <a href="{{ route('site::rank') }}">Ranking Square</a>
                                </li>

                                <?php
                                print (strpos($actual['routeName'], 'blog::') === 0) ? '<li class="ativo">' : '<li>'; ?>
                                    <a href="{{ route('blog::index') }}">Square News</a>
                                </li>

                                <?php $rota = route('site::plan');
                                print ($urlAtual == $rota) ? '<li class="ativo">' : '<li>'; ?>
                                    <a href="{{ $rota }}">Planos</a>
                                </li>

                                <?php $rota = route('site::contact');
                                print ($urlAtual == $rota) ? '<li class="ativo">' : '<li>'; ?>
                                    <a href="{{ $rota }}">Contato</a>
                                </li>
                            </ul>
                        </div>
                    </div>
<!--
                    <div class="col-md-12 col-xs-10 col-lg-4 col-sm-12 socialbar">
                    </div>
-->
                    <!-- end of main menu -->

                </div><!-- /.navbar-collapse -->
            </div>
        </div>
    </div><!-- End Row -->
</div> <!-- End Container -->
</nav> <!-- Fim do menu -->