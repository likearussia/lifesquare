@inject('partners', 'Mind2Press\Http\Controllers\PartnerController')
<?php
$data = $partners->getData();
$partners = $data['partners']; ?>

@if (!$partners->isEmpty())
<section class="container-fluid" id="parceiros">
    <article class="container">
        <div class="row">
            <h3>Parceiros</h3>
            
            @foreach ($partners as $partner)
                <div class="col-sm-3 parceiro">
                    <img src="{{ $partner->getFirstMediaUrl('partner', 'partner') }}" title="{{ $partner->title }}"
                         class="img-responsive center-block"/>
                </div>
            @endforeach
        </div>
    </article>
</section>
@endif