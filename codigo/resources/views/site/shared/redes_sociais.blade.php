<div class="redes-sociais">
    <div class="redes">
        <a target="_blank" class="fa fa-facebook" href="{{ url('/') }}"></a>
        <a href="https://twitter.com/share" class="fa fa-twitter"></a>
    </div>
</div>

<script>
    $(document).ready(function () {

        var x = $(location).attr('href');
        $('.fa-facebook').attr('href', "http://www.facebook.com/sharer.php?u=" + x);

    })
</script>


<script>
    ! function (d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0],
            p = /^http:/.test(d.location) ? 'http' : 'https';
        if (!d.getElementById(id)) {
            js = d.createElement(s);
            js.id = id;
            js.src = p + '://platform.twitter.com/widgets.js';
            fjs.parentNode.insertBefore(js, fjs);
        }
    }(document, 'script', 'twitter-wjs');
</script>