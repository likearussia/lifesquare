﻿@inject('menu', 'Mind2Press\Http\Controllers\RodapeMenuController')
<?php $data = $menu->getData(); ?>

<footer>
    <section class="container-fluid" id="ftr">
        <!--<div class="col-lg-3 col-md-5 col-xs-12 col-sm-12 ftr-logo" id="logo">
            <img src="{{ asset('img/logo.png') }}" alt="logo Tennis Square">
        </div>-->
        
        @foreach ($data['categories'] as $category)
        <?php $content = $data['content'][$category->id]; ?>
        <div class="ftr ftr-0{{ $loop->index }}">
            <h5>{{ $category->term }}</h5>
            <ul>
                @foreach ($content as $c)
                    <li>
                        <a href="{{ route('site::page', ['categorySlug' => $category->slug, 'contentSlug' => $c->slug]) }}">
                            {{ $c->title }}
                        </a>
                    </li>
                @endforeach
            </ul>
        </div>
        @endforeach
        
        <div class="ftr-cat ftr">
            <h5><a href="{{ route('site::structure') }}" title="" style="color: #00464f;">ESTRUTURA</a></h5>
            <h5><a href="{{url('ranking-square')}}" title="" style="color: #00464f;">RANKING SQUARE</a></h5>
            <h5><a href="{{ route('blog::index') }}" title="" style="color: #00464f;">SQUARE news</a></h5>
            <h5><a href="{{url('contato')}}" title="" style="color: #00464f;">contato</a></h5>
        </div>
        <div class="ftr ftr-soc">
            <ul class="social">
                <li>
                    <a href="https://www.facebook.com/tennissquareipiranga/" target="_blank">
                        <i class="fa fa-facebook"></i> / tennissquareipiranga
                    </a>
                </li>
                <li>
                    <a href="http://twitter.com/TenniSquare1" target="_blank">
                        <i class="fa fa-twitter"></i> / TenniSquare1
                    </a>
                </li>
                <li>
                <a href="http://instagram.com/square_tennis" target="_blank">
                        <i class="fa fa-instagram"></i> / square_tennis
                    
                    </a>
                </li>
                <li>
                <a href="https://www.snapchat.com/add/TennisSquare" target="_blank">
                        <i class="fa fa-snapchat-ghost"></i> / TennisSquare
                    
                    </a>
                </li>
                <li>
                <a href="https://www.youtube.com/channel/UCysjlGXsMV1WBJT4FH4---A" target="_blank">
                        <i class="fa fa-youtube-play"></i> / square_tennis
                    
                    </a>
                </li>
            </ul>
        </div>
    </section>
    
    <section class="container" id="bottom">
        <div class="col-md-9 col-lg-10 col-xs-5 col-sm-5">
            <p>© Copyright 2016 TennisSquare</p>
        </div>
        <div class="col-md-3 col-lg-2 col-sm-6 col-xs-6 text-right">
            <p>Ph2 full Creativity</p>
        </div>
    </section>

</footer>