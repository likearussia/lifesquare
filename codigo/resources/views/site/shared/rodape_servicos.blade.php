@inject('rank', 'Mind2Press\Http\Controllers\RankController')
<?php $data = $rank->getFooterData(); ?>

<section id="footer">

    <div class="">
        <div class="fb--flex-item fb--flex-item__A" id="ranking">
            <h4>Ranking Square</h4>
            <div class="item item-ranking">
                <div class="col-md-6 col-sm-12 col-xs-12">
                    <ol class="rank">
                        <h3>Masculino</h3>
                        <li class="header-table">
                            <div class="row ftr-rank">
                                <div class="col-xs-6">Nome</div>
                                <div class="col-xs-2 pts">Pontos</div>
                                <div class="col-xs-4">Categoria</div>
                            </div>
                        </li>

                        @foreach ($data['ranks_masculinos'] as $rank)
                        <?php $player = $data['players'][$rank->id]; ?>
                        @if (empty($player))
                            @continue
                        @endif
                        <li>
                            <div class="row">
                                <div class="col-xs-6">{{ $player->player_name }}</div>
                                <div class="col-xs-2 pts">{{ $player->player_points }}</div>
                                <div class="col-xs-4">{{ str_replace(' masculino', '', $rank->term) }}</div>
                            </div>
                        </li>
                        @endforeach
                    </ol>
                </div>

                <div class="col-md-6 col-sm-12 col-xs-12">
                    <ol class="rank">
                        <h3>Feminino</h3>
                        <li class="header-table">
                            <div class="row ftr-rank">
                                <div class="col-xs-6">Nome</div>
                                <div class="col-xs-2 pts">Pontos</div>
                                <div class="col-xs-4">Categoria</div>
                            </div>
                        </li>

                        @foreach ($data['ranks_femininos'] as $rank)
                        <?php $player = $data['players'][$rank->id]; ?>
                        @if (empty($player))
                            @continue
                        @endif
                        <li>
                            <div class="row">
                                <div class="col-xs-6">{{ $player->player_name }}</div>
                                <div class="col-xs-2 pts">{{ $player->player_points }}</div>
                                <div class="col-xs-4">{{ str_replace(' feminino', '', $rank->term) }}</div>
                            </div>
                        </li>
                        @endforeach
                    </ol>
                    <a href="{{ route('site::rank') }}" style="float: right;"><button class="btn btn-default">Ver todos</button></a>
                </div>
            </div>
        </div>

        <div class="fb--flex-item itens-r fb--flex-item__B" id="locacao" onclick="location.href ='{{ route('site::page', ['categorySlug' => 'academia-de-tenis', 'contentSlug' => 'locacao-de-quadras']) }}'">
            <div class="box-flex-inner">
                <h3>LOCAÇÃO DE QUADRAS</h3>
                <h2>2 Quadras de Saibro <br>
                    4 Quadras de piso rápido
                </h2>
            </div>
        </div>
        <div class="fb--flex-item itens-r fb--flex-item__C" id="academia" onclick="location.href ='{{ route('site::page', ['categorySlug' => 'fitness-center']) }}'">
            <div class="box-flex-inner">
                <h3>ACADEMIA</h3>
                <h2>Conheça nossa academia<br>e mantenha a boa forma</h2>
            </div>
        </div>
        <div class="fb--flex-item itens-r fb--flex-item__D" id="fitness" onclick="location.href ='{{ route('site::plan') }}'">
            <div class="box-flex-inner">
                <h3>PLANOS</h3>
                <h2>Seja um aluno da TennisSquare</h2>
            </div>
        </div>

    </div>

</section>
