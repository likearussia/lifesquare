<section class="container-fluid" id="srvc">
    <article class="container">
        <div class="row">
            <div class="col-md-4 col-sm-12 col-xs-12">
                <a href="{{url('fitness-center/fisioterapia-esportiva')}}">
                    <img src="{{ asset('img/fisioterapia.png') }}" alt="quadra">
                    <div class="traco"></div>
                   <h3>Fisioterapia</h3>
                    <p>Sala de fisioterapia completa para<br> a recuperação de atletas, com ênfase <br>em trabalho preventivo. </p>
                </a>
            </div>
            <div class="col-md-4 col-sm-12 col-xs-12">
                <a href="{{url('fitness-center/nutricao')}}">
                    <img src="{{ asset('img/nutricao.png') }}" alt="quadra">
                    <div class="traco"></div>
                    <h3>Nutrição</h3>
                    <p>Orientação nutricional e programa<br> de fortalecimento ou perda de peso.</p>
                </a>
            </div>
            <div class="col-md-4 col-sm-12 col-xs-12">
                   <a href="{{url('fitness-center')}}">
                    <img src="{{ asset('img/psicologia.png') }}" alt="quadra">
                    <div class="traco"></div>
                    <h3 class="men">Fitness</h3>
                    <p>Aprimoramento psicomotor<br> dos grupos musculares envolvidos<br> na prática do esporte.</p>
                    </a>
                </li>
            </div>
        </div>
    </article>
</section>
