@if (count($errors) > 0)
    <div class="alert alert-danger">
        <p>Os seguintes erros foram encontrados no formulário:</p>
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif