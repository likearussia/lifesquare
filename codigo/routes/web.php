<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

/**
 * M2P
 */
Auth::routes();
Route::get('logout', [
    'uses' => '\Mind2Press\Http\Controllers\Auth\LoginController@logout',
    'as' => 'logout',
]);

Route::group(['prefix' => 'squaredmin', 'namespace' => 'M2P', 'as' => 'm2p::', 'middleware' => 'auth'], function() {
    Route::get('/', [
        'uses' => 'HomeController@index', 'as' => 'home'
    ]);
    
    // Content
    Route::group(['prefix' => 'content', 'as' => 'content.'], function() {
        Route::get('/index', [
            'uses' => 'ContentController@index', 'as' => 'index',
        ]);
        Route::get('/create', [
            'uses' => 'ContentController@create', 'as' => 'create',
        ]);
        Route::post('/store', [
            'uses' => 'ContentController@store', 'as' => 'store',
        ]);
        Route::get('/edit/{content}', [
            'uses' => 'ContentController@edit', 'as' => 'edit',
        ]);
        Route::post('/edit/{content}', [
            'uses' => 'ContentController@update', 'as' => 'update',
        ]);
        Route::get('/delete/{slug}', [
            'uses' => 'ContentController@destroy', 'as' => 'destroy',
        ]);
    });
    
    // Taxonomy
    Route::group(['prefix' => 'taxonomy', 'as' => 'taxonomy.'], function() {
        Route::get('/index', [
            'uses' => 'TaxonomyController@index', 'as' => 'index',
        ]);
        Route::get('/create', [
            'uses' => 'TaxonomyController@create', 'as' => 'create',
        ]);
        Route::post('/store', [
            'uses' => 'TaxonomyController@store', 'as' => 'store',
        ]);
        Route::get('/edit/{content}', [
            'uses' => 'TaxonomyController@edit', 'as' => 'edit',
        ]);
        Route::post('/edit/{content}', [
            'uses' => 'TaxonomyController@update', 'as' => 'update',
        ]);
        Route::get('/delete/{slug}', [
            'uses' => 'TaxonomyController@destroy', 'as' => 'destroy',
        ]);
    });
    
    // Banner
    Route::group(['prefix' => 'banner', 'as' => 'banner.'], function() {
        Route::get('/index', [
            'uses' => 'BannerController@index', 'as' => 'index',
        ]);
        Route::get('/create', [
            'uses' => 'BannerController@create', 'as' => 'create',
        ]);
        Route::post('/store', [
            'uses' => 'BannerController@store', 'as' => 'store',
        ]);
        Route::get('/edit/{banner}', [
            'uses' => 'BannerController@edit', 'as' => 'edit',
        ]);
        Route::post('/edit/{banner}', [
            'uses' => 'BannerController@update', 'as' => 'update',
        ]);
        Route::get('/delete/{banner}', [
            'uses' => 'BannerController@destroy', 'as' => 'destroy',
        ]);
    });
    
    // Partner
    Route::group(['prefix' => 'partner', 'as' => 'partner.'], function() {
        Route::get('/index', [
            'uses' => 'PartnerController@index', 'as' => 'index',
        ]);
        Route::get('/create', [
            'uses' => 'PartnerController@create', 'as' => 'create',
        ]);
        Route::post('/store', [
            'uses' => 'PartnerController@store', 'as' => 'store',
        ]);
        Route::get('/edit/{banner}', [
            'uses' => 'PartnerController@edit', 'as' => 'edit',
        ]);
        Route::post('/edit/{banner}', [
            'uses' => 'PartnerController@update', 'as' => 'update',
        ]);
        Route::get('/delete/{banner}', [
            'uses' => 'PartnerController@destroy', 'as' => 'destroy',
        ]);
    });
    
    // Galery
    Route::group(['prefix' => 'galery', 'as' => 'galery.'], function() {
        Route::get('/galery/{contentId}', [
            'uses' => 'GaleryController@show', 'as' => 'show',
        ]);
        Route::post('/upload', [
            'uses' => 'GaleryController@upload', 'as' => 'upload',
        ]);
        Route::get('/delete/{filename?}', [
            'uses' => 'GaleryController@delete', 'as' => 'delete',
        ]);
    });
    
    // Player
    Route::group(['prefix' => 'player', 'as' => 'player.'], function() {
        Route::get('/index', [
            'uses' => 'PlayerController@index', 'as' => 'index',
        ]);
        Route::get('/create', [
            'uses' => 'PlayerController@create', 'as' => 'create',
        ]);
        Route::post('/store', [
            'uses' => 'PlayerController@store', 'as' => 'store',
        ]);
        Route::post('/storeField', [
            'uses' => 'PlayerController@storeField', 'as' => 'storeField',
        ]);
        Route::get('/edit/{user}', [
            'uses' => 'PlayerController@edit', 'as' => 'edit',
        ]);
        Route::post('/edit/{user}', [
            'uses' => 'PlayerController@update', 'as' => 'update',
        ]);
        Route::get('/delete/{user}', [
            'uses' => 'PlayerController@destroy', 'as' => 'destroy',
        ]);
    });
    
    // Infrastructure
    Route::group(['prefix' => 'infrastructure', 'as' => 'infrastructure.'], function() {
        Route::get('/index', [
            'uses' => 'InfrastructureController@index', 'as' => 'index',
        ]);
        Route::get('/create', [
            'uses' => 'InfrastructureController@create', 'as' => 'create',
        ]);
        Route::post('/store', [
            'uses' => 'InfrastructureController@store', 'as' => 'store',
        ]);
        Route::get('/edit/{content}', [
            'uses' => 'InfrastructureController@edit', 'as' => 'edit',
        ]);
        Route::post('/edit/{content}', [
            'uses' => 'InfrastructureController@update', 'as' => 'update',
        ]);
        Route::get('/delete/{content}', [
            'uses' => 'InfrastructureController@destroy', 'as' => 'destroy',
        ]);
    });
    
    // Plan
    Route::group(['prefix' => 'plan', 'as' => 'plan.'], function() {
        Route::get('/index', [
            'uses' => 'PlanController@index', 'as' => 'index',
        ]);
        Route::get('/create', [
            'uses' => 'PlanController@create', 'as' => 'create',
        ]);
        Route::post('/store', [
            'uses' => 'PlanController@store', 'as' => 'store',
        ]);
        Route::get('/edit/{user}', [
            'uses' => 'PlanController@edit', 'as' => 'edit',
        ]);
        Route::post('/edit/{user}', [
            'uses' => 'PlanController@update', 'as' => 'update',
        ]);
        Route::get('/delete/{user}', [
            'uses' => 'PlanController@destroy', 'as' => 'destroy',
        ]);
    });
    
    // User
    Route::group(['prefix' => 'user', 'as' => 'user.'], function() {
        Route::get('/index', [
            'uses' => 'UserController@index', 'as' => 'index',
        ]);
        Route::get('/create', [
            'uses' => 'UserController@create', 'as' => 'create',
        ]);
        Route::post('/store', [
            'uses' => 'UserController@store', 'as' => 'store',
        ]);
        Route::get('/edit/{user}', [
            'uses' => 'UserController@edit', 'as' => 'edit',
        ]);
        Route::post('/edit/{user}', [
            'uses' => 'UserController@update', 'as' => 'update',
        ]);
        Route::get('/delete/{user}', [
            'uses' => 'UserController@destroy', 'as' => 'destroy',
        ]);
    });
    
    Route::group(['prefix' => 'options', 'as' => 'options.'], function() {
        Route::get('/layout', [
            'uses' => 'OptionController@index', 'as' => 'layout'
        ]);
    });
});

/**
 * Blog
 */
Route::group(['prefix' => 'square-news', 'as' => 'blog::'], function() {
    Route::get('/', [
        'uses' => 'BlogController@index', 'as' => 'index',
    ]);
    Route::get('/{slug}', [
        'uses' => 'BlogController@show', 'as' => 'show',
    ]);
});

/**
 * Site
 */
Route::group(['as' => 'site::'], function() {
    Route::get('/', [
        'uses' => 'HomeController@index', 'as' => 'home',
    ]);
    
    Route::get('/ranking-square', [
        'uses' => 'RankController@index', 'as' => 'rank',
    ]);
    
    Route::get('/planos', [
        'uses' => 'PlanController@index', 'as' => 'plan',
    ]);
    Route::post('/planos/enviar', [
       'uses' => 'PlanController@sendMessage', 'as' => 'plan.send',
    ]);
    
    Route::get('/buscar', [
        'uses' => 'SearchController@search', 'as' => 'search',
    ]);

    // Estrutura
    Route::get('/estrutura', [
        'uses' => 'InfrastructureController@index', 'as' => 'structure',
    ]);
    
    // Contato
    Route::get('/contato', [
        'uses' => 'ContactController@show', 'as' => 'contact',
    ]);
    Route::post('/contato/enviar', [
        'uses' => 'ContactController@send', 'as' => 'contact.send',
    ]);

    // Páginas
    Route::get('/{categorySlug}/{contentSlug?}', [
       'uses' => 'PagesController@index', 'as' => 'page',
    ]);
});
