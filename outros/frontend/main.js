$(document).ready(function() {
 
  $("#owl-principal").owlCarousel({
 
      navigation : false, // 
      pagination : true,
 
      slideSpeed : 300,
      paginationSpeed : 400,
 
      items : 1, 
      itemsDesktop : false,
      itemsDesktopSmall : false,
      itemsTablet: false,
      itemsMobile : false,

 
  });

   $('#owl-mob').owlCarousel({
    loop:true,
    nav:false,
    center:true,
    pagination:true,
    responsive:{
        0:{
            items:1
        },
        600:{
            items:1
        },
        768:{
            items:2
        }

    }
})

   $('#owl-ranking').owlCarousel({
    loop:true,
    singleItem:true,
    responsiveClass:true,
    responsive:{
        0:{
            items:1
        },
        600:{
            items:1
        }

      
    }
})

    $('#owl-quadras').owlCarousel({
    loop:true,
    singleItem:true,
    responsiveClass:true,
    nav:true,
    pagination:false,
    dots:false,
    navText: ["<i class='fa fa-angle-left'></i>","<i class='fa fa-angle-right'></i>"],
    responsive:{
        0:{
            items:1
        },
        600:{
            items:1
        }

      
    }
})

     $('#owl-loja').owlCarousel({
    loop:true,
    singleItem:true,
    responsiveClass:true,
    responsive:{
        0:{
            items:1
        },
        600:{
            items:1
        }

      
    }
})


   $(window).scroll(function () {
    $('.parallax').css('top', -$(window).scrollTop() / 2);
    var blur = $(window).scrollTop() / 50;
    if (blur > 10)
        blur = 10;
    $('.parallax').css('-webkit-filter', 'blur(' + blur + 'px) grayscale(' + blur / 5 + ')');
});

  
      $(document).ready(function () {
    $('#gallery a').fancybox({
        overlayColor: '#060',
        overlayOpacity: 0.4,
        transitionIn: 'elastic',
        transitionOut: 'elastic',
        easingIn: 'easeInSine',
        easingOut: 'easeOutSine',
        titlePosition: 'outside',
        cyclic: true
    });
});


$('.search-toggle').addClass('closed');
$('.search-toggle .search-icon').click(function (e) {
    if ($('.search-toggle').hasClass('closed')) {
        $('.search-toggle').removeClass('closed').addClass('opened');
        $('.search-toggle, .search-container').addClass('opened');
        $('#search-terms').focus();
    } else {
        $('.search-toggle').removeClass('opened').addClass('closed');
        $('.search-toggle, .search-container').removeClass('opened');
    }
});

      
   




});


